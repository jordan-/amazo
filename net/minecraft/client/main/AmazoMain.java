package net.minecraft.client.main;

import java.util.Arrays;

public class AmazoMain {

    public static void main(String[] args)
    {
        Main.main(concat(new String[] {"--version", "mcp"}, args));
    }

    public static <T> T[] concat(T[] first, T[] second)
    {
        T[] result = Arrays.copyOf(first, first.length + second.length);
        System.arraycopy(second, 0, result, first.length, second.length);
        return result;
    }
	
}
