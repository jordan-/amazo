package com.waytoogosu.amazo.keybind;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.lwjgl.input.Keyboard;

import com.waytoogosu.amazo.Wrapper;
import com.waytoogosu.amazo.event.Event;
import com.waytoogosu.amazo.event.EventListener;
import com.waytoogosu.amazo.event.EventListenerData;
import com.waytoogosu.amazo.event.EventPriority;
import com.waytoogosu.amazo.event.events.EventKeyPress;
import com.waytoogosu.amazo.mod.Mod;
import com.waytoogosu.amazo.mod.ModManager;

public class KeybindManager {

	private List<Keybind> keybinds;

	public KeybindManager(){
		keybinds = new ArrayList();
		registerKeybinds();
	}

	private void registerKeybinds(){
		ModManager modManager = Wrapper.getModManager();

		for(Mod m : modManager.getMods()){
			getKeybinds().add(new ModKeybind(m));
		}

		int i = Keyboard.KEY_RSHIFT, j = Keyboard.KEY_Y;

		File file = new File(Wrapper.getSaveManager().getClientDirectory(), "gui.txt");

		if(!file.exists()){
			try {
				BufferedWriter bw = new BufferedWriter(new FileWriter(file));
				bw.write("gui:\"" + Keyboard.getKeyName(i) + "\"\r\n");
				bw.write("console:\"" + Keyboard.getKeyName(j) + "\"");
				bw.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			try{
				BufferedReader br = new BufferedReader(new FileReader(file));
				String s;
				while((s = br.readLine()) != null){
					String str = s;
					s = s.split(":")[1];
					s = s.substring(1, s.length() - 1);
					if(str.contains("gui")){
						i = Keyboard.getKeyIndex(s);
					} else if(str.contains("console")){
						j = Keyboard.getKeyIndex(s);
					}
				}

				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		final int guiKey = i;
		final int consoleKey = j;

		keybinds.add(new Keybind(){

			@Override
			public int getKeybind() {
				return guiKey;
			}

			@Override
			public void onAction() {
				Wrapper.getMc().displayGuiScreen(Wrapper.getUiScreen());
			}

			@Override
			public String getName() {
				return "Gui";
			}

		});
		keybinds.add(new Keybind(){

			@Override
			public int getKeybind() {
				return consoleKey;
			}

			@Override
			public void onAction() {
				Wrapper.getMc().displayGuiScreen(Wrapper.getUiConsole());
			}

			@Override
			public String getName() {
				return "Console";
			}

		});
	}

	public List<Keybind> getKeybinds(){
		return keybinds;
	}


}
