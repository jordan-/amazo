package com.waytoogosu.amazo.keybind;

public abstract class Keybind {

	public abstract int getKeybind();
	public abstract void onAction();
	public abstract String getName();

}