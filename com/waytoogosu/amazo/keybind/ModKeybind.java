package com.waytoogosu.amazo.keybind;

import com.waytoogosu.amazo.mod.Mod;

public class ModKeybind extends Keybind{

	private Mod mod;
	
	public ModKeybind(Mod mod){
		this.mod = mod;
	}
	
	@Override
	public int getKeybind() {
		return mod.getKeybind();
	}

	@Override
	public void onAction() {
		mod.toggle();
	}

	@Override
	public String getName() {
		return mod.getName();
	}
	

}
