package com.waytoogosu.amazo.mod;

import java.util.ArrayList;
import java.util.List;

import com.waytoogosu.amazo.mod.mods.*;
import com.waytoogosu.amazo.mod.mods.mining.ModInstantMiner;
import com.waytoogosu.amazo.mod.mods.mining.ModSpeedmine;
import com.waytoogosu.amazo.mod.mods.movement.ModFlight;
import com.waytoogosu.amazo.mod.mods.movement.ModNofall;
import com.waytoogosu.amazo.mod.mods.movement.ModSprint;
import com.waytoogosu.amazo.mod.mods.player.ModAutoRespawn;
import com.waytoogosu.amazo.mod.mods.player.ModAutoTool;
import com.waytoogosu.amazo.mod.mods.pvp.ModAutoSoup;
import com.waytoogosu.amazo.mod.mods.pvp.ModKillAura;
import com.waytoogosu.amazo.mod.mods.pvp.ModTriggerBot;
import com.waytoogosu.amazo.mod.mods.render.ModFullbright;
import com.waytoogosu.amazo.mod.mods.render.ModNametags;
import com.waytoogosu.amazo.mod.mods.render.ModTracers;
import com.waytoogosu.amazo.mod.mods.world.ModAutoHarvest;
import com.waytoogosu.amazo.mod.mods.world.ModAutoPlant;

public class ModManager {

	private List<Mod> mods;
	private List<Mod> enabledMods;
	
	public ModManager(){
		mods = new ArrayList();
		enabledMods = new ArrayList();
		registerMods();
	}
	
	private void registerMods(){
		mods.add(new ModInstantMiner());
		mods.add(new ModFullbright());
		mods.add(new ModSpeedmine());
		mods.add(new ModFlight());
		mods.add(new ModNofall());
		mods.add(new ModKillAura());
		mods.add(new ModSprint());
		mods.add(new ModTracers());
	//	mods.add(new ModFind());
	//	mods.add(new ModSurvivalControl());
		mods.add(new ModNametags());
		mods.add(new ModAutoPlant());
		mods.add(new ModAutoHarvest());
		mods.add(new ModTriggerBot());
		mods.add(new ModAutoSoup());
		mods.add(new ModAutoRespawn());
		mods.add(new ModAutoTool());
	}
	
	public List<Mod> getMods(){
		return mods;
	}
	
	public List<Mod> getEnabledMods(){
		return enabledMods;
	}
	
}