package com.waytoogosu.amazo.mod;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;

import com.waytoogosu.amazo.Wrapper;
import com.waytoogosu.amazo.command.Command;
import com.waytoogosu.amazo.command.CommandManager;
import com.waytoogosu.amazo.command.Parameter;
import com.waytoogosu.amazo.event.Event;
import com.waytoogosu.amazo.event.EventListener;
import com.waytoogosu.amazo.event.events.EventSendChatMessage;
import com.waytoogosu.amazo.font.AmazoUnicodeFont;

public class ChatEventListener implements EventListener{

	public static final String INPUT = "\247f[INPUT-]";
	public static final String OUTPUT = "\247f[OUTPUT]\247f";
	public static String prefix;

	public ChatEventListener(){
		prefix = ".";
		File file = new File(Wrapper.getSaveManager().getClientDirectory(), "commands.txt");

		if(!file.exists()){
			try {
				BufferedWriter bw = new BufferedWriter(new FileWriter(file));
				bw.write("prefix:\"" + prefix + "\"");
				bw.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			try{
				BufferedReader br = new BufferedReader(new FileReader(file));
				String s = br.readLine().split(":")[1];
				prefix = s.substring(1, s.length() - 1);

				System.out.println("Prefix found: \"" + prefix + "\"");
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void onEvent(Event e) {
		EventSendChatMessage escm = (EventSendChatMessage) e;
		String msg = escm.getMessage().trim();
		if(msg.startsWith(prefix)){
			handleInput(msg.replaceFirst(prefix, ""));
			escm.setCancelled(true);
		}
	}

	public void handleInput(String s){
		respond(INPUT, s);

		CommandManager cm = Wrapper.getCommandManager();
		
		int shortest = -1;
		String input = s.split(" ")[0];
		String closest = "";
		
		for(Command cmd : cm.getCommands()){
			int lev = StringUtils.getLevenshteinDistance(input.toLowerCase(), cmd.getName().toLowerCase());
			
			if(lev == 0) {
				closest = cmd.getName().toLowerCase();
				shortest = 0;
			}
			
			if(lev <= shortest || shortest < 0) {
				closest = cmd.getName().toLowerCase();
				shortest = lev;
			}
			
			try{
				String[] args = s.split(" ");
				if(!cmd.getName().equalsIgnoreCase(args[0])){
					continue;
				}
				if(args.length - 1!= cmd.getParams().length || !cm.argsMatchParam(args, cmd.getParams())){
					String params = "";
					for(Parameter parm : cmd.getParams()){
						params += " <\247a" + parm.getType() + " " + parm.getName() + "\247f>";
					}
					respond(OUTPUT, "\247cArguments: \247f\"" + cmd.getName() + params + "\"");			
					return;
				}
				if(cmd.onCommand(args, s, false)) return;
			} catch (Exception e) {
				respond(OUTPUT, "An error occured when running the command. Stacktrace printed to console");
				e.printStackTrace();
				return;
			}
		}
		respond(OUTPUT, "\247cUnknown command: \"\247f" + s + "\247c\""); 
		respond(OUTPUT, "\247cDid you mean: \"\247f" + closest + "\247c\"?");
	}

	private void respond(String prefix, String s){
		Wrapper.getMc().thePlayer.addChatMessage(s);
	}

}
