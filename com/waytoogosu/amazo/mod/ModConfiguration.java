package com.waytoogosu.amazo.mod;


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.waytoogosu.amazo.Wrapper;

public class ModConfiguration{
	
	public static final int ONE_ACTIVE_BOOLEAN = 1;
	
//	private File booleansFile, valuesFile;
	private HashMap<String, Boolean> booleans;
	private HashMap<String, Double> values;
	private Mod mod;
	private int rule;
	
	public ModConfiguration(Mod m){
		booleans = new HashMap();	
		values = new HashMap();
		mod = m;
		/*booleansFile = new File(LuridWrapper.getClientDirectory(), m.getName() + "-booleans.txt");
		valuesFile = new File(LuridWrapper.getClientDirectory(), m.getName() + "-values.txt");*/
		setRule(0);
	}
	
	public int getRule() {
		return rule;
	}

	public void setRule(int rule) {
		this.rule = rule;
	}

	public HashMap<String, Boolean> getBooleans() {
		return booleans;
	}

	public HashMap<String, Double> getValues() {
		return values;
	}
	
	public void addBoolean(String name, boolean state){
		booleans.put(name.toLowerCase(), state);
	}
	
	public void addValue(String name, double value){
		values.put(name.toLowerCase(), value);
	}
	
	public boolean getBoolean(String name){
		return booleans.get(name.toLowerCase());
	}
	
	public float getFloatValue(String name){
		return (float) getValue(name.toLowerCase());
	}
	
	public int getIntValue(String name){
		return (int) getValue(name.toLowerCase());
	}	
	
	public double getValue(String name){	
		return values.get(name.toLowerCase());
	}
	
	public void addValueWithSlider(String name, double value, double maxValue, boolean round){
		values.put(name.toLowerCase(), value);
		mod.addSlider(name, maxValue/100D, round);
	}
	
	public void setBoolean(String name, boolean state){	
		addBoolean(name, state);
		if(state && getRule() == ONE_ACTIVE_BOOLEAN){
			List<String> disableBooleans = new ArrayList();
			for(String s : getBooleans().keySet()){
				if(getBooleans().get(s) && !name.equalsIgnoreCase(s)){
					disableBooleans.add(s);
				}
			}
			for(String s : disableBooleans){
				getBooleans().remove(s);
				addBoolean(s, false);
			}
		}
		Wrapper.getSaveManager().getSave("ModConfigs").save();
	}
	
	public void setValue(String name, double value){
		values.put(name.toLowerCase(), value);
		Wrapper.getSaveManager().getSave("ModConfigs").save();
	}
	
	public void setBooleanWithoutSaving(String name, boolean state){
		addBoolean(name, state);
		if(state && getRule() == ONE_ACTIVE_BOOLEAN){
			List<String> disableBooleans = new ArrayList();
			for(String s : getBooleans().keySet()){
				if(getBooleans().get(s) && !name.equalsIgnoreCase(s)){
					disableBooleans.add(s);
				}
			}
			for(String s : disableBooleans){
				getBooleans().remove(s);
				addBoolean(s, false);
			}
		}
	}
	
	public void setValueWithoutSaving(String name, double value){
		values.put(name.toLowerCase(), value);
	}
	
}
