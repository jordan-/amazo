package com.waytoogosu.amazo.mod;

public enum Category {
	PVP("PvP"),
	MINING("Mining"),
	MOVEMENT("Movement"),
	RENDER("Render"),
	WORLD("World"),
	PLAYER("Player");
	
	private String name;
	
	private Category(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
}
