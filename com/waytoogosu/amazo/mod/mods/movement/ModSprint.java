package com.waytoogosu.amazo.mod.mods.movement;

import net.minecraft.src.Minecraft;

import org.lwjgl.input.Keyboard;

import com.waytoogosu.amazo.Wrapper;
import com.waytoogosu.amazo.event.Event;
import com.waytoogosu.amazo.event.events.EventTick;
import com.waytoogosu.amazo.mod.Category;
import com.waytoogosu.amazo.mod.Mod;

public class ModSprint extends Mod {

	public ModSprint() {
		super(Keyboard.KEY_LCONTROL, 0xff94FFF8);
		setShowInGuiWithoutSaving(true);
	}

	@Override
	public void registerEvents() {
		super.registerEvent(EventTick.class);
	}

	@Override
	public String getName() {
		return "Sprint";
	}

	@Override
	public String getDescription() {
		return "Enables sprinting constantly while this is enabled.";
	}

	@Override
	public Category getCategory() {
		return Category.MOVEMENT;
	}

	@Override
	public void onEnabled(Event e) {
		Minecraft mc = Wrapper.getMc();
		if (e instanceof EventTick && Wrapper.getMc().currentScreen == null) {
			boolean moving = mc.currentScreen == null
					&& mc.inGameHasFocus
					&& ((Keyboard
							.isKeyDown(mc.gameSettings.keyBindForward.keyCode) || Keyboard
							.isKeyDown(mc.gameSettings.keyBindBack.keyCode)) || Keyboard
							.isKeyDown(mc.gameSettings.keyBindLeft.keyCode))
					|| Keyboard.isKeyDown(mc.gameSettings.keyBindRight.keyCode);
			if (moving) {
				mc.thePlayer.setSprinting(true);
			}
		}
	}

}
