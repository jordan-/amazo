package com.waytoogosu.amazo.mod.mods.movement;

import net.minecraft.src.EntityClientPlayerMP;

import com.waytoogosu.amazo.Wrapper;
import com.waytoogosu.amazo.event.Event;
import com.waytoogosu.amazo.event.events.EventTick;
import com.waytoogosu.amazo.mod.Category;
import com.waytoogosu.amazo.mod.Mod;

public class ModFlight extends Mod{

	public ModFlight() {
		super("F", 0xff757DFF);
		setShowInGuiWithoutSaving(true);
	}

	@Override
	public void registerEvents() {
		super.registerEvent(EventTick.class);
	}

	@Override
	public String getName() {
		return "Flight";
	}

	@Override
	public String getDescription() {
		return "Allows you to fly.";
	}

	@Override
	public Category getCategory() {
		return Category.MOVEMENT;
	}

	private EntityClientPlayerMP getPlayer(){
		return Wrapper.getPlayer();
	}

	@Override
	public void onEnabled(Event e){
		if(e instanceof EventTick){
			getPlayer().capabilities.isFlying = true;
			getPlayer().capabilities.allowFlying = true;
		}
	}

	@Override
	public void onDisable(Event e){
		if(Wrapper.getMc().theWorld == null) return;
		getPlayer().capabilities.isFlying = false;
		if(!Wrapper.getMc().playerController.isInCreativeMode()){
			getPlayer().capabilities.allowFlying = false;
		}
	}

}
