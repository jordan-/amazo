package com.waytoogosu.amazo.mod.mods.movement;

import net.minecraft.src.EntityClientPlayerMP;

import com.waytoogosu.amazo.Wrapper;
import com.waytoogosu.amazo.event.Event;
import com.waytoogosu.amazo.event.events.EventPreMotionUpdate;
import com.waytoogosu.amazo.event.events.EventTick;
import com.waytoogosu.amazo.mod.Category;
import com.waytoogosu.amazo.mod.Mod;

public class ModNofall extends Mod{

	public ModNofall() {
		super("N", 0xff05FF37);
		setShowInGuiWithoutSaving(true);
	}

	@Override
	public void registerEvents() {
		super.registerEvent(EventPreMotionUpdate.class);
	}

	@Override
	public String getName() {
		return "Nofall";
	}

	@Override
	public String getDescription() {
		return "Stops all fall damage.";
	}

	@Override
	public Category getCategory() {
		return Category.MOVEMENT;
	}
	
	@Override
	public void onEnabled(Event e){	
		if(e instanceof EventPreMotionUpdate){
			Wrapper.getValueManager().getValue("onGround").setValue(true);
		}
	}
	
	@Override
	public void onDisable(Event e){
		Wrapper.getValueManager().getValue("onGround").resetValue();
	}

}
