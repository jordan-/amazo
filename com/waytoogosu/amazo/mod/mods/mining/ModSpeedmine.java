package com.waytoogosu.amazo.mod.mods.mining;

import net.minecraft.src.Potion;
import net.minecraft.src.PotionEffect;

import com.waytoogosu.amazo.Wrapper;
import com.waytoogosu.amazo.event.Event;
import com.waytoogosu.amazo.event.events.EventDamageBlock;
import com.waytoogosu.amazo.event.events.EventPostMotionUpdate;
import com.waytoogosu.amazo.event.events.EventPreMotionUpdate;
import com.waytoogosu.amazo.mod.Category;
import com.waytoogosu.amazo.mod.Mod;
import com.waytoogosu.amazo.mod.ModConfiguration;
import com.waytoogosu.amazo.value.ValueManager;

public class ModSpeedmine extends Mod{

	private boolean mining;
	private PotionEffect pe;
	
	public ModSpeedmine() {
		super("V", 0xffFFA200);
		setConfig(new ModConfiguration(this));
		getConfig().addValueWithSlider("Speed", 0.29D, 1, false);
		getConfig().addBoolean("OnGround", false);
		setShowInGuiWithoutSaving(true);
		mining = false;
	}

	@Override
	public void registerEvents() {
		super.registerEvent(EventDamageBlock.class);
		super.registerEvent(EventPreMotionUpdate.class);
		super.registerEvent(EventPostMotionUpdate.class);
	}

	@Override
	public String getName() {
		return "Speedmine";
	}

	@Override
	public String getDescription() {
		return "Makes you mine faster.";
	}

	@Override
	public Category getCategory() {
		return Category.MINING;
	}

	@Override
	public void onEnabled(Event e){
		boolean onGround = getConfig().getBoolean("onground");
		if(e instanceof EventDamageBlock){
			if(onGround){
				mining = true;
				Wrapper.getPlayer().onGround = true;
			}
			EventDamageBlock eodb = (EventDamageBlock) e;
			eodb.setBlockHitDelay(-1);
			eodb.setBreakAtBlockDamage(1.0f - getFloatConfigValue("speed"));
		}

		ValueManager vm = Wrapper.getValueManager();
		if(e instanceof EventPreMotionUpdate && mining && onGround){
			Wrapper.getPlayer().onGround = true;
			vm.getValue("onground").setValue(true);
		}
		if(e instanceof EventPostMotionUpdate && mining && onGround){
			Wrapper.getPlayer().onGround = true;
			vm.getValue("onground").resetValue();
			mining = false;
		}
	}
}
