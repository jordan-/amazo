package com.waytoogosu.amazo.mod.mods.mining;

import net.minecraft.src.Block;
import net.minecraft.src.EnumMovingObjectType;
import net.minecraft.src.Minecraft;
import net.minecraft.src.Packet14BlockDig;
import net.minecraft.src.Packet18Animation;

import com.waytoogosu.amazo.Wrapper;
import com.waytoogosu.amazo.event.Event;
import com.waytoogosu.amazo.event.EventListener;
import com.waytoogosu.amazo.event.events.EventClickBlock;
import com.waytoogosu.amazo.event.events.EventTick;
import com.waytoogosu.amazo.mod.Category;
import com.waytoogosu.amazo.mod.Mod;
import com.waytoogosu.amazo.mod.ModConfiguration;
import com.waytoogosu.amazo.util.Position;

public class ModInstantMiner extends Mod{

	private long nextSendPacket;

	public ModInstantMiner() {
		super("NONE", 0xffFF0000);
		setConfig(new ModConfiguration(this));
		getConfig().addBoolean("Premining", false);
		setShowInGuiWithoutSaving(true);
		setInWorld(true);
	}

	@Override
	public void registerEvents() {
		super.registerEvent(EventClickBlock.class);
		super.registerEvent(EventTick.class);
	}

	@Override
	public String getName() {
		return "InstantMiner";
	}

	@Override
	public String getDescription() {
		return "Mines blocks with just one click.";
	}

	@Override
	public Category getCategory() {
		return Category.MINING;
	}

	@Override
	public void onEnabled(Event e){
		if(e instanceof EventClickBlock){
			EventClickBlock ecb = (EventClickBlock) e;
			Position pos = ecb.getPosition();
			Wrapper.sendPacket(new Packet14BlockDig(2, (int)pos.getPosX(), (int)pos.getPosY(), (int)pos.getPosZ(), ecb.getSideHit()));
			e.setCancelled(true);
		}
		if(e instanceof EventTick && getConfig().getBoolean("Premining")){
			if(System.currentTimeMillis() >= nextSendPacket){
				Minecraft mc = Wrapper.getMc();
				if(mc.objectMouseOver != null){	
					if (mc.objectMouseOver.typeOfHit == EnumMovingObjectType.TILE)
					{
						int x = mc.objectMouseOver.blockX;
						int y = mc.objectMouseOver.blockY;
						int z = mc.objectMouseOver.blockZ;
						int id = mc.theWorld.getBlockId(x, y, z);
						if(id != 6 && id != 31 && id != 32 && id != 37 && id != 38  && id != 39 && id != 40 && id != 50 && id != 46 && id != 75 && id != 76 && id != 59 && id != 83){
							Wrapper.sendPacket(new Packet18Animation(mc.thePlayer, 1));
							Wrapper.sendPacket(new Packet14BlockDig(0, (int)x, (int)y, (int)z, mc.objectMouseOver.sideHit));
							net.minecraft.src.ItemStack itemstack = mc.thePlayer.getCurrentEquippedItem();
							try{
								float f = itemstack.getStrVsBlock(Block.blocksList[id]);
								nextSendPacket = System.currentTimeMillis() + (int)((1000L - (f * 100)) * 1.5)/3;		
							} catch (Exception e1) {}
						}
					}
				}
			}
		}
	}





}
