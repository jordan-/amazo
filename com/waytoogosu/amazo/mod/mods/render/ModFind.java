package com.waytoogosu.amazo.mod.mods.render;

import java.util.ArrayList;
import java.util.List;

import static org.lwjgl.opengl.GL11.*;
import net.minecraft.src.EntityClientPlayerMP;
import net.minecraft.src.EntityPlayer;
import net.minecraft.src.MathHelper;
import net.minecraft.src.Minecraft;
import net.minecraft.src.PathEntity;
import net.minecraft.src.Vec3;

import com.waytoogosu.amazo.Wrapper;
import com.waytoogosu.amazo.command.Command;
import com.waytoogosu.amazo.command.Parameter;
import com.waytoogosu.amazo.event.Event;
import com.waytoogosu.amazo.event.events.EventRenderEntities;
import com.waytoogosu.amazo.mod.Category;
import com.waytoogosu.amazo.mod.Mod;
import com.waytoogosu.amazo.mod.ModType;
import com.waytoogosu.amazo.util.FindLine;
import com.waytoogosu.amazo.util.Position;

public class ModFind extends Mod{

	private PathEntity path;
	private EntityPlayer player;
	private Position position;
	private List<FindLine> lines;

	public ModFind() {
		super(0, 0);
		lines = new ArrayList();
		getCommands().add(new Command(){

			@Override
			public String getName() {
				return "Find";
			}

			@Override
			public String getDescription() {
				return "Finds players.";
			}

			@Override
			public Parameter[] getParams() {
				return new Parameter[] { new Parameter(Parameter.STRING, "player") };
			}

			@Override
			public boolean onCommand(String[] args, String cmd, boolean console) {
				for(Object o : Wrapper.getMc().theWorld.playerEntities){
					EntityPlayer ep = (EntityPlayer) o;
					if(ep.username.equalsIgnoreCase(args[1])){
						player = ep;
						respond("Finding player...", console);
						return true;
					} 
				}
				respond("Player not found.", console);
				return true;
			}

		});
	}

	@Override
	public ModType getType(){
		return ModType.BACKGROUND;
	}

	@Override
	public void registerEvents() {
		super.registerEvent(EventRenderEntities.class);
	}

	@Override
	public String getName() {
		return "Find";
	}

	@Override
	public String getDescription() {
		return "Finds players.";
	}

	@Override
	public Category getCategory() {
		return Category.RENDER;
	}

	@Override
	public void onBackground(Event e){
		if(player != null){
			if(System.currentTimeMillis() % 500 == 0){
				setPath(player.posX, player.posY, player.posZ);
			}

			glPushMatrix();
			getMc().entityRenderer.disableLightmap(1.0D);
			glEnable(3042 /*GL_BLEND*/);
			glDisable(3553 /*GL_TEXTURE_2D*/);
			glDisable(2896 /*GL_LIGHTING*/);
			glDisable(2929 /*GL_DEPTH_TEST*/);
			glDepthMask(false);
			glColor4f(1f, 1f, 1f, 1f);
			glLineWidth(5.0F);
			glBlendFunc(770, 771);
			glEnable(2848 /*GL_LINE_SMOOTH*/);
			glLineWidth(2.0F); 
			for(FindLine fl : lines){
				FindLine line = fl;
				Position v1 = line.getVertexOne();
				Position v2 = line.getVertexTwo();

				double d = v1.getPosX() - getMc().thePlayer.posX;
				double d1 = v1.getPosY() - getMc().thePlayer.posY;
				double d2 = v1.getPosZ() - getMc().thePlayer.posZ;

				double d3 = v2.getPosX() - getMc().thePlayer.posX;
				double d4 = v2.getPosY() - getMc().thePlayer.posY;
				double d5 = v2.getPosZ() - getMc().thePlayer.posZ;

				glBegin(GL_LINES); 
				glVertex3d(d, d1, d2); 
				glVertex3d(d3, d4, d5); 
				glEnd(); 

			}
			glDisable(2848 /*GL_LINE_SMOOTH*/);
			glDepthMask(true);
			glEnable(2929 /*GL_DEPTH_TEST*/);
			glEnable(3553 /*GL_TEXTURE_2D*/);
			glEnable(2896 /*GL_LIGHTING*/);
			glDisable(3042 /*GL_BLEND*/);
			getMc().entityRenderer.enableLightmap(1.0D);
			glPopMatrix();
		}
	}


	private void setPath(double d, double d1, double d2){
		path = getMc().theWorld.getEntityPathToXYZ(getPlayer(), (int) d, (int) d1, (int) d2, 200, false, false, false, false);
		position = new Position((double)getPlayer().posX, (double)getPlayer().posY, (double)getPlayer().posZ);

		calculateAndWalkOnPath();
	}


	private Position lastPos;

	private void calculateAndWalkOnPath(){
		if(path != null) {
			calculateAndWalkOnPath(); 
			return;
		}
		if (!path.isFinished()) {
			Vec3 pos = path.getPosition(getPlayer());

			position.setPosX(pos.xCoord);
			position.setPosY(pos.yCoord + 1.65D);
			position.setPosX(pos.zCoord);

			if(lastPos == null){
				lastPos = new Position(position.getPosX(), position.getPosY(), position.getPosZ());
			} else {
				lines.add(new FindLine(lastPos, new Position(position.getPosX(), position.getPosY(), position.getPosZ())));
				lastPos = null;
			}

			path.incrementPathIndex();
			calculateAndWalkOnPath();
		}
	}

	private EntityClientPlayerMP getPlayer(){
		return Wrapper.getMc().thePlayer;
	}


}
