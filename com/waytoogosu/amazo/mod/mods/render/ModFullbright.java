package com.waytoogosu.amazo.mod.mods.render;

import net.minecraft.src.Minecraft;

import com.waytoogosu.amazo.Wrapper;
import com.waytoogosu.amazo.event.Event;
import com.waytoogosu.amazo.event.events.EventTick;
import com.waytoogosu.amazo.mod.Category;
import com.waytoogosu.amazo.mod.Mod;
import com.waytoogosu.amazo.mod.ModConfiguration;

public class ModFullbright extends Mod{

	private Minecraft mc;
	
	public ModFullbright() {
		super("B", 0xffFFFFB8);
		mc = Wrapper.getMc();
		setShowInGuiWithoutSaving(true);
		setConfig(new ModConfiguration(this));
		getConfig().addValueWithSlider("WorldGamma", 10f, 10f, false);
	}
	
	private long nextGammaChange;

	@Override
	public void onEnabled(Event e) { 
		if(Math.floor(mc.gameSettings.gammaSetting) < Math.floor(getFloatConfigValue("WorldGamma"))) {
			if(System.currentTimeMillis() > nextGammaChange){
				mc.gameSettings.gammaSetting += 0.25f;
				nextGammaChange = System.currentTimeMillis() + 12L;
			}
		}
		if(Math.floor(mc.gameSettings.gammaSetting) > Math.floor(getFloatConfigValue("WorldGamma"))) {
			if(System.currentTimeMillis() > nextGammaChange){
				mc.gameSettings.gammaSetting -= 0.25f;
				nextGammaChange = System.currentTimeMillis() + 12L;
			}
		}
	}
	
	@Override
	public void onDisabled(Event e) {
		if(mc.gameSettings.gammaSetting > 1f) {
			if(System.currentTimeMillis() > nextGammaChange){
				mc.gameSettings.gammaSetting -= 0.25f;
				nextGammaChange = System.currentTimeMillis() + 12L;
			}
		}
	}

	@Override
	public void registerEvents() {
		super.registerEvent(EventTick.class);
	}

	@Override
	public String getName() {
		return "Fullbright";
	}

	@Override
	public String getDescription() {
		return "Maxmizes the world gamma.";
	}

	@Override
	public Category getCategory() {
		return Category.RENDER;
	}

	
}
