package com.waytoogosu.amazo.mod.mods.render;

import net.minecraft.src.*;

import org.lwjgl.input.Keyboard;

import static org.lwjgl.opengl.GL11.*;

import com.waytoogosu.amazo.Wrapper;
import com.waytoogosu.amazo.event.Event;
import com.waytoogosu.amazo.event.events.EventRenderEntities;
import com.waytoogosu.amazo.event.events.EventSetupViewBobbing;
import com.waytoogosu.amazo.mod.Category;
import com.waytoogosu.amazo.mod.Mod;
import com.waytoogosu.amazo.mod.ModConfiguration;
import com.waytoogosu.amazo.util.Position;

public class ModTracers extends Mod{

	public ModTracers() {
		super(Keyboard.KEY_PERIOD, 0xff427D1D);
		setConfig(new ModConfiguration(this));
		setShowInGuiWithoutSaving(true);
		getConfig().addBoolean("Players", true);
		getConfig().addBoolean("Mobs", false);
		getConfig().addBoolean("Boundingboxes", true);
		getConfig().addValueWithSlider("limit", 15, 50, true);
		getConfig().addValueWithSlider("alpha", 0.5f, 1, false);
		setInWorld(true);
	}

	@Override
	public void registerEvents() {
		super.registerEvent(EventSetupViewBobbing.class);
		super.registerEvent(EventRenderEntities.class);
	}

	@Override
	public String getName() {
		return "Tracers";
	}

	@Override
	public String getDescription() {
		return "Draws a tracer from your crosshair to entities.";
	}

	@Override
	public Category getCategory() {
		return Category.RENDER;
	}

	@Override
	public void onEnabled(Event e){
		if(e instanceof EventSetupViewBobbing){
			e.setCancelled(true);
		}
		if(e instanceof EventRenderEntities){
			Minecraft mc = Wrapper.getMc();
			int limit = getIntConfigValue("limit");
			int i = 0;
			mc.entityRenderer.disableLightmap(1.0D);
			glPushMatrix();
			glEnable(3042 /*GL_BLEND*/);
			glDisable(3553 /*GL_TEXTURE_2D*/);
			glDisable(2896 /*GL_LIGHTING*/);
			glDisable(2929 /*GL_DEPTH_TEST*/);
			glDepthMask(false);
			glBlendFunc(770, 771);
			glEnable(2848 /*GL_LINE_SMOOTH*/);
			for(Object o : Wrapper.getMc().theWorld.loadedEntityList){
				Entity ent = (Entity) o;
				if(ent == mc.thePlayer) continue;
				if(ent instanceof EntityPlayer && isConfigBoolean("players")){		
					drawTracerToEntity(ent, 1f - mc.thePlayer.getDistanceToEntity(ent)/300, mc.thePlayer.getDistanceToEntity(ent)/100, 0);
					i++;
				}
				if(ent instanceof EntityLivingBase && !(ent instanceof EntityPlayer) && isConfigBoolean("mobs")){
					boolean entityPassive = isEntityPassive((EntityLivingBase) ent);
					float r = entityPassive ? 0 : 0.5f;
					float g = (entityPassive ? 1f : 0.2f);
					float b = entityPassive ? 0 : 0;
					drawTracerToEntity(ent, r, g, b);
					i++;
				}	
				if(i >= limit) break;
			}
			glDisable(2848 /*GL_LINE_SMOOTH*/);
			glDepthMask(true);
			glEnable(2929 /*GL_DEPTH_TEST*/);
			glEnable(3553 /*GL_TEXTURE_2D*/);
			glEnable(2896 /*GL_LIGHTING*/);
			glDisable(3042 /*GL_BLEND*/);
			glPopMatrix();
			mc.entityRenderer.enableLightmap(1.0D);
		}
	}

	private boolean isEntityPassive(EntityLivingBase e){
		return !(e instanceof EntityMob);
	}

	private void drawTracerToEntity(Entity e, float r, float g, float b){
		drawTracerTo(e, r, g, b, getFloatConfigValue("alpha"));
	}

	private void drawTracerTo(Entity e, float r, float g, float b, float a){
		Minecraft mc = Wrapper.getMc();	
		Entity rve = mc.renderViewEntity;
		AxisAlignedBB bb = e.boundingBox;
		AxisAlignedBB bb1 = rve.boundingBox;
		double x = bb.minX - bb1.minX;
		double y = bb.minY - bb1.minY;
		double z = bb.minZ - bb1.minZ;

		glColor4f(r, g, b, a);
		glLineWidth(1.5f);
		glBegin(GL_LINES);
		glVertex2d(0D, 0D);
		glVertex3d(x, y - 1.5, z);
		glEnd();

		if(isConfigBoolean("Boundingboxes")){
			x = e.posX - RenderManager.renderPosX - 0.5;
			y = e.posY - RenderManager.renderPosY;
			z = e.posZ - RenderManager.renderPosZ - 0.5;
			double height = (e instanceof EntityPlayer || (e instanceof EntityMob && !isEntityPassive((EntityLivingBase) e) && !(e instanceof EntitySpider)) ? 2 : 1.5);

			glLineWidth(1.6f);
			glBegin(GL_LINES);
			glVertex3d(x, y, z);
			glVertex3d(x, y, z + 1);
			glVertex3d(x, y, z);
			glVertex3d(x + 1, y, z);
			glVertex3d(x, y, z);
			glVertex3d(x, y + height, z);
			glVertex3d(x, y + height, z);
			glVertex3d(x + 1, y + height, z);
			glVertex3d(x, y + height, z);
			glVertex3d(x, y + height, z + 1);
			glVertex3d(x + 1, y + height, z);
			glVertex3d(x + 1, y, z);
			glVertex3d(x, y, z + 1);
			glVertex3d(x, y + height, z + 1);
			glVertex3d(x + 1, y + height, z + 1);
			glVertex3d(x + 1, y + height, z);
			glVertex3d(x + 1, y + height, z + 1);
			glVertex3d(x, y + height, z + 1);
			glVertex3d(x + 1, y + height, z + 1);
			glVertex3d(x + 1, y, z + 1);
			glVertex3d(x + 1, y, z + 1);
			glVertex3d(x, y, z + 1);
			glEnd();
		}
	}

}
