package com.waytoogosu.amazo.mod.mods.render;

import net.minecraft.src.EntityLiving;
import net.minecraft.src.EntityLivingBase;

import org.lwjgl.input.Keyboard;

import com.waytoogosu.amazo.Wrapper;
import com.waytoogosu.amazo.event.Event;
import com.waytoogosu.amazo.event.events.EventRenderLivingLabel;
import com.waytoogosu.amazo.mod.Category;
import com.waytoogosu.amazo.mod.Mod;
import com.waytoogosu.amazo.mod.ModConfiguration;

public class ModNametags extends Mod{

	public ModNametags() {
		super(Keyboard.KEY_NONE, 0);
		setConfig(new ModConfiguration(this));
		getConfig().addValueWithSlider("Distance", 64, 64, true);
		getConfig().addBoolean("Scale", true);
		getConfig().addBoolean("SeeThroughWalls", true);
	}

	@Override
	public void registerEvents() {
		super.registerEvent(EventRenderLivingLabel.class);
	}

	@Override
	public String getName() {
		return "Nametags";
	}

	@Override
	public String getDescription() {
		return "Handles nametags.";
	}

	@Override
	public Category getCategory() {
		return Category.RENDER;
	}

	@Override
	public void onEnabled(Event e){
		EventRenderLivingLabel erll = (EventRenderLivingLabel) e;
		if(isConfigBoolean("SeeThroughWalls")){
			erll.setSeeThroughWalls(true);
		}
		if(isConfigBoolean("scale")){
			erll.setScaleMultiplier(getScaleMultiplier(erll.getEntityLiving()));
		}
		erll.setRenderDistance(getIntConfigValue("Distance"));
	}

	private float getScaleMultiplier(EntityLivingBase par1EntityLiving){ 
		EntityLivingBase e = Wrapper.getPlayer();

		if (par1EntityLiving.getDistanceToEntity(e) / 4f <= 3f)
		{
			return 3f;
		} 
		if (par1EntityLiving.getDistanceToEntity(e) / 4f >= 20f)
		{
			return 20f;
		}
		return par1EntityLiving.getDistanceToEntity(e) / 4f;
	}


}
