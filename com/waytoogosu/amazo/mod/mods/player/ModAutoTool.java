package com.waytoogosu.amazo.mod.mods.player;

import org.lwjgl.input.Mouse;

import net.minecraft.src.Block;
import net.minecraft.src.ItemSword;
import net.minecraft.src.EntityPlayer;
import net.minecraft.src.EntityLivingBase;
import net.minecraft.src.EnumMovingObjectType;
import net.minecraft.src.ItemStack;
import net.minecraft.src.MovingObjectPosition;
import net.minecraft.src.Packet205ClientCommand;

import com.waytoogosu.amazo.Wrapper;
import com.waytoogosu.amazo.event.Event;
import com.waytoogosu.amazo.event.events.EventTick;
import com.waytoogosu.amazo.mod.Category;
import com.waytoogosu.amazo.mod.Mod;

public class ModAutoTool extends Mod {
	private int oldSlot = -1;
	private int returnSlot = -1;
	private int hittingBlock;
	private Thread thread = null;

	public ModAutoTool() {
		super("", 0xffff00ff);
	}

	@Override
	public void registerEvents() {
		super.registerEvent(EventTick.class);
	}

	@Override
	public String getName() {
		return "AutoTool";
	}

	@Override
	public String getDescription() {
		return "Automatically wields the correct tool to use";
	}

	@Override
	public Category getCategory() {
		return Category.PLAYER;
	}

	@Override
	public void onEnable(Event e) {
		thread = new Thread(new Runnable() {

			@Override
			public void run() {
				while (!thread.isInterrupted()) {
					if (Wrapper.getMc().currentScreen != null
							|| !Wrapper.getMc().inGameHasFocus)
						continue;

					if (mouseDown()) {
						MovingObjectPosition l = Wrapper.getMc().objectMouseOver;

						if (l != null && l.typeOfHit != null) {
							if (hittingBlock != Wrapper.getMc().theWorld
									.getBlockId(l.blockX, l.blockY, l.blockZ))
								bestTool(l.blockX, l.blockY, l.blockZ);
							else
								hittingBlock = 0;
						} else {
							returnItems();
						}

					} else {
						returnItems();
					}
				}
			}

			private void returnItems() {
				if (oldSlot == -1) {
					return;
				} else if (oldSlot == -2) {
					Wrapper.getPlayer().inventory.currentItem = returnSlot;
					oldSlot = -1;
					return;
				}

				// System.out.println(oldSlot);

				boolean finished = false;

				if (!(oldSlot >= 36 && oldSlot <= 44)) {
					try {
						Wrapper.getMc().playerController.windowClick(
								Wrapper.getPlayer().inventoryContainer.windowId,
								oldSlot, 0, 0, Wrapper.getPlayer());
						Thread.sleep(400);
						Wrapper.getMc().playerController.windowClick(
								Wrapper.getPlayer().inventoryContainer.windowId,
								38, 0, 1, Wrapper.getPlayer());
						Thread.sleep(400);
						Wrapper.getMc().playerController.windowClick(
								Wrapper.getPlayer().inventoryContainer.windowId,
								38, 0, 0, Wrapper.getPlayer());
						Thread.sleep(400);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}

				Wrapper.getPlayer().inventory.currentItem = returnSlot;

				oldSlot = -1;
			}

			public void bestTool(int x, int y, int z) {
				int block = Wrapper.getMc().theWorld.getBlockId(x, y, z);

				if (block == 0) {
					returnItems();
					hittingBlock = 0;
					return;
				}

				if (hittingBlock != block) {
					hittingBlock = block;
				}

				float topStrength = 1.0F;
				int item = -1;

				for (int slot = 0; slot < 45; slot++) {
					ItemStack itemStack = Wrapper.getPlayer().inventoryContainer
							.getSlot(slot).getStack();

					if (itemStack == null) {
						continue;
					}

					float strength = itemStack
							.getStrVsBlock(Block.blocksList[block]);

					if (strength > topStrength) {
						topStrength = strength;
						item = slot;
					}
				}

				if (oldSlot == -1) {
					oldSlot = item;

					if (item >= 36 && item <= 44) {
						oldSlot = -2;
					}

					returnSlot = Wrapper.getPlayer().inventory.currentItem;
				}

				if (item >= 36 && item <= 44) {
					item = item - 36;
					Wrapper.getPlayer().inventory.currentItem = item;
					return;
				}

				if (item == -1 || item == -2)
					return;

				try {
					Wrapper.getMc().playerController.windowClick(
							Wrapper.getPlayer().inventoryContainer.windowId,
							38, 0, 0, Wrapper.getPlayer());
					Thread.sleep(400);
					Wrapper.getMc().playerController.windowClick(
							Wrapper.getPlayer().inventoryContainer.windowId,
							item, 0, 0, Wrapper.getPlayer());
					Thread.sleep(400);
					Wrapper.getMc().playerController.windowClick(
							Wrapper.getPlayer().inventoryContainer.windowId,
							38, 0, 0, Wrapper.getPlayer());
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				Wrapper.getPlayer().inventory.currentItem = 2;

				// bestTool(x, y, z);
			}
		});
		thread.setDaemon(true);
		thread.start();
	}

	@Override
	public void onDisable(Event e) {
		if (!thread.isInterrupted())
			thread.interrupt();
	}

	private boolean mouseDown() {
		return Mouse.isButtonDown(0);
	}
}
