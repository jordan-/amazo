package com.waytoogosu.amazo.mod.mods.player;

import net.minecraft.src.Packet205ClientCommand;

import com.waytoogosu.amazo.Wrapper;
import com.waytoogosu.amazo.event.Event;
import com.waytoogosu.amazo.event.events.EventTick;
import com.waytoogosu.amazo.mod.Category;
import com.waytoogosu.amazo.mod.Mod;

public class ModAutoRespawn extends Mod{

	public ModAutoRespawn() {
		super("", 0xff00ff00);
	}

	@Override
	public void registerEvents() {
		super.registerEvent(EventTick.class);
	}

	@Override
	public String getName() {
		return "AutoRespawn";
	}

	@Override
	public String getDescription() {
		return "Automatically respawns you";
	}

	@Override
	public Category getCategory() {
		return Category.PLAYER;
	}
	
	public void onEnabled(Event e) {
		if(Wrapper.getPlayer().getHealth() <= 0
				|| Wrapper.getPlayer().isDead)
			Wrapper.sendPacket(new Packet205ClientCommand(1));
	}

}
