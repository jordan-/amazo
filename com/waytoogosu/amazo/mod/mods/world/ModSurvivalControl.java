package com.waytoogosu.amazo.mod.mods.world;

import static org.lwjgl.opengl.GL11.GL_LINES;
import static org.lwjgl.opengl.GL11.GL_QUADS;
import static org.lwjgl.opengl.GL11.glBegin;
import static org.lwjgl.opengl.GL11.glBlendFunc;
import static org.lwjgl.opengl.GL11.glColor4f;
import static org.lwjgl.opengl.GL11.glDepthMask;
import static org.lwjgl.opengl.GL11.glDisable;
import static org.lwjgl.opengl.GL11.glEnable;
import static org.lwjgl.opengl.GL11.glEnd;
import static org.lwjgl.opengl.GL11.glLineWidth;
import static org.lwjgl.opengl.GL11.glPopMatrix;
import static org.lwjgl.opengl.GL11.glPushMatrix;
import static org.lwjgl.opengl.GL11.glVertex3d;
import net.minecraft.src.AxisAlignedBB;
import net.minecraft.src.Entity;
import net.minecraft.src.EntityLivingBase;
import net.minecraft.src.EntityPlayer;
import net.minecraft.src.EnumMovingObjectType;
import net.minecraft.src.Minecraft;
import net.minecraft.src.Packet11PlayerPosition;
import net.minecraft.src.Packet14BlockDig;

import com.waytoogosu.amazo.Wrapper;
import com.waytoogosu.amazo.command.Command;
import com.waytoogosu.amazo.command.Parameter;
import com.waytoogosu.amazo.event.Event;
import com.waytoogosu.amazo.event.events.EventClickMiddleMouseButton;
import com.waytoogosu.amazo.event.events.EventRenderEntities;
import com.waytoogosu.amazo.event.events.EventTick;
import com.waytoogosu.amazo.mod.Category;
import com.waytoogosu.amazo.mod.Mod;
import com.waytoogosu.amazo.util.Position;

public class ModSurvivalControl extends Mod{

	public ModSurvivalControl() {
		super("NONE", 0xffD68FFF);
		getCommands().add(new Command(){

			@Override
			public String getName() {
				return "scdestroy";
			}

			@Override
			public String getDescription() {
				return "Initiates survival control";
			}

			@Override
			public Parameter[] getParams() {
				return new Parameter[] {};
			}

			@Override
			public boolean onCommand(String[] args, String cmd, boolean console) {
				destroy();
				return false;
			}
			
		});
	}

	@Override
	public void registerEvents() {		
		super.registerEvent(EventClickMiddleMouseButton.class);
		super.registerEvent(EventTick.class);
		super.registerEvent(EventRenderEntities.class);
	}

	@Override
	public String getName() {
		return "SurvivalControl";
	}

	@Override
	public String getDescription() {
		return "Enables survival control mode.";
	}

	@Override
	public Category getCategory() {
		return Category.WORLD;
	}

	public void drawBoundingBox(AxisAlignedBB axisalignedbb) {
		double x = axisalignedbb.minX;
		double x2 = axisalignedbb.maxX;

		double y = axisalignedbb.minY;
		double y2 = axisalignedbb.maxY;

		double z = axisalignedbb.minZ;
		double z2 = axisalignedbb.maxZ;
		glBegin(GL_QUADS);
		glVertex3d(x, y, z); 
		glVertex3d(x, y2, z);
		glVertex3d(x2, y, z);
		glVertex3d(x2, y2, z);
		glVertex3d(x2, y, z2);
		glVertex3d(x2, y2, z2);
		glVertex3d(x, y, z2);
		glVertex3d(x, y2, z2);
		glEnd();

		glBegin(GL_QUADS);
		glVertex3d(x2, y2, z);
		glVertex3d(x2, y, z);
		glVertex3d(x, y2, z);
		glVertex3d(x, y, z);
		glVertex3d(x, y2, z2);
		glVertex3d(x, y, z2);
		glVertex3d(x2, y2, z2);
		glVertex3d(x2, y, z2);
		glEnd();

		glBegin(GL_QUADS);      
		glVertex3d(x, y2, z);        
		glVertex3d(x2, y2, z);
		glVertex3d(x2, y2, z2);
		glVertex3d(x, y2, z2);
		glVertex3d(x, y2, z);
		glVertex3d(x, y2, z2);
		glVertex3d(x2, y2, z2);
		glVertex3d(x2, y2, z);
		glEnd();

		glBegin(GL_QUADS);       
		glVertex3d(x, y, z);        
		glVertex3d(x2, y, z);
		glVertex3d(x2, y, z2);
		glVertex3d(x, y, z2);
		glVertex3d(x, y, z);
		glVertex3d(x, y, z2);
		glVertex3d(x2, y, z2);
		glVertex3d(x2, y, z);
		glEnd();

		glBegin(GL_QUADS);
		glVertex3d(x, y, z); 
		glVertex3d(x, y2, z);
		glVertex3d(x, y, z2);
		glVertex3d(x, y2, z2);
		glVertex3d(x2, y, z2);
		glVertex3d(x2, y2, z2);
		glVertex3d(x2, y, z);
		glVertex3d(x2, y2, z);
		glEnd();

		glBegin(GL_QUADS);
		glVertex3d(x, y2, z2);
		glVertex3d(x, y, z2);
		glVertex3d(x, y2, z);
		glVertex3d(x, y, z);
		glVertex3d(x2, y2, z);
		glVertex3d(x2, y, z);
		glVertex3d(x2, y2, z2);
		glVertex3d(x2, y, z2);
		glEnd();
	}

	private Position lastChanged, startPosition, endPosition, standPosition;

	@Override
	public void onEnabled(Event e){
		if(e instanceof EventClickMiddleMouseButton){
			if(getMc().objectMouseOver != null && getMc().objectMouseOver.typeOfHit.equals(EnumMovingObjectType.TILE)){
				if(lastChanged == null || lastChanged == endPosition){
					startPosition = new Position(getMc().objectMouseOver.blockX, getMc().objectMouseOver.blockY, getMc().objectMouseOver.blockZ);
					lastChanged = startPosition;
				} else if(lastChanged == startPosition){
					endPosition = new Position(getMc().objectMouseOver.blockX, getMc().objectMouseOver.blockY, getMc().objectMouseOver.blockZ);
					lastChanged = endPosition;

					double d = 0;
					if(startPosition.getPosX() > endPosition.getPosX()){
						d = startPosition.getPosX();
						startPosition.setPosX(endPosition.getPosX());
						endPosition.setPosX(d);
					}
					if(startPosition.getPosY() > endPosition.getPosY()){
						d = startPosition.getPosY();
						startPosition.setPosY(endPosition.getPosY());
						endPosition.setPosY(d);
					}
					if(startPosition.getPosZ() > endPosition.getPosZ()){
						d = startPosition.getPosZ();
						startPosition.setPosZ(endPosition.getPosZ());
						endPosition.setPosZ(d);
					}
					standPosition = new Position(endPosition.getPosX(), endPosition.getPosY() + 1, endPosition.getPosZ());
				}
			}
			e.setCancelled(true);
		}
		if(e instanceof EventRenderEntities){
			if(startPosition != null && endPosition != null){
				double d = startPosition.getPosX() - getMc().thePlayer.posX;
				double d2 = startPosition.getPosY() - getMc().thePlayer.posY;
				double d3 = startPosition.getPosZ() - getMc().thePlayer.posZ;
				double d4 = endPosition.getPosX() + 1 -getMc().thePlayer.posX;
				double d5 = endPosition.getPosY() + 1 -getMc().thePlayer.posY;
				double d6 = endPosition.getPosZ() + 1 - getMc().thePlayer.posZ;
				esp(new AxisAlignedBB(d, d2, d3, d4, d5, d6), 1f, 1f, 1f);
			}
	
			if(standPosition != null){
				double d = standPosition.getPosX() - getMc().thePlayer.posX;
				double d2 = standPosition.getPosY() - getMc().thePlayer.posY;
				double d3 = standPosition.getPosZ() - getMc().thePlayer.posZ;
				esp(new AxisAlignedBB(d, d2, d3, d + 1, d2 + 1, d3 + 1), 0f, 1f, 1f);
			}
		}
	}
	
	private int round(double d){
		return (int)Math.floor(d); 
	} 
	
	private void sleep(long l){
		try {
			Thread.sleep(l);
		} catch (InterruptedException e) {
		}
	}
	
	private void destroy(){
		new Thread(new Runnable(){

			@Override
			public void run() {
				int currentPosX = round(getMc().thePlayer.posX);
				int currentPosY = round(getMc().thePlayer.posY+1);
				int currentPosZ = round(getMc().thePlayer.posZ);
				Wrapper.sendPacket(new Packet14BlockDig(2, (int)endPosition.getPosX(), (int)endPosition.getPosY(), (int)endPosition.getPosZ(),1));				
				sleep(1000L);
				System.out.println("Start");
				for(int y = (int) endPosition.getPosY(); y >=  startPosition.getPosY(); y--){
					for(int z = (int) endPosition.getPosZ(); z >= startPosition.getPosZ(); z--){
						for(int x = (int) endPosition.getPosX(); x >= startPosition.getPosX(); x--){
							if(getMc().theWorld.getBlockId(x,y,z) != 0){
								Wrapper.sendPacket(new Packet14BlockDig(0,x,y,z,1));	
								System.out.println("Break: " + x + " "+y + " "+ z);
								sleep(1000L);
							}
							if(x != (int) startPosition.getPosX()){
							currentPosX--;
							Wrapper.sendPacket(new Packet11PlayerPosition(currentPosX,currentPosY,currentPosY+1,currentPosZ,true));
							System.out.println("X decrease.");
							sleep(1000L);
							}
						}
						for(int x1 = currentPosX; x1 < (int)endPosition.getPosX()+1; x1++){
							Wrapper.sendPacket(new Packet11PlayerPosition(x1,currentPosY,currentPosY+1,currentPosZ,true));
							currentPosX = x1;
							System.out.println("X increase.");
							sleep(1000L);
						}
						if(z != (int) startPosition.getPosX()){
						currentPosZ--;
						Wrapper.sendPacket(new Packet11PlayerPosition(currentPosX,currentPosY,currentPosY+1,currentPosZ,true));
						System.out.println("Z decrease.");
						sleep(1000L);
						}
					}
					for(int z1 = currentPosZ; z1 < endPosition.getPosZ()+1; z1++){
						Wrapper.sendPacket(new Packet11PlayerPosition(currentPosX,currentPosY,currentPosY+1,z1,true));
						currentPosZ = z1;
						System.out.println("Z increase.");
						sleep(1000L);
					}
					if(y != (int)startPosition.getPosY()){
					currentPosY--;
					Wrapper.sendPacket(new Packet11PlayerPosition(currentPosX,currentPosY,currentPosY+1,currentPosZ,true));
					System.out.println("Y decrease.");
					sleep(1000L);
					}
				}
			}
			
		}).start();
	}

	public void drawOutlinedBoundingBox(AxisAlignedBB axisalignedbb) {
		double x = axisalignedbb.minX;
		double x2 = axisalignedbb.maxX;

		double y = axisalignedbb.minY;
		double y2 = axisalignedbb.maxY;

		double z = axisalignedbb.minZ;
		double z2 = axisalignedbb.maxZ;
		glLineWidth(1.0F);

		glBegin(GL_LINES);
		glVertex3d(x, y, z); 
		glVertex3d(x, y2, z);
		glVertex3d(x2, y, z);
		glVertex3d(x2, y2, z);
		glVertex3d(x2, y, z2);
		glVertex3d(x2, y2, z2);
		glVertex3d(x, y, z2);
		glVertex3d(x, y2, z2);
		glEnd();

		glBegin(GL_LINES);
		glVertex3d(x2, y2, z);
		glVertex3d(x2, y, z);
		glVertex3d(x, y2, z);
		glVertex3d(x, y, z);
		glVertex3d(x, y2, z2);
		glVertex3d(x, y, z2);
		glVertex3d(x2, y2, z2);
		glVertex3d(x2, y, z2);
		glEnd();

		glBegin(GL_LINES);      
		glVertex3d(x, y2, z);        
		glVertex3d(x2, y2, z);
		glVertex3d(x2, y2, z2);
		glVertex3d(x, y2, z2);
		glVertex3d(x, y2, z);
		glVertex3d(x, y2, z2);
		glVertex3d(x2, y2, z2);
		glVertex3d(x2, y2, z);
		glEnd();

		glBegin(GL_LINES);       
		glVertex3d(x, y, z);        
		glVertex3d(x2, y, z);
		glVertex3d(x2, y, z2);
		glVertex3d(x, y, z2);
		glVertex3d(x, y, z);
		glVertex3d(x, y, z2);
		glVertex3d(x2, y, z2);
		glVertex3d(x2, y, z);
		glEnd();

		glBegin(GL_LINES);
		glVertex3d(x, y, z); 
		glVertex3d(x, y2, z);
		glVertex3d(x, y, z2);
		glVertex3d(x, y2, z2);
		glVertex3d(x2, y, z2);
		glVertex3d(x2, y2, z2);
		glVertex3d(x2, y, z);
		glVertex3d(x2, y2, z);
		glEnd();

		glBegin(GL_LINES);
		glVertex3d(x, y2, z2);
		glVertex3d(x, y, z2);
		glVertex3d(x, y2, z);
		glVertex3d(x, y, z);
		glVertex3d(x2, y2, z);
		glVertex3d(x2, y, z);
		glVertex3d(x2, y2, z2);
		glVertex3d(x2, y, z2);
		glEnd();

	}

	public void esp(AxisAlignedBB ax, float r, float g, float b)
	{	    
		glPushMatrix();
		glEnable(3042 /*GL_BLEND*/);
		glDisable(3553 /*GL_TEXTURE_2D*/);
		glDisable(2896 /*GL_LIGHTING*/);
		glDisable(2929 /*GL_DEPTH_TEST*/);
		glDepthMask(false);
		glColor4f(r,g, b, 0.12F);
		glLineWidth(3.0F);
		glBlendFunc(770, 771);
		glEnable(2848 /*GL_LINE_SMOOTH*/);
		drawBoundingBox(ax);
		glColor4f(r,g, b, 0.15F);
		glLineWidth(5.0F); 
		drawOutlinedBoundingBox(ax);
		glColor4f(r,g, b, 0.2F);
		glDisable(2848 /*GL_LINE_SMOOTH*/);
		glDepthMask(true);
		glEnable(2929 /*GL_DEPTH_TEST*/);
		glEnable(3553 /*GL_TEXTURE_2D*/);
		glEnable(2896 /*GL_LIGHTING*/);
		glDisable(3042 /*GL_BLEND*/);
		glPopMatrix();
	}

}
