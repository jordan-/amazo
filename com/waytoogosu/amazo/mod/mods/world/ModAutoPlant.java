package com.waytoogosu.amazo.mod.mods.world;

import net.minecraft.src.Block;
import net.minecraft.src.Entity;
import net.minecraft.src.EntityClientPlayerMP;
import net.minecraft.src.EnumMovingObjectType;
import net.minecraft.src.MathHelper;
import net.minecraft.src.Minecraft;
import net.minecraft.src.MovingObjectPosition;
import net.minecraft.src.Packet15Place;
import net.minecraft.src.Packet18Animation;
import net.minecraft.src.Vec3;

import com.waytoogosu.amazo.Wrapper;
import com.waytoogosu.amazo.event.Event;
import com.waytoogosu.amazo.event.events.EventTick;
import com.waytoogosu.amazo.mod.Category;
import com.waytoogosu.amazo.mod.Mod;
import com.waytoogosu.amazo.mod.ModConfiguration;
import com.waytoogosu.amazo.value.ValueManager;

public class ModAutoPlant extends Mod{

	private long nextPlant;

	public ModAutoPlant() {
		super("NONE", 0xffADE480);
		setConfig(new ModConfiguration(this));
		getConfig().addValueWithSlider("Delay", 125L, 500L, true);
		setShowInGuiWithoutSaving(true);
	}

	@Override
	public void registerEvents() {
		super.registerEvent(EventTick.class);
	}

	@Override
	public void onEnabled(Event e){
		if(nextPlant >= System.currentTimeMillis()) return;
		ValueManager vm = Wrapper.getValueManager();	
		double posX = getMc().thePlayer.posX;
		double posY = getMc().thePlayer.posY;
		double posZ = getMc().thePlayer.posZ;
		int range = 3;
		for (int x = range; x > -range; x--)
		{
			boolean stop = false;
			for (int z = range; z > -range; z--)
			{
				for (int y = range; y > -range; y--)
				{
					double d = posX - x;
					double d1 = posY - y;
					double d2 = posZ - z;
					int bX = (int) d;
					int bY = (int) d1;
					int bZ = (int) d2;
					int id = getMc().theWorld.getBlockId(bX, bY, bZ);
					if (id == 60 && getMc().theWorld.getBlockId(bX, bY + 1, bZ) == 0 && getMc().thePlayer.inventory.getCurrentItem() != null)
					{
						float yaw = getYawTo(bX, bY, bZ);
						float pitch = getPitchTo(bX, bY, bZ);
						vm.getValue("rotationYaw").setValue(yaw);
						vm.getValue("rotationPitch").setValue(pitch);
						
						if(System.currentTimeMillis() - nextPlant > 1000) {
							nextPlant = System.currentTimeMillis() + getIntConfigValue("Delay");
							stop = true;
							break;
						}

						int currId = getMc().thePlayer.inventory.getCurrentItem().getItem().itemID;
						if(currId == 115 || currId == 141 || currId == 59 || currId == 142 || currId == 295 || currId == 362 || currId == 392 || currId == 141){
							Wrapper.sendPacket(new Packet18Animation(getMc().thePlayer, 1));
							Wrapper.sendPacket(new Packet15Place(bX, bY, bZ, 1, getMc().thePlayer.inventory.getCurrentItem(), 1f, 1f, 1f));
							nextPlant = System.currentTimeMillis() + getIntConfigValue("Delay");

						}

						nextPlant = System.currentTimeMillis() + getIntConfigValue("Delay");
						stop = true;
					}
				}
				if(stop) break;
			}
			if(stop) break;
		}
		if(System.currentTimeMillis() - nextPlant > 1000L){
			vm.getValue("rotationYaw").resetValue();
			vm.getValue("rotationPitch").resetValue();
		}
	}

	@Override
	public void onDisable(Event e){
		ValueManager vm = Wrapper.getValueManager();	
		vm.getValue("rotationYaw").resetValue();
		vm.getValue("rotationPitch").resetValue();
	}


	private float getYawTo(double par1, double par2, double par3)
	{
		Minecraft mc = Wrapper.getMc();
		EntityClientPlayerMP player = mc.thePlayer;
		double d = par1 - player.posX;
		double d1 = par2 - player.posY;
		double d2 = par3 - player.posZ;
		double d3 = (player.posY + (double)player.getEyeHeight()) - (par2 + 1.05);
		d += 0.5;
		d2 += 0.5;
		double d4 = MathHelper.sqrt_double(d * d + d2 * d2);
		float f = (float)((Math.atan2(d2, d) * 180D) / Math.PI) - 90F;
		return f;
	}

	private float getPitchTo(double par1, double par2, double par3)
	{
		Minecraft mc = Wrapper.getMc();
		EntityClientPlayerMP player = mc.thePlayer;
		double d = par1 - player.posX;
		double d1 = par2 - player.posY;
		double d2 = par3 - player.posZ;
		double d3 = (player.posY + (double)player.getEyeHeight()) - (par2 + 1.05);
		d += 0.5;
		d2 += 0.5;
		double d4 = MathHelper.sqrt_double(d * d + d2 * d2);
		float f1 = (float)(((Math.atan2(d3, d4) * 180D) / Math.PI));
		return f1;
	}

	@Override
	public String getName() {
		return "AutoPlant";
	}

	@Override
	public String getDescription() {
		return "Automatically plants on un used soil.";
	}

	@Override
	public Category getCategory() {
		return Category.WORLD;
	}

}
