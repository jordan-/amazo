package com.waytoogosu.amazo.mod.mods.world;

import net.minecraft.src.Block;
import net.minecraft.src.Entity;
import net.minecraft.src.EntityClientPlayerMP;
import net.minecraft.src.EntityPlayer;
import net.minecraft.src.EnumMovingObjectType;
import net.minecraft.src.MathHelper;
import net.minecraft.src.Minecraft;
import net.minecraft.src.MovingObjectPosition;
import net.minecraft.src.Packet14BlockDig;
import net.minecraft.src.Packet15Place;
import net.minecraft.src.Packet18Animation;
import net.minecraft.src.Vec3;

import com.waytoogosu.amazo.Wrapper;
import com.waytoogosu.amazo.event.Event;
import com.waytoogosu.amazo.event.events.EventClickBlock;
import com.waytoogosu.amazo.event.events.EventPreMotionUpdate;
import com.waytoogosu.amazo.event.events.EventTick;
import com.waytoogosu.amazo.mod.Category;
import com.waytoogosu.amazo.mod.Mod;
import com.waytoogosu.amazo.mod.ModConfiguration;
import com.waytoogosu.amazo.value.ValueManager;

public class ModAutoDestroy extends Mod{

	private long nextDestroy;

	public ModAutoDestroy() {
		super("NONE", 0xff480C6B);
		setConfig(new ModConfiguration(this));
		getConfig().addValue("Id", 0);
		setShowInGuiWithoutSaving(true);
	}

	@Override
	public void registerEvents() {
		super.registerEvent(EventTick.class);
		super.registerEvent(EventClickBlock.class);
	}


	private MovingObjectPosition rayTraceCustom(double d, float f,int pitch, int yaw) {
		EntityClientPlayerMP player = getMc().thePlayer;
		Vec3 vec3d = player.getPosition(f);
		Vec3 vec3d1 = getCustomLook(pitch,yaw);
		Vec3 vec3d2 = vec3d.addVector(vec3d1.xCoord * d, vec3d1.yCoord * d, vec3d1.zCoord * d);
		return getMc().theWorld.clip(vec3d, vec3d2, false);
	}

	private Vec3 getCustomLook(int pitch, int yaw) {
		float f1 = MathHelper.cos(-yaw * 0.01745329F - 3.141593F);
		float f3 = MathHelper.sin(-yaw * 0.01745329F - 3.141593F);
		float f5 = -MathHelper.cos(-pitch * 0.01745329F);
		float f7 = MathHelper.sin(-pitch * 0.01745329F);
		return Vec3.createVectorHelper(f3 * f5, f7, f1 * f5);
	}

	private int fakeYaw;

	@Override
	public void onEnabled(Event e){
		if(e instanceof EventTick){
			if(nextDestroy >= System.currentTimeMillis()) return;	
			
			EntityClientPlayerMP player = getMc().thePlayer;
			double par1 = player.posX;
			double par2 = player.posY;
			double par3 = player.posZ;
			int range = 4;
			for (int x = range; x > -range; x--)
			{
				boolean stop = false;
				for (int z = range; z > -range; z--)
				{
					for (int y = range; y > -range; y--)
					{
						double d = par1 - x;
						double d1 = par2 - y;
						double d2 = par3 - z;
						int i2 = (int) d;
						int j2 = (int) d1;
						int k2 = (int) d2;
						int id = getMc().theWorld.getBlockId(i2, j2, k2);
						if ((id == getIntConfigValue("Id")))
						{
							ValueManager vm = Wrapper.getValueManager();
							vm.getValue("rotationYaw").setValue(getYawTo(i2, j2 -1, k2));
							vm.getValue("rotationPitch").setValue(getPitchTo(i2, j2 -1, k2));

							if(System.currentTimeMillis() - nextDestroy > 1000) {
								nextDestroy = System.currentTimeMillis() + 200L;
								stop = true;
								break;
							}

							Wrapper.sendPacket(new Packet18Animation(player, 1));
							Wrapper.sendPacket(new Packet14BlockDig(0, i2, j2, k2, 1));
							Wrapper.sendPacket(new Packet14BlockDig(2, i2, j2, k2, 1));
							try{
								net.minecraft.src.ItemStack itemstack = getMc().thePlayer.getCurrentEquippedItem();
								float f = itemstack.getStrVsBlock(Block.blocksList[id]);
								nextDestroy = System.currentTimeMillis() + (int)((1500L - (f * 100)) * 1.5)/2;
							} catch (Exception e1) {}
							stop = true;
							break;
						}
					}
					if(stop) break;
				}
				if(stop) break;
			}

			if(System.currentTimeMillis() - nextDestroy > 1000){
				ValueManager vm = Wrapper.getValueManager();
				vm.getValue("rotationYaw").resetValue();
				vm.getValue("rotationPitch").resetValue();
			}
		}
		if(e instanceof EventClickBlock){
	
			EventClickBlock ecb = (EventClickBlock) e;
			if(ecb.getBlock() != null){
				getConfig().setValue("Id", ecb.getBlock().blockID);
				ecb.setCancelled(true);
			}
		}
		/*	

		}*/
	}

	@Override
	public void onDisable(Event e){
		ValueManager vm = Wrapper.getValueManager();	
		vm.getValue("rotationYaw").resetValue();
		vm.getValue("rotationPitch").resetValue();
	}

	private boolean canBlockBeSeen(double x, double y, double z)
	{
		EntityClientPlayerMP player = getMc().thePlayer;
		double posX = player.posX;
		double posY = player.posY;
		double posZ = player.posZ;
		return getMc().theWorld.clip(getMc().theWorld.getWorldVec3Pool().getVecFromPool(posX, posY + (double)player.getEyeHeight(), posZ), getMc().theWorld.getWorldVec3Pool().getVecFromPool(x, y + 0.5, z)) == null;
	}

	private float getYawTo(double par1, double par2, double par3)
	{
		Minecraft mc = Wrapper.getMc();
		EntityClientPlayerMP player = mc.thePlayer;
		double d = par1 - player.posX;
		double d1 = par2 - player.posY;
		double d2 = par3 - player.posZ;
		double d3 = (player.posY + (double)player.getEyeHeight()) - (par2 + 1.05);
		d += 0.5;
		d2 += 0.5;
		double d4 = MathHelper.sqrt_double(d * d + d2 * d2);
		float f = (float)((Math.atan2(d2, d) * 180D) / Math.PI) - 90F;
		return f;
	}

	private float getPitchTo(double par1, double par2, double par3)
	{
		Minecraft mc = Wrapper.getMc();
		EntityClientPlayerMP player = mc.thePlayer;
		double d = par1 - player.posX;
		double d1 = par2 - player.posY;
		double d2 = par3 - player.posZ;
		double d3 = (player.posY + (double)player.getEyeHeight()) - (par2 + 1.05);
		d += 0.5;
		d2 += 0.5;
		double d4 = MathHelper.sqrt_double(d * d + d2 * d2);
		float f1 = (float)(((Math.atan2(d3, d4) * 180D) / Math.PI));
		return f1;
	}
	@Override
	public String getName() {
		return "AutoDestroy";
	}

	@Override
	public String getDescription() {
		return "Automatically destroys blocks of a certain Id.";
	}

	@Override
	public Category getCategory() {
		return Category.WORLD;
	}

}
