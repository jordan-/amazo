package com.waytoogosu.amazo.mod.mods.pvp;

import net.minecraft.src.Item;
import net.minecraft.src.ItemStack;

import com.waytoogosu.amazo.Wrapper;
import com.waytoogosu.amazo.event.Event;
import com.waytoogosu.amazo.event.events.EventTick;
import com.waytoogosu.amazo.mod.Category;
import com.waytoogosu.amazo.mod.Mod;
import com.waytoogosu.amazo.mod.ModConfiguration;

public class ModAutoSoup extends Mod{
	private long currentMS = 0;
    private long lastSoup = -1;
	public int soupID = Item.bowlSoup.itemID;
	
	public ModAutoSoup() {
		super("O", 0xffff0037);
		setShowInGuiWithoutSaving(true);
		setConfig(new ModConfiguration(this));
		getConfig().addValueWithSlider("Delay", 60L, 500L, true);
	}

	@Override
	public void registerEvents() {
		super.registerEvent(EventTick.class);
	}

	@Override
	public String getName() {
		return "AutoSoup";
	}

	@Override
	public String getDescription() {
		return "Automatically eats soup";
	}

	@Override
	public Category getCategory() {
		// TODO Auto-generated method stub
		return Category.PVP;
	}
	
	@Override
	public void onEnabled(Event e){
		currentMS = System.currentTimeMillis();
		
		if(hasDelayRun(getConfig().getIntValue("Delay"))) {
			int slotsAvailable = 0;
			
			for(int slot = 44; slot >= 9; slot--) {
				ItemStack stack = Wrapper.getPlayer().inventoryContainer.getSlot(slot).getStack();
				
				if(slot >= 36 && slot <= 44) {
					if(stack != null && stack.itemID == Item.bowlEmpty.itemID) {
						Wrapper.getMc().playerController.windowClick(Wrapper.getPlayer().inventoryContainer.windowId, slot, 0, 1, Wrapper.getPlayer());
						slotsAvailable++;
					} else if(stack == null) {
						slotsAvailable++;
					}
				} else {
					if(stack != null && stack.itemID == soupID && slotsAvailable > 0) {
						Wrapper.getMc().playerController.windowClick(Wrapper.getPlayer().inventoryContainer.windowId, slot, 0, 1, Wrapper.getPlayer());
						slotsAvailable--;
					}
				}
			}
		
			if(Wrapper.getPlayer().getHealth() > 15)
				return;
			
			int currentItem = Wrapper.getPlayer().inventory.currentItem;
			
			for(int slot = 44; slot >= 36; slot--) {
				ItemStack stack = Wrapper.getPlayer().inventoryContainer.getSlot(slot).getStack();
				
				if(stack != null && stack.itemID  == soupID) {
					Wrapper.getPlayer().inventory.currentItem = slot - 36;
					Wrapper.getMc().playerController.sendUseItem(Wrapper.getPlayer(), Wrapper.getMc().theWorld, Wrapper.getPlayer().inventoryContainer.getSlot(slot).getStack());
					
					lastSoup = System.currentTimeMillis();
					Wrapper.getPlayer().inventory.currentItem = currentItem;
					break;
				}
			}
		}
	}
	
	private boolean hasDelayRun(long l) {
		if(currentMS == 0 || lastSoup == -1) return true;
		return ((currentMS - lastSoup) >= l);
	}
}
