package com.waytoogosu.amazo.mod.mods.pvp;

import net.minecraft.src.EnumMovingObjectType;
import net.minecraft.src.MovingObjectPosition;

import com.waytoogosu.amazo.event.Event;
import com.waytoogosu.amazo.event.events.EventTick;
import com.waytoogosu.amazo.mod.Category;
import com.waytoogosu.amazo.mod.Mod;
import com.waytoogosu.amazo.mod.ModConfiguration;

public class ModTriggerBot extends Mod{

	public ModTriggerBot() {
		super("J", 0xff00FFC3);
		setConfig(new ModConfiguration(this));
		getConfig().addValueWithSlider("Delay", 125L, 500L, true);
		setShowInGuiWithoutSaving(true);
	}

	@Override
	public void registerEvents() {
		super.registerEvent(EventTick.class);
	}

	@Override
	public String getName() {
		return "TriggerBot";
	}

	@Override
	public String getDescription() {
		return "Attacks entities you hover over.";
	}

	@Override
	public Category getCategory() {
		return Category.PVP;
	}
	
	private long nextAttack;
	
	@Override
	public void onEnabled(Event e){
		if(nextAttack >= System.currentTimeMillis()) return;
		MovingObjectPosition objectMouseOver = getMc().objectMouseOver;
		
		if(objectMouseOver != null && objectMouseOver.typeOfHit == EnumMovingObjectType.ENTITY ){
			 getMc().playerController.attackEntity(getMc().thePlayer, objectMouseOver.entityHit);
			 getMc().thePlayer.swingItem();
			 nextAttack = System.currentTimeMillis() + getIntConfigValue("Delay");
		}
	}

}
