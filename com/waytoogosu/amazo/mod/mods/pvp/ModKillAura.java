package com.waytoogosu.amazo.mod.mods.pvp;

import net.minecraft.src.*;

import com.waytoogosu.amazo.Wrapper;
import com.waytoogosu.amazo.event.Event;
import com.waytoogosu.amazo.event.events.EventPostMotionUpdate;
import com.waytoogosu.amazo.event.events.EventPreMotionUpdate;
import com.waytoogosu.amazo.event.events.EventTick;
import com.waytoogosu.amazo.mod.Category;
import com.waytoogosu.amazo.mod.Mod;
import com.waytoogosu.amazo.mod.ModConfiguration;
import com.waytoogosu.amazo.value.ValueManager;

public class ModKillAura extends Mod{

	private long nextAttack, nextReset;
	private Entity target;
	private int slot;
	private boolean needsResetWeapon;

	public ModKillAura() {
		super("G", 0xff05FF37);
		setShowInGuiWithoutSaving(true);
		setConfig(new ModConfiguration(this));
		getConfig().addBoolean("Players", false);
		getConfig().addBoolean("Mobs", false);
		getConfig().addBoolean("Both", true);
		getConfig().addBoolean("Criticals", false);
		getConfig().addBoolean("Block", false);
		getConfig().addBoolean("Autoweapon", true);
		getConfig().addBoolean("Resetweapon", true);
		getConfig().addValueWithSlider("Range", 4D, 6D, false);
		getConfig().addValueWithSlider("Delay", 125L, 500L, true);
		needsResetWeapon = false;
	}

	@Override
	public void registerEvents() {
		super.registerEvent(EventPreMotionUpdate.class);
		super.registerEvent(EventPostMotionUpdate.class);
		super.registerEvent(EventTick.class);
	}

	@Override
	public String getName() {
		return "KillAura";
	}

	@Override
	public String getDescription() {
		return "Attacks all entities around you";
	}

	@Override
	public Category getCategory() {
		return Category.PVP;
	}

	@Override
	public void onEnabled(Event e){	
		EntityClientPlayerMP tp  = Wrapper.getPlayer();
		if(e instanceof EventPreMotionUpdate && isDelayOver()){
			target = getClosestEntity();
			if(target != null){
				ValueManager vm = Wrapper.getValueManager();
				if(isConfigBoolean("Autoweapon")){
					weaponSwitch();
				}
				ItemStack is = Wrapper.getMc().thePlayer.getCurrentEquippedItem();
				if(is != null &&isConfigBoolean("Block")){
					Item var1 = is.getItem();
					if(var1.equals(Item.swordWood) || var1.equals(Item.swordStone) || var1.equals(Item.swordIron) || var1.equals(Item.swordGold) || var1.equals(Item.swordDiamond)){
						Wrapper.getMc().clickMouse(1);
					}
				}
				Wrapper.getMc().playerController.attackEntity(tp, target);
				tp.swingItem();
				if(tp.ridingEntity == null && !tp.isRidingHorse()){
				vm.getValue("rotationyaw").setValue(getYawTo(target));
				vm.getValue("rotationpitch").setValue(getPitchTo(target));
				} else {
					tp.rotationYaw = getYawTo(target);
					tp.rotationPitch = getPitchTo(target);
				}
				if(tp.onGround && isConfigBoolean("Criticals")
						&& !tp.isInWater()
						&& !tp.isInsideOfMaterial(Material.lava)
						&& !tp.isInsideOfMaterial(Material.web)){
					tp.motionY += 0.2f;
			        tp.onGround = false;
				}
			}
		}
		if(e instanceof EventPostMotionUpdate){
			if(target != null){	
				nextReset = System.currentTimeMillis() + 1000L;
				target = null;
			} else {
				if(System.currentTimeMillis() >= nextReset){
					if(isConfigBoolean("Resetweapon") && needsResetWeapon){
						Wrapper.getMc().thePlayer.inventory.currentItem = slot;
						needsResetWeapon = false;
					}
					Wrapper.getValueManager().getValue("rotationyaw").resetValue();
					Wrapper.getValueManager().getValue("rotationpitch").resetValue();
				}
			}
		}
	}
	
	@Override
	public void onDisable(Event e){
		Wrapper.getValueManager().getValue("rotationyaw").resetValue();
		Wrapper.getValueManager().getValue("rotationpitch").resetValue();
	}


	private boolean isDelayOver() {
		long millis = System.currentTimeMillis();
		if(nextAttack < millis) {
			nextAttack = millis + getIntConfigValue("Delay");
			return true;
		}
		return false;
	}

	private Entity getClosestEntity(){
		Minecraft mc = Wrapper.getMc();
		EntityClientPlayerMP player = mc.thePlayer;
		Entity target = null;
		for(Object o : mc.theWorld.loadedEntityList){
			Entity e = (Entity) o; 
			if(e == player  || e instanceof EntityHorse && player.isRidingHorse() && e == player.ridingEntity|| !player.canEntityBeSeen(e) || e.isDead || !(e instanceof EntityLivingBase) || e instanceof AbstractClientPlayer && !(isConfigBoolean("Players") || isConfigBoolean("Both")) || !(e instanceof EntityPlayer) && !(isConfigBoolean("Mobs") || isConfigBoolean("Both")) || player.getDistanceToEntity(e) > getDoubleConfigValue("Range")) continue;
			if(target == null){
				target = e;
				continue;
			}
			float yawPitch = getYawPitch();
			if(Math.abs(getYawPitchTo(target) - yawPitch)> Math.abs(getYawPitchTo(e) - yawPitch)){
				target = e;
			}
		}
		return target;
	}

	private float getYawPitch(){
		ValueManager vm = Wrapper.getValueManager();
		float yaw = vm.getValue("rotationYaw").getFloatValue();
		float pitch = vm.getValue("rotationPitch").getFloatValue();
		return (float) Math.sqrt((yaw * yaw) + (pitch * pitch));
	}

	private float getYawPitchTo(Entity e){
		float yaw = getYawTo(e);
		float pitch = getPitchTo(e);
		return (float) Math.sqrt((yaw * yaw) + (pitch * pitch));
	}

	private void weaponSwitch(){
		InventoryPlayer inventory = Wrapper.getMc().thePlayer.inventory;
		if(!needsResetWeapon){
			slot = inventory.currentItem;
		}
		float highestDmg = 1;
		float dmg = 0;
		int newItem = -1;
		for (int i = 0; i < 9; i++) {
			ItemStack stack = inventory.mainInventory[i];

			if (stack == null) {
				continue;
			}

			if(stack.getItem() instanceof ItemSword){
				dmg = ((ItemSword) stack.getItem()).getWeaponDamage();
			}

			if(stack.getItem() instanceof ItemTool){
				dmg = ((ItemTool) stack.getItem()).getWeaponDamage();
			}


			if (dmg > highestDmg) {
				newItem = i;
				highestDmg = dmg;
			}
		}
		if (newItem < 0) {
			return;
		}
		inventory.currentItem = newItem;
		needsResetWeapon = true;
	}

	private float getYawTo(Entity e)
	{
		Minecraft mc = Wrapper.getMc();
		EntityClientPlayerMP player = mc.thePlayer;
		double d = e.posX - player.posX;
		double d1 = e.posY - player.posY;
		double d2 = e.posZ - player.posZ;
		double d3 = (player.posY + (double)player.getEyeHeight()) - (e.posY + (double)e.getEyeHeight());
		double d4 = MathHelper.sqrt_double(d * d + d2 * d2);
		float f = (float)((Math.atan2(d2, d) * 180D) / Math.PI) - 90F;
		return f;
	}

	private float getPitchTo(Entity e)
	{
		Minecraft mc = Wrapper.getMc();
		EntityClientPlayerMP player = mc.thePlayer;
		double d = e.posX - player.posX;
		double d1 = e.posY - player.posY;
		double d2 = e.posZ - player.posZ;
		double d3 = (player.posY + (double)player.getEyeHeight()) - (e.posY + (double)e.getEyeHeight());
		double d4 = MathHelper.sqrt_double(d * d + d2 * d2);
		float f1 = (float)(((Math.atan2(d3, d4) * 180D) / Math.PI));
		return f1;
	}

}
