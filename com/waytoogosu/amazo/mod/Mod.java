package com.waytoogosu.amazo.mod;

import java.util.ArrayList;
import java.util.List;

import javax.security.auth.login.Configuration;

import net.minecraft.src.Minecraft;

import org.lwjgl.input.Keyboard;

import com.waytoogosu.amazo.Wrapper;
import com.waytoogosu.amazo.command.Command;
import com.waytoogosu.amazo.event.*;
import com.waytoogosu.amazo.ui.panel.PanelComponentSlider;

public abstract class Mod implements EventListener{

	private int keybind, color;
	private boolean enabled, showInGui, togglableByCommand, inWorld, lastEnabled;
	private List<EventListenerData> eventListenerData;
	private List<PanelComponentSlider> sliders;
	private List<Command> commands;
	private ModConfiguration config;

	public Mod(String keybind, int color){
		this(Keyboard.getKeyIndex(keybind), color);
	}
	
	public Mod(int keybind, int color){
		this.keybind = keybind;
		this.enabled = false;
		this.showInGui = false;
		this.togglableByCommand = true;
		this.inWorld = true;
		this.color = color;
		this.sliders = new ArrayList();
		this.eventListenerData = new ArrayList();
		this.commands = new ArrayList();
		this.lastEnabled = enabled;
		//sliders = new ArrayList();
		registerEvents();
	}

	/*public void addSlider(String valueName, double maxValue, boolean round){
		sliders.add(new PanelModSlider(valueName, maxValue/100D, 0, 0, round, this));
	}

	public List<PanelModSlider> getSliders(){
		return sliders;
	}*/
	
	public void addSlider(String name, double multiplier, boolean round){
		sliders.add(new PanelComponentSlider(null, name, multiplier, round, this));
	}

	public List<PanelComponentSlider> getSliders(){
		return sliders;
	}

	public List<EventListenerData> getEventListenerData(){
		return eventListenerData;
	}
	
	protected Minecraft getMc(){
		return Wrapper.getMc();
	}

	public abstract void registerEvents();

	public int getKeybind() {
		return keybind;
	}

	public void setKeybind(int keybind) {
		this.keybind = keybind;
		Wrapper.getSaveManager().getSave("Mods").save();
	}

	public int getColor() {
		return color;
	}

	public void setColor(int color) {
		this.color = color;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public boolean isInWorld(){
		return inWorld;
	}

	public void setInWorld(boolean inWorld){
		this.inWorld = inWorld;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
		Wrapper.getSaveManager().getSave("Mods").save();
		addRemoveEnabledMods();
	}

	public boolean isShowInGui() {
		return showInGui;
	}

	public void setShowInGui(boolean showInGui) {
		this.showInGui = showInGui;
		Wrapper.getSaveManager().getSave("Mods").save();
	}

	protected void registerEvent(Class<? extends Event> eventClass, EventPriority priority){
		eventListenerData.add(new EventListenerData(this, eventClass, priority));
	}

	protected void registerEvent(Class<? extends Event> eventClass){
		registerEvent(eventClass, EventPriority.NORMAL);
	}

	public void onEnable(Event e) {}
	public void onDisable(Event e) {}
	public void onEnabled(Event e) {}
	public void onDisabled(Event e) {}

	@Override
	public void onEvent(Event e) {
		if(isInWorld() && Wrapper.getMc().theWorld == null) return;
		if(lastEnabled != enabled){
			if(ModType.TOGGLE.equals(getType())){
			if(enabled){
				onEnable(e);
			} else {
				onDisable(e);
			}
			} else if(ModType.ACTION.equals(getType())){
				onAction(e);
			}
			lastEnabled = enabled;
		}
		if(ModType.BACKGROUND.equals(getType())){
			onBackground(e);
		} else if(ModType.TOGGLE.equals(getType())){
			if(isEnabled()){
				onEnabled(e);
			} else {
				onDisabled(e);
			}
		}
	}

	public abstract String getName();
	//public abstract String getAuthor();
	public abstract String getDescription();
	public abstract Category getCategory();
	
	public void toggle() {
		this.enabled = !this.enabled;
		Wrapper.getSaveManager().getSave("Mods").save();
		addRemoveEnabledMods();
	}

	public void addRemoveEnabledMods(){
		ModManager modManager = Wrapper.getModManager();
		if(isEnabled()){
			modManager.getEnabledMods().add(this);
		} else {
			modManager.getEnabledMods().remove(this);
		}
	}

	public void setEnabledWithoutSaving(boolean enabled){
		this.enabled = enabled;
		addRemoveEnabledMods();
	}

	public void setShowInGuiWithoutSaving(boolean showInGui){
		this.showInGui = showInGui;
	}

	public void setKeybindWithoutSaving(int keybind){
		this.keybind = keybind;
	}

	public boolean isConfigBoolean(String name){
		return config.getBoolean(name);
	}

	public double getDoubleConfigValue(String name){
		return config.getValue(name);			
	}

	public int getIntConfigValue(String name){
		return (int) config.getValue(name);			
	}

	public float getFloatConfigValue(String name){
		return (float) config.getValue(name);			
	}

	public ModConfiguration getConfig() {
		return config;
	}

	public void setConfig(ModConfiguration config) {
		this.config = config;
	}

	public List<Command> getCommands() {
		return commands;
	}

	public boolean isTogglableByCommand() {
		return togglableByCommand;
	}

	public void setTogglableByCommand(boolean togglableByCommand) {
		this.togglableByCommand = togglableByCommand;
	}

	public ModType getType(){
		return ModType.TOGGLE;
	}

	public void onBackground(Event e) {}
	
	public void onAction(Event e) {}


}

