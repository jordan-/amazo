package com.waytoogosu.amazo;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

public class Downloader {

	public static void downloadFile(File output, String link) {
		try
		{
			URL url = new URL(link);
			url.openConnection();
			InputStream reader = url.openStream();

			FileOutputStream writer = new FileOutputStream(output);
			byte[] buffer = new byte[153600];
			int totalBytesRead = 0;
			int bytesRead = 0;
			while ((bytesRead = reader.read(buffer)) > 0)
			{  
				writer.write(buffer, 0, bytesRead);
				buffer = new byte[153600];
				totalBytesRead += bytesRead;
			}
			long endTime = System.currentTimeMillis();
			writer.close();
			reader.close();

		}
		catch (MalformedURLException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

}
