package com.waytoogosu.amazo.save.saves;

import java.util.HashMap;
import java.util.List;

import com.waytoogosu.amazo.Wrapper;
import com.waytoogosu.amazo.mod.Mod;
import com.waytoogosu.amazo.save.Save;
import com.waytoogosu.amazo.save.SaveManager;

public class ModConfigSave extends Save{

	public ModConfigSave(SaveManager saveManager) {
		super(saveManager);
	}

	@Override
	public List getList() {
		return Wrapper.getModManager().getMods();
	}

	@Override
	public String getName() {
		return "ModConfigs";
	}

	@Override
	public String[] getOutputFromObject(Object o) {
		Mod mod = (Mod) o;

		if(mod.getConfig() == null) return new String[] {mod.getName()};

		HashMap<String, Boolean> booleans = mod.getConfig().getBooleans();
		HashMap<String, Double> values = mod.getConfig().getValues();
		String output[] = new String[booleans.size() + values.size() + 1];

		output[0] = mod.getName();

		int i = 1;
		for(String s : booleans.keySet()){
			output[i] = s + "=" + booleans.get(s);
			i++;
		}

		for(String s : values.keySet()){
			output[i] = s + "=" + values.get(s);
			i++;
		}
		return output;
	}

	@Override
	public String getSplitString() {
		return ":";
	}

	@Override
	public void load(String[] args, String s) {
		Mod mod = null;
		for(Mod m : Wrapper.getModManager().getMods()){
			if(m.getName().equalsIgnoreCase(args[0])){
				mod = m;
				break;	
			}
		}
		for(String s1 : args){
			if(mod == null) break;
			if(mod.getConfig() == null) continue;
			try { 
			String split[] = s1.split("=");
			String key = split[0];
				String state = split[1];
			if(mod.getConfig().getBooleans().containsKey(key)){
				System.out.println(mod.getName() + " boolean found named " + key);

				mod.getConfig().setBooleanWithoutSaving(key, Boolean.parseBoolean(state));
			}
			} catch (Exception e) {}
		}
	}


}
