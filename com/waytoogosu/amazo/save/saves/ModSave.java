package com.waytoogosu.amazo.save.saves;

import java.util.List;

import com.waytoogosu.amazo.Wrapper;
import com.waytoogosu.amazo.mod.Mod;
import com.waytoogosu.amazo.save.Save;
import com.waytoogosu.amazo.save.SaveManager;

public class ModSave extends Save{

	public ModSave(SaveManager saveManager) {
		super(saveManager);
	}

	@Override
	public List getList() {
		return Wrapper.getModManager().getMods();
	}

	@Override
	public String getName() {
		return "Mods";
	}

	@Override
	public String[] getOutputFromObject(Object o) {
		Mod mod = (Mod) o;
		return new String[] {mod.getName(), "e=" + mod.isEnabled(), "s=" + mod.isShowInGui(), "k=" + mod.getKeybind()};
	}

	@Override
	public String getSplitString() {
		return ":";
	}

	@Override
	public void load(String[] args, String s) {
		Mod mod = null;
		for(Mod m : Wrapper.getModManager().getMods()){
			if(m.getName().equalsIgnoreCase(args[0])){
				mod = m;
				break;
			}
		}
		if(mod == null) return;
		mod.setEnabledWithoutSaving(Boolean.parseBoolean(args[1].substring(2)));
		mod.setShowInGuiWithoutSaving(Boolean.parseBoolean(args[2].substring(2)));
		mod.setKeybindWithoutSaving(Integer.parseInt(args[3].substring(2)));
	}

}
