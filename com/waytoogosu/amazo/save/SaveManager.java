package com.waytoogosu.amazo.save;

import java.io.File;
import java.util.HashMap;

import com.waytoogosu.amazo.save.saves.ModConfigSave;
import com.waytoogosu.amazo.save.saves.ModSave;

import net.minecraft.src.Minecraft;

public class SaveManager {

	private File clientDirectory;
	private HashMap<String, Save> saves;
	
	public SaveManager(){
		saves = new HashMap<String, Save>();
		clientDirectory = new File(Minecraft.getMinecraft().mcDataDir, "/Amazo");
		
		if(!clientDirectory.exists()) clientDirectory.mkdir();
		registerSaves();	
	}
	
	public void registerSaves(){
		addSave(new ModSave(this));
		addSave(new ModConfigSave(this));
	}
	
	private void addSave(Save save){
		saves.put(save.getName(), save);
	}
	
	public File getClientDirectory(){
		return clientDirectory;
	}

	public Save getSave(String name){
		return saves.get(name);
	}
	
}
