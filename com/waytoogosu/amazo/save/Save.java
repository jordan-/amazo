package com.waytoogosu.amazo.save;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import com.waytoogosu.amazo.Wrapper;

public abstract class Save {

	private SaveManager saveManager;
	
	public Save(SaveManager saveManager){
		this.saveManager = saveManager;
		load();
	}
	
	public abstract List getList();
	public abstract String getName();
	public abstract String[] getOutputFromObject(Object o);
	public abstract String getSplitString();
	public abstract void load(String args[], String s);

	public File getDirectory(){
		return new File(saveManager.getClientDirectory(), getName() + ".cfg");
	}
	
	public void save(){
		try {
			BufferedWriter writer = new BufferedWriter(new FileWriter(getDirectory()));
			for(Object o : getList()){
				String s  = "";
				for(String s1 : getOutputFromObject(o)){
					s += s1 + getSplitString();
				}
				writer.write(s + "\r\n");
			}
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void load(){
		try {
			if(!getDirectory().exists())
				getDirectory().createNewFile();
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		
		try {
			BufferedReader reader = new BufferedReader(new FileReader(getDirectory()));

			String s;
			while((s = reader.readLine()) != null){
				load(s.split(getSplitString()), s);
			}
			reader.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
