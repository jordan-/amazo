package com.waytoogosu.amazo;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.MalformedURLException;
import java.net.URL;

import javax.swing.JOptionPane;

public class UpdateManager {

	public static boolean OUTDATED;
	
	public UpdateManager(){
		boolean authorized = false;
		try {
		    URL url = new URL("https://dl.dropboxusercontent.com/u/27876767/amazo/version.txt");
		    BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
		    String str = in.readLine();
		    double onlineVersion = Double.parseDouble(str);
		    
		    if(onlineVersion > Amazo.VERSION) OUTDATED = true;
		    in.close();
		} catch (MalformedURLException e) {
		} catch (IOException e) {
		}
		
		if(OUTDATED){
			
			new Thread(new Runnable(){

				@Override
				public void run() {
					File f;
					Downloader.downloadFile(f = new File(Wrapper.getSaveManager().getClientDirectory(), "AmazoUpdater.jar"), "https://dl.dropboxusercontent.com/u/27876767/amazo/AmazoUpdater.jar");
					try {

					    String command = "java -jar " + f.getAbsolutePath();
					    Process child = Runtime.getRuntime().exec(command);
					} catch (IOException e) {
					}
					JOptionPane.showMessageDialog(null, "A newer version is avaiable, if you did not see the Amazo Updater open, please manually run it at .minecraft/Amazo/AmazoUpdater.jar");
				}
				
			}).start();
			
			
			
		}
	}
	
}
