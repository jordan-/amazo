package com.waytoogosu.amazo.hooks;

import com.waytoogosu.amazo.Wrapper;
import com.waytoogosu.amazo.event.events.*;
import com.waytoogosu.amazo.util.Position;
import com.waytoogosu.amazo.value.ValueManager;

import net.minecraft.src.EntityClientPlayerMP;
import net.minecraft.src.MathHelper;
import net.minecraft.src.Minecraft;
import net.minecraft.src.NetClientHandler;
import net.minecraft.src.Packet10Flying;
import net.minecraft.src.Packet11PlayerPosition;
import net.minecraft.src.Packet12PlayerLook;
import net.minecraft.src.Packet13PlayerLookMove;
import net.minecraft.src.Packet19EntityAction;
import net.minecraft.src.Packet27PlayerInput;
import net.minecraft.src.Session;
import net.minecraft.src.World;

public class HookEntityClientPlayerMP extends EntityClientPlayerMP{

	public HookEntityClientPlayerMP(Minecraft par1Minecraft, World par2World,
			Session par3Session, NetClientHandler par4NetClientHandler) {
		super(par1Minecraft, par2World, par3Session, par4NetClientHandler);
	}

	@Override
	public void onUpdate(){
		if (this.worldObj.blockExists(MathHelper.floor_double(this.posX), 0, MathHelper.floor_double(this.posZ)))
		{
			super.onUpdate();

			if (this.isRiding())
			{
				EventPreMotionUpdate epmu = (EventPreMotionUpdate) Wrapper.getEventManager().onEvent(new EventPreMotionUpdate());
				if(!epmu.isCancelled()){
					ValueManager vm = Wrapper.getValueManager();
					this.sendQueue.addToSendQueue(new Packet12PlayerLook(vm.getValue("rotationYaw").getFloatValue(), vm.getValue("rotationPitch").getFloatValue(), vm.getValue("onGround").isValue()));
					this.sendQueue.addToSendQueue(new Packet27PlayerInput(this.moveStrafing, this.moveForward, this.movementInput.jump, this.movementInput.sneak));
				}
				Wrapper.getEventManager().onEvent(new EventPostMotionUpdate());
			}
			else
			{
				this.sendMotionUpdates();
			}
		}
	}

	@Override
	public void sendMotionUpdates()
	{
		EventPreMotionUpdate epmu = (EventPreMotionUpdate) Wrapper.getEventManager().onEvent(new EventPreMotionUpdate());
		if(!epmu.isCancelled()){

			/**/
			ValueManager vm = Wrapper.getValueManager();
			boolean sOnGround = this.onGround;
			boolean sSneaking = this.isSneaking();
			float sRotationYaw = this.rotationYaw;
			float sRotationPitch = this.rotationPitch;
			double sPosX = this.posX;
			double sPosY = this.posY;
			double sPosZ = this.posZ;
			this.setSneaking(vm.getValue("sneaking").isValue());
			this.onGround = vm.getValue("onGround").isValue();
			Position position = vm.getValue("pos").getPositionValue();
			this.posX = position.getPosX();
			this.posY = position.getPosY();
			this.posZ = position.getPosZ();
			this.rotationYaw = vm.getValue("rotationYaw").getFloatValue();
			this.rotationPitch = vm.getValue("rotationPitch").getFloatValue();	
			/**/

			boolean var1 = this.isSprinting();

			if (var1 != this.wasSneaking)
			{
				if (var1)
				{
					this.sendQueue.addToSendQueue(new Packet19EntityAction(this, 4));
				}
				else
				{
					this.sendQueue.addToSendQueue(new Packet19EntityAction(this, 5));
				}

				this.wasSneaking = var1;
			}

			boolean var2 = this.isSneaking();

			if (var2 != this.shouldStopSneaking)
			{
				if (var2)
				{
					this.sendQueue.addToSendQueue(new Packet19EntityAction(this, 1));
				}
				else
				{
					this.sendQueue.addToSendQueue(new Packet19EntityAction(this, 2));
				}

				this.shouldStopSneaking = var2;
			}

			double var3 = this.posX - this.oldPosX;
			double var5 = this.boundingBox.minY - this.oldMinY;
			double var7 = this.posZ - this.oldPosZ;
			double var9 = (double)(this.rotationYaw - this.oldRotationYaw);
			double var11 = (double)(this.rotationPitch - this.oldRotationPitch);
			boolean var13 = var3 * var3 + var5 * var5 + var7 * var7 > 9.0E-4D || this.field_71168_co >= 20;
			boolean var14 = var9 != 0.0D || var11 != 0.0D;

			if (this.ridingEntity != null)
			{
				this.sendQueue.addToSendQueue(new Packet13PlayerLookMove(this.motionX, -999.0D, -999.0D, this.motionZ, this.rotationYaw, this.rotationPitch, this.onGround));
				var13 = false;
			}
			else if (var13 && var14)
			{
				this.sendQueue.addToSendQueue(new Packet13PlayerLookMove(this.posX, this.boundingBox.minY, this.posY, this.posZ, this.rotationYaw, this.rotationPitch, this.onGround));
			}
			else if (var13)
			{
				this.sendQueue.addToSendQueue(new Packet11PlayerPosition(this.posX, this.boundingBox.minY, this.posY, this.posZ, this.onGround));
			}
			else if (var14)
			{
				this.sendQueue.addToSendQueue(new Packet12PlayerLook(this.rotationYaw, this.rotationPitch, this.onGround));
			}
			else
			{
				this.sendQueue.addToSendQueue(new Packet10Flying(this.onGround));
			}

			++this.field_71168_co;
			this.wasOnGround = this.onGround;

			if (var13)
			{
				this.oldPosX = this.posX;
				this.oldMinY = this.boundingBox.minY;
				this.oldPosY = this.posY;
				this.oldPosZ = this.posZ;
				this.field_71168_co = 0;
			}

			if (var14)
			{
				this.oldRotationYaw = this.rotationYaw;
				this.oldRotationPitch = this.rotationPitch;
			}

			/**/
			this.rotationYaw = sRotationYaw;
			this.rotationPitch = sRotationPitch;	
			this.posX = sPosX;
			this.posY = sPosY;
			this.posZ = sPosZ;
			setSneaking(sSneaking);
			this.onGround = sOnGround;
			/**/
		}
		Wrapper.getEventManager().onEvent(new EventPostMotionUpdate());
	}

}
