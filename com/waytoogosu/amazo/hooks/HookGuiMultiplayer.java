package com.waytoogosu.amazo.hooks;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import com.waytoogosu.amazo.ui.accountSwitcher.GuiAccountSwitcher;

import net.minecraft.src.GuiButton;
import net.minecraft.src.GuiMultiplayer;
import net.minecraft.src.GuiScreen;
import net.minecraft.src.I18n;

public class HookGuiMultiplayer extends GuiMultiplayer{
	public HookGuiMultiplayer(GuiScreen par1GuiScreen) {
		super(par1GuiScreen);
	}

	@Override
	public void initGuiControls(){
		super.initGuiControls();
		
		int i = 0;
		
		List<Object> buttons = new CopyOnWriteArrayList(buttonList);
		
		for(Object o : buttons) {
			GuiButton b = (GuiButton) o;
			
			if(b.id == 1 || b.id == 3 || b.id == 4) { // top row buttons
				buttons.remove(b);
				
				if(b.id != 1) {
					b.xPosition = this.width / 2 + ((b.id == 3) ? -74 : 4);
				}
			
				if(b.id == 4) {
					b.displayString = "Direct";
				}
			
				buttons.add(new GuiButton(b.id, b.xPosition, b.yPosition, 70, 20, b.displayString));
			}
			
			i++;
		}
		
		buttons.add(new GuiButton(1337, this.width / 2 + 4 + 76, this.height - 52, 75, 20, "Alt Manager"));
		buttonList = buttons;
	}
	
	@Override
	protected void actionPerformed(GuiButton par1GuiButton){
		if (par1GuiButton.id == 8)
        {
            this.mc.displayGuiScreen(new HookGuiMultiplayer(null));
            return;
        }
		
		if (par1GuiButton.id == 1337)
		{
			this.mc.displayGuiScreen(new GuiAccountSwitcher(this));
			return;
		}
		
		super.actionPerformed(par1GuiButton);
	}
}
