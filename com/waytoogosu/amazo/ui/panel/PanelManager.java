package com.waytoogosu.amazo.ui.panel;

import java.util.ArrayList;
import java.util.List;

import com.waytoogosu.amazo.Wrapper;
import com.waytoogosu.amazo.mod.Mod;
import com.waytoogosu.amazo.mod.ModType;

public class PanelManager {

	public static final int PANEL_WIDTH = 104;
	public static final int PANEL_BAR_SIZE = 12;
	
	private List<Panel> panels;

	public PanelManager(){
		panels = new ArrayList();

		registerPanels();
		
		int posX = 0, panelsPerLine = Wrapper.getWidth()/PANEL_WIDTH, panelsOnLine = 0, line = 0;
		Panel[] panelLine = new Panel[panelsPerLine];
		for(Panel p : getPanels()){
			panelsOnLine++;
			p.setPosX(posX);	
			posX += PANEL_WIDTH;
			if(line != 0){
				Panel panel = panelLine[panelsOnLine - 1];
				p.setPosY(panel.getPosY() + panel.getHeight() + panel.getBarSize());
			}
			if(panelsOnLine <= panelsPerLine){
				panelLine[panelsOnLine - 1] = p;
				if(panelsPerLine == panelsOnLine){
					posX = 0;
					panelsOnLine = 0;
					line++;
				}
			} 			
		}
	}

	private void registerPanels(){
		for(Mod m : Wrapper.getModManager().getMods()){
			if(m.getType() != ModType.TOGGLE) continue;
			Panel p = null;
			for(Panel panel : panels){
				if(panel.getName() == m.getCategory().getName()){
					p = panel;
					break;
				}
			}
			if(p == null){
				panels.add(p = new Panel(m.getCategory(), PANEL_WIDTH, PANEL_BAR_SIZE));
				p.setOpen(true);
			}
			p.addMod(m);
		}
	}

	public List<Panel> getPanels(){
		return panels;
	}

	public void drawPanels(){
		for(Panel p : panels){
			p.draw();
		}
	}

	public void handleMouseInput(){
		for(Panel p : panels){
			p.handleMouseInput();
		}
	}

	public void keyTypePanels(char par1, int par2){
		for(Panel p : panels){
			p.onKeyTyped(par1, par2);
		}
	}

	public void clickPanels(int par1, int par2, int par3){
		Panel panel = null;
		for(Panel p : panels){
			if(p.onClick(par1, par2)){
				panel = p;				
				break;
			}			
		}
		if(panel != null){
			panels.remove(panel);
			panels.add(panel);
		}
	}

}
