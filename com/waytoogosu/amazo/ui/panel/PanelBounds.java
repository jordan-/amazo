package com.waytoogosu.amazo.ui.panel;

import com.waytoogosu.amazo.Wrapper;
import com.waytoogosu.amazo.util.Bounds;

public class PanelBounds extends Bounds{

	private Panel panel;
	
	public PanelBounds(Panel panel, int posX, int posY, int width, int height) {
		super(posX, posY, width, height);
		this.panel = panel;
	}
	
	public int getPosXRelativeToPanel(){
		return getPosX() + panel.getPosX();
	}
	
	public int getPosYRelativeToPanel(){
		return getPosY() + panel.getPosY();
	}
	
	@Override
	public boolean isMouseOverBounds(){
		return !panel.isObstructed() && Wrapper.isMouseOver(getPosXRelativeToPanel(), getPosXRelativeToPanel() + getWidth(), getPosYRelativeToPanel(), getPosYRelativeToPanel() + getHeight());
	}


	
}
