package com.waytoogosu.amazo.ui.panel;

import org.lwjgl.input.Mouse;

import com.waytoogosu.amazo.Wrapper;
import com.waytoogosu.amazo.font.AmazoUnicodeFont;
import com.waytoogosu.amazo.font.FontManager;
import com.waytoogosu.amazo.mod.Mod;

public class PanelComponentSlider extends PanelComponent{

	private Mod mod;
	private String name;
	private double realValue, multiplier, value, defaultValue;
	private int posX, posY;
	private boolean dragging, round;
	private PanelConfig panel;
	private PanelBounds bounds, sliderBounds, resetBounds;

	public PanelComponentSlider(PanelConfig panel, String name, double multiplier, boolean round, Mod mod){
		this.name = name;
		this.realValue = mod.getConfig().getValue(getName());
		this.multiplier = multiplier;
		this.dragging = false;
		this.round = round;
		this.mod = mod;
		this.value = (double)realValue/(double)multiplier;
		fixValue();
		defaultValue = getRealValue();
		this.panel = panel;
	}

	public void setBounds(){
		bounds = new PanelBounds(panel, 0, panel.getBarSize() + panel.getHeight(), panel.getWidth(), 20);
	}

	public void fixValue(){
		if(value > 100){
			value = 100;
		} else if(value < 0){
			value = 0;
		}
	}

	public double getRealValue() {
		return realValue;
	}

	public void setPanelConfig(PanelConfig panel){
		this.panel = panel;
	}

	@Override
	public String getName() {
		return name;
	}


	@Override
	public void onClick(int x, int y) {
		if(sliderBounds.isMouseOverBounds()){
			dragging = true;
			drag();
		}
		if(resetBounds.isMouseOverBounds()){
			this.value = ((double)defaultValue/(double)multiplier);
			this.realValue = (value * multiplier);
			updateRealValue();
		}
	}

	@Override
	public void draw() {
		FontManager fm = Wrapper.getFontManager();

		PanelBounds bounds = getBounds();
		bounds.drawBorderedRect(0.5f, 0x90000000, 0x905c5c5c);

		String value = Double.toString(realValue);
		if(round){
			value = Long.toString(Math.round(Double.parseDouble(value)));
		} else {
			if(value.length() > 3){
				value = value.substring(0, 4);
			}
		}
		String s = "\247f" + name + " [\247e" + value + "\247f]";
		AmazoUnicodeFont nameFont = fm.getFont("Myriad", 14);
		nameFont.drawString(s, getBounds().getPosX() + 2, getBounds().getPosY() + 1, 0);

		if(sliderBounds == null){
			sliderBounds = new PanelBounds(panel, 2, getBounds().getPosY() + 3 + nameFont.getStringHeight(s), 100, 4);
		}
		sliderBounds.drawBorderedRect(0.5f, 0x90000000, 0x905c5c5c);

		AmazoUnicodeFont labelFont = fm.getFont("Myriad", 10);
		String str = "RESET";
		int width = labelFont.getStringWidth(str) + 2;
		int height = labelFont.getStringHeight(str) + 1;
		if(resetBounds == null){
			resetBounds = new PanelBounds(panel, bounds.getEndPosX() - width - 2, getBounds().getPosY() + 3, width, height);
		}
		resetBounds.drawBorderedRect(1.5f, 0x40000000, 0x505c5c5c);
		labelFont.drawString((resetBounds.isMouseOverBounds() && !Mouse.isButtonDown(0)? "\247e" : "\247f")+ str, resetBounds.getPosX() + resetBounds.getWidth()/2 - labelFont.getStringWidth(str)/2, resetBounds.getPosY() + resetBounds.getHeight()/2 - labelFont.getStringHeight(str)/2, 0xffffffff);
		Wrapper.getUiMethods().drawRect(sliderBounds.getPosX() + 1, sliderBounds.getPosY() + 1,  sliderBounds.getPosX() + 1 + (int)this.value, sliderBounds.getEndPosY() - 1, 0x993879C2);
	}

	private void updateRealValue(){
		mod.getConfig().setValue(getName(), getRealValue());
	}

	@Override
	public void handleMouseInput(){
		if(dragging){
			drag();
		}
	}

	private void drag(){
		int l = Wrapper.getMouseX();
		if(Mouse.isButtonDown(0))
		{
			int newValue = l - panel.getPosX() - sliderBounds.getPosX();

			if(newValue > 100){
				newValue = 100;
			}
			if(newValue < 0){
				newValue = 0;
			}
			this.value = newValue;
			this.realValue = value * multiplier;
			panel.setDoNotRemove(true);
		} else
		{
			updateRealValue();
			this.dragging = false;
		}
	}

	@Override
	public PanelBounds getBounds() {
		return bounds;
	}

}
