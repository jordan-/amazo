package com.waytoogosu.amazo.ui.panel;

import java.awt.Font;

import org.lwjgl.input.Mouse;

import com.waytoogosu.amazo.Wrapper;
import com.waytoogosu.amazo.font.AmazoUnicodeFont;
import com.waytoogosu.amazo.mod.Mod;

public class PanelComponentModButton extends PanelComponent{

	private Mod mod;
	private Panel panel;
	private PanelBounds bounds;
	private PanelConfig panelConfig;

	public PanelComponentModButton(Panel panel, Mod mod){
		this.mod = mod;	
		this.panel = panel;
		bounds = new PanelBounds(panel, 0, panel.getBarSize() + panel.getHeight(), panel.getWidth(), 15);

		this.panelConfig = new PanelConfig(panel, this);
	}
	
	public Mod getMod(){
		return mod;
	}

	@Override
	public String getName() {
		return "Buttons";
	}

	@Override
	public PanelBounds getBounds() {
		return bounds;
	}

	@Override
	public void onClick(int x, int y) {
		if(Mouse.isButtonDown(0)){
			mod.toggle();
		}
		else if(Mouse.isButtonDown(1)){
			panel.setPanelConfig(panelConfig);
			int posX = panel.getPosX() + panel.getWidth() + 1;
			
			if(posX >= Wrapper.getWidth() - panel.getWidth()){
				posX = panel.getPosX() - panelConfig.getWidth() - 1;
			}
			panelConfig.setPosX(posX);
			panelConfig.setPosY(panel.getPosY());
		}
	}

	@Override
	public void draw() {
		PanelBounds bounds = getBounds();
		bounds.drawBorderedRect(0.5f, 0x90000000, mod.isEnabled() ? 0x90A1CAF7 : 0x905c5c5c);
		AmazoUnicodeFont font = Wrapper.getFontManager().getFont("Verdana", Font.TRUETYPE_FONT, 13);
		font.drawString(mod.getName(), bounds.getPosX() + (bounds.getWidth())/2 - font.getStringWidth(mod.getName())/2, bounds.getPosY() + (bounds.getHeight())/2 - font.getStringHeight(mod.getName())/2, 0xffffffff);
	}

	@Override
	public void drawMouseHover() {
		PanelBounds bounds = getBounds();
		bounds.drawBorderedRect(0.5f, 0x90000000, 0xff3879C2);
		AmazoUnicodeFont font = Wrapper.getFontManager().getFont("Verdana", Font.TRUETYPE_FONT, 13);
		font.drawString(mod.getName(), bounds.getPosX() + (bounds.getWidth())/2 - font.getStringWidth(mod.getName())/2, bounds.getPosY() + (bounds.getHeight())/2 - font.getStringHeight(mod.getName())/2, 0xffffffff);
	}

}
