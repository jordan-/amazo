package com.waytoogosu.amazo.ui.panel;

import java.awt.Font;

import org.lwjgl.input.Mouse;

import com.waytoogosu.amazo.Wrapper;
import com.waytoogosu.amazo.font.AmazoUnicodeFont;
import com.waytoogosu.amazo.mod.Mod;

public class PanelComponentModConfigButton extends PanelComponent{

	private Mod mod;
	private Panel panel;
	private PanelBounds bounds;
	private String name;

	public PanelComponentModConfigButton(Panel panel, Mod mod, String name){
		this.name = name;
		this.mod = mod;	
		this.panel = panel;
		bounds = new PanelBounds(panel, 0, panel.getBarSize() + panel.getHeight(), panel.getWidth(), 15);
	}
	
	public Mod getMod(){
		return mod;
	}

	@Override
	public String getName() {
		return "Buttons";
	}

	@Override
	public PanelBounds getBounds() {
		return bounds;
	}

	@Override
	public void onClick(int x, int y) {
		if(Mouse.isButtonDown(0)){
			mod.getConfig().setBoolean(name, !mod.isConfigBoolean(name));
		}
	}

	@Override
	public void draw() {
		String s = Character.toString(name.charAt(0)).toUpperCase() + name.substring(1);
		PanelBounds bounds = getBounds();
		bounds.drawBorderedRect(0.5f, 0x90000000, mod.isConfigBoolean(s) ? 0x90A1CAF7 : 0x905c5c5c);
		AmazoUnicodeFont font = Wrapper.getFontManager().getFont("Verdana", Font.TRUETYPE_FONT, 13);
		font.drawString(s, bounds.getPosX() + (bounds.getWidth())/2 - font.getStringWidth(s)/2, bounds.getPosY() + (bounds.getHeight())/2 - font.getStringHeight(s)/2, 0xffffffff);
	}

	@Override
	public void drawMouseHover() {
		String s = Character.toString(name.charAt(0)).toUpperCase() + name.substring(1);
		PanelBounds bounds = getBounds();
		bounds.drawBorderedRect(0.5f, 0x90000000, 0xff3879C2);
		AmazoUnicodeFont font = Wrapper.getFontManager().getFont("Verdana", Font.TRUETYPE_FONT, 13);
		font.drawString(s, bounds.getPosX() + (bounds.getWidth())/2 - font.getStringWidth(s)/2, bounds.getPosY() + (bounds.getHeight())/2 - font.getStringHeight(s)/2, 0xffffffff);
	}

}
