package com.waytoogosu.amazo.ui.panel;

import java.awt.Font;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;

import com.waytoogosu.amazo.Wrapper;
import com.waytoogosu.amazo.font.AmazoUnicodeFont;
import com.waytoogosu.amazo.mod.Mod;

public class PanelConfig extends Panel{

	public static final int PANEL_CONFIG_BARSIZE = 24;

	private Panel panel;
	private PanelComponentModButton componentModButton;
	private boolean settingKeybind;
	private boolean doNotRemove, remove;
	private long removeAt;

	public PanelConfig(final Panel panel, PanelComponentModButton pcmb) {
		super(pcmb.getMod().getName(), panel.getWidth(), panel.getBarSize());
		doNotRemove = false;
		this.componentModButton = pcmb;
		this.panel = panel;
		this.barSize = PANEL_CONFIG_BARSIZE;

		final int w = getWidth();
		final int bs = getBarSize();
		final Panel p = this;

		components.clear();
		final AmazoUnicodeFont nameFont = Wrapper.getFontManager().getFont("Myriad", 15);
		final String name = p.getName().toUpperCase();
		components.add(dragArea = new PanelComponent(){
			@Override
			public String getName() {
				return "Drag Area";
			}

			@Override
			public PanelBounds getBounds() {
				return new PanelBounds(p, 0, 0, w, bs);
			}

			@Override
			public void onClick(int x, int y) {}

			@Override
			public void draw() {
				PanelBounds bounds = getBounds();
				bounds.drawGradientRect(0xFF4696Ef, 0xff3879C2);
				bounds.drawHollowBorderedRect(0.5f, 0x90000000);

				String s = name;
				nameFont.drawString("\247f" + s, p.getWidth()/2 - nameFont.getStringWidth(s)/2, 1, 0);
			}
		});

		final PanelComponent lastComponent;
		final AmazoUnicodeFont font = Wrapper.getFontManager().getFont("Verdana", Font.TRUETYPE_FONT, 12);
		components.add(lastComponent = new PanelComponent(){

			@Override
			public String getName() {
				return "Keybind";
			}

			@Override
			public PanelBounds getBounds() {
				String s = "Keybind: " + Keyboard.getKeyName(getMod().getKeybind());
				return new PanelBounds(p, 2, nameFont.getStringHeight(name) + 1, font.getStringWidth(s), font.getStringHeight(s) - 3);
			}

			@Override
			public void onClick(int x, int y) {
				settingKeybind =! settingKeybind;
			}

			@Override
			public void draw() {		
				String s = settingKeybind ? "Set Me, ESCAPE Clears Me!" : "Keybind: " + Keyboard.getKeyName(getMod().getKeybind());
				font.drawString("\247f" + s, getBounds().getPosX(), getBounds().getPosY(), 0);
			}

			@Override
			public void drawMouseHover() {		
				font.drawString("\247aClick Me", getBounds().getPosX(), getBounds().getPosY(), 0);
			}

			@Override
			public void handleMouseInput(){
				if(panel.getPanelConfig() == null){
					settingKeybind = false;
				}
			}

			@Override
			public void onKeyTyped(char par1, int par2){
				if(!settingKeybind) return;
				if(par2 == Keyboard.KEY_ESCAPE){
					Wrapper.getUiScreen().setCancelCloseUiScreen(true);
					getMod().setKeybind(Keyboard.KEY_NONE);
				} else {
					getMod().setKeybind(par2);
				}
				settingKeybind = false;
			}
		});


		components.add(new PanelComponent(){
			@Override
			public String getName() {
				return "Show In Gui";
			}

			@Override
			public PanelBounds getBounds() {
				String s = "Show In Gui: " + getMod().isShowInGui();
				return new PanelBounds(p, 2, lastComponent.getBounds().getEndPosY() + 1, font.getStringWidth(s), font.getStringHeight(s) + 1);
			}

			@Override
			public void onClick(int x, int y) {
				getMod().setShowInGui(!getMod().isShowInGui());
			}

			@Override
			public void draw() {		
				String s = "Show In Gui: " + getMod().isShowInGui();
				font.drawString("\247f" + s, getBounds().getPosX(), getBounds().getPosY(), 0);
			}

			@Override
			public void drawMouseHover() {		
				String s = "Show In Gui: " + getMod().isShowInGui();
				font.drawString("\247aToggle me!", getBounds().getPosX(), getBounds().getPosY(), 0);
			}
		});

		addButtons();
		addSliders();
	}

	public void setDoNotRemove(boolean flag){
		this.doNotRemove = flag;
	}

	public boolean isDoNotRemove(){
		return doNotRemove;
	}

	private void addButtons(){
		if(getMod().getConfig() == null) return;
		for(String s : getMod().getConfig().getBooleans().keySet()){
			PanelComponent pc = null;
			components.add(pc = new PanelComponentModConfigButton(this, getMod(), s));
			height += pc.getBounds().getHeight();
		}
	}

	private void addSliders(){
		if(getMod().getConfig() == null) return;
		for(PanelComponentSlider pcs : getMod().getSliders()){
			pcs.setPanelConfig(this);
			pcs.setBounds();
			components.add(pcs);
			height += pcs.getBounds().getHeight();
		}

	}

	private Mod getMod(){
		return componentModButton.getMod();
	}
	
	@Override
	public void draw(){
		this.obstructed = false;
		super.draw();
		if(remove && System.currentTimeMillis() > removeAt){
			remove = false;
			panel.setPanelConfig(null);
		}
	}

	@Override
	public void handleMouseInput(){
		for(PanelComponent pc : components){
			pc.handleMouseInput();
		}
		PanelBounds bounds = new PanelBounds(this, -1, 0, getWidth(), panel.getBounds().getHeight() > barSize + getHeight() ? panel.getBounds().getHeight() : barSize + getHeight());
		if(!componentModButton.getBounds().isMouseOverBounds() && !bounds.isMouseOverBounds()) {
			if(!doNotRemove && !remove){
				remove = true;			
				removeAt = System.currentTimeMillis() + 500L;
			} else {
				doNotRemove = false;
			}
			settingKeybind = false;
		} else {
			remove = false;
		}
	}

	@Override
	public boolean onClick(int x, int y){
		for(PanelComponent pc : components){
			if(pc == dragArea) continue;
			if(pc.getBounds().isMouseOverBounds()){		
				pc.onClick(x, y);
				return true;
			}
		}
		if(dragArea.getBounds().isMouseOverBounds()){
			dragArea.onClick(x, y);
			return true;
		}
		return false;
	}

}
