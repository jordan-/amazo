package com.waytoogosu.amazo.ui.panel;

import java.util.ArrayList;
import java.util.List;







import org.lwjgl.input.Mouse;

import static org.lwjgl.opengl.GL11.*;

import com.waytoogosu.amazo.Wrapper;
import com.waytoogosu.amazo.font.AmazoUnicodeFont;
import com.waytoogosu.amazo.mod.Category;
import com.waytoogosu.amazo.mod.Mod;

public class Panel {

	private int posX, posY, width;
	protected int barSize;
	protected int height;
	private int index;
	protected List<PanelComponent> components;
	private String name;
	private boolean open, pinned;
	protected boolean obstructed;
	protected PanelComponent dragArea;
	private PanelConfig panelConfig;

	public Panel(Category c, int width, int barSize) {
		this(c.getName(), width, barSize);
	}
	
	public Panel(String category, int width, int barSize){
		this.width = width;
		this.barSize = barSize;
		this.name = category;
		this.open = false;
		this.pinned = false;
		this.obstructed = false;

		final int w = width;
		final int bs = barSize;
		final Panel panel = this;

		components = new ArrayList();
		components.add(dragArea = new PanelComponent(){

			int xOffset, yOffset;
			boolean dragging;

			@Override
			public String getName() {
				return "Drag Area";
			}

			@Override
			public PanelBounds getBounds() {
				return new PanelBounds(panel, 0, 0, w, bs);
			}

			@Override
			public void onClick(int x, int y) {
				xOffset = x - panel.getPosX();
				yOffset = y - panel.getPosY();
				dragging = true;
			}

			@Override
			public void draw() {
				PanelBounds bounds = getBounds();
				bounds.drawGradientRect(0xFF4696Ef, 0xff3879C2);
				bounds.drawHollowBorderedRect(0.5f, 0x90000000);

				AmazoUnicodeFont f = Wrapper.getFontManager().getFont("Myriad", 15);
				f.drawString("\247f" + panel.getName().toUpperCase(), 2, 0, 0);
			}

			@Override
			public void handleMouseInput(){
				if(!dragging) return;
				if(Mouse.isButtonDown(0))
				{

					setPosX(Wrapper.getMouseX() - xOffset);
					setPosY(Wrapper.getMouseY() - yOffset);


				} else {
					dragging = false;
				}
				setPanelConfig(null);
			}

		});
		components.add(new PanelComponent(){
			@Override
			public String getName() {
				return "Open";
			}

			@Override
			public PanelBounds getBounds() {
				return new PanelBounds(panel, w - 10, 2, 8, 8);
			}

			@Override
			public void onClick(int x, int y) {
				open =! open;
			}

			@Override
			public void draw() {
				PanelBounds bounds = getBounds();
				bounds.drawBorderedRect(0.5f, 0x90000000, open ? 0x90000000 : 0x905c5c5c);

			}

			@Override
			public void drawMouseHover(){
				PanelBounds bounds = getBounds();
				bounds.drawBorderedRect(0.5f, 0x90000000, 0xff3879C2);		
			}
		});
		components.add(new PanelComponent(){
			@Override
			public String getName() {
				return "Pinned";
			}

			@Override
			public PanelBounds getBounds() {
				return new PanelBounds(panel, w - 20, 2, 8, 8);
			}

			@Override
			public void onClick(int x, int y) {
				pinned =! pinned;
			}

			@Override
			public void draw() {
				PanelBounds bounds = getBounds();
				bounds.drawBorderedRect(0.5f, 0x90000000, pinned ? 0x90000000 : 0x905c5c5c);

			}

			@Override
			public void drawMouseHover(){
				PanelBounds bounds = getBounds();
				bounds.drawBorderedRect(0.5f, 0x90000000, 0xff3879C2);		
			}
		});

	}

	public boolean isPinned(){
		return pinned;
	}

	public boolean isObstructed(){
		return obstructed;
	}

	public boolean isOpen(){
		return open;
	}

	public void addMod(Mod mod){
		components.add(new PanelComponentModButton(this, mod));
		height += 15;
	}

	public void draw(){
		glPushMatrix();
		glTranslatef(posX, posY, 0);
		for(PanelComponent pc : components){
			if(pc instanceof PanelComponentModButton && !open) continue;
			if(pc.getBounds().isMouseOverBounds() && !Mouse.isButtonDown(0)){
				pc.drawMouseHover();
			} else {
				pc.draw();
			}
		}
		glPopMatrix();

		if(panelConfig != null){
			panelConfig.draw();
		}
	}

	public int getPosX() {
		return posX;
	}

	public void setPosX(int posX) {
		this.posX = posX;
	}

	public int getPosY() {
		return posY;
	}

	public void setPosY(int posY) {
		this.posY = posY;
	}

	public String getName(){
		return name;
	}

	public void handleMouseInput(){
		for(PanelComponent pc : components){
			pc.handleMouseInput();
		}
		updateObstructed(Wrapper.getMouseX(), Wrapper.getMouseY());
		if(panelConfig != null){
			panelConfig.handleMouseInput();
		}
	}

	private void updateObstructed(int i, int j){
		List<Panel> panels = Wrapper.getUiScreen().getPanelManager().getPanels();
		for(int k = 0; k < panels.size(); k++){
			Panel p = panels.get(k);
			if(p == this) continue;
			PanelConfig pc = p.getPanelConfig();
			int index = getIndex(this);
			if(!p.isOpen()){
				if(i >= p.getPosX() && i <= p.getPosX() + p.getWidth() + (pc != null ? pc.getWidth() : 0) && j >= p.getPosY() && j <= p.getPosY() + getBarSize() && k > index){
					obstructed = true;
					return;
				}
			} else if(i >= p.getPosX() && i <= p.getPosX() + p.getWidth() + (pc != null ? pc.getWidth() : 0) && j >= p.getPosY() && j <= p.getPosY() + getBarSize() + p.getHeight() && k > index){
				obstructed = true;
				return;
			}

			if(pc != null){
				if(i >= pc.getPosX() && i <= pc.getPosX() + pc.getWidth() && j >= pc.getPosY() && j <= pc.getPosY() + pc.getBarSize() + pc.getHeight() && k > index){
					obstructed = true;
					return;
				}
			}
		}
		obstructed = false;
	}

	public int getIndex(Panel p){
		for(int i = 0 ; i < Wrapper.getUiScreen().getPanelManager().getPanels().size(); i++){
			Panel p1 = Wrapper.getUiScreen().getPanelManager().getPanels().get(i);
			if(p == p1){
				return i;
			}
		}
		return -1;
	}

	public void onKeyTyped(char par1, int par2){
		for(PanelComponent pc : components){
			pc.onKeyTyped(par1, par2);
		}
		if(panelConfig != null){
			panelConfig.onKeyTyped(par1, par2);
		}
	}

	public boolean onClick(int x, int y) {
		if(isObstructed()) return false;
		for(PanelComponent pc : components){
			if(pc == dragArea || pc instanceof PanelComponentModButton && !open) continue;
			if(pc.getBounds().isMouseOverBounds()){
				pc.onClick(x, y);
				return true;
			}
		}

		if(dragArea.getBounds().isMouseOverBounds()){
			dragArea.onClick(x, y);
			return true;
		}

		if(panelConfig != null){
			return panelConfig.onClick(x, y);
		}

		return false;
	}

	public PanelBounds getBounds(){
		return new PanelBounds(this, 0, 0, getWidth(), (open ? getHeight() : 0) + getBarSize());
	}

	public void setOpen(boolean open){
		this.open = open;
	}

	public int getBarSize(){
		return barSize;
	}

	public int getHeight(){
		return height;
	}

	public int getWidth(){
		return width;
	}

	public PanelConfig getPanelConfig(){
		return panelConfig;
	}

	public void setPanelConfig(PanelConfig panelConfig){
		this.panelConfig = panelConfig;
	}

}
