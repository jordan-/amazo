package com.waytoogosu.amazo.ui.panel;

public abstract class PanelComponent {

	public abstract String getName();
	public abstract PanelBounds getBounds();
	public abstract void onClick(int x, int y);

	public abstract void draw();
	
	public void handleMouseInput(){}
	
	public void onKeyTyped(char par1, int par2){}
	
	public void drawMouseHover(){
		draw();
	}
	
}
