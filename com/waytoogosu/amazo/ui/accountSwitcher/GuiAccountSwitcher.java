package com.waytoogosu.amazo.ui.accountSwitcher;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import javax.swing.JFileChooser;

import org.lwjgl.input.Keyboard;

import com.waytoogosu.amazo.Wrapper;
import com.waytoogosu.amazo.util.account.Account;
import com.waytoogosu.amazo.util.account.Accounts;

import net.minecraft.src.GuiButton;
import net.minecraft.src.GuiScreen;
import net.minecraft.src.GuiTextField;
import net.minecraft.src.I18n;

public class GuiAccountSwitcher extends GuiScreen {
	protected GuiScreen parentGui;
	private GuiAccountList altList;
	public GuiTextField field;
	
	public GuiAccountSwitcher(GuiScreen parent) {
		parentGui = parent;
	}
	
	public void initGui() {
		altList = new GuiAccountList(this);
		altList.registerScrollButtons(Keyboard.KEY_DOWN, Keyboard.KEY_UP);
		
		field = new GuiTextField(mc.fontRenderer, width / 2 - 75 - 39, height - 60, 227, 14);
		
        this.buttonList.add(new GuiButton(0, this.width / 2 - 154, this.height - 52, 100, 20, "Login"));
        this.buttonList.add(new GuiButton(1, this.width / 2 + 4 + 50, this.height - 52, 100, 20, "Add"));
        this.buttonList.add(new GuiButton(2, this.width / 2 - 50, this.height - 52, 100, 20, "Direct Login"));
        this.buttonList.add(new GuiButton(3, this.width / 2 - 154, this.height - 28, 70, 20, "Open List"));
        this.buttonList.add(new GuiButton(4, this.width / 2 - 74, this.height - 28, 70, 20, "Delete"));
        this.buttonList.add(new GuiButton(5, this.width / 2 + 4, this.height - 28, 70, 20, "Refresh"));
        this.buttonList.add(new GuiButton(6, this.width / 2 + 4 + 76, this.height - 28, 75, 20, "Back"));
       
        ((GuiButton) buttonList.get(0)).enabled = altList.getCurrentSlot() >= 0;
        ((GuiButton) buttonList.get(4)).enabled = altList.getCurrentSlot() >= 0;
	}
	
	public void drawScreen(int par1, int par2, float par3) {
		super.drawDefaultBackground();
		altList.drawScreen(par1, par2, par3);
		super.drawScreen(par1, par2, par3);
		
		String username = mc.getSession().getUsername();
		
		mc.fontRenderer.drawString(username, 2, 2, 0xffffff);
		
		((GuiButton) buttonList.get(0)).enabled = altList.getCurrentSlot() >= 0;
        ((GuiButton) buttonList.get(4)).enabled = altList.getCurrentSlot() >= 0;
	}
	
	protected void actionPerformed(GuiButton b) {
		if(!b.enabled) {
			return;
		}
		
		switch(b.id) {
			case 0:
				if (altList.getCurrentSlot() >= 0 && altList.getSize() > 0)
					altList.elementClicked(altList.getCurrentSlot(), true);
				break;
				
			case 1:
				mc.displayGuiScreen(new GuiLogin(this, true));
				break;
				
			case 2:
				mc.displayGuiScreen(new GuiLogin(this, false));
				break;
			
			case 3:
				loadList();
				break;
				
			case 4:
				altList.removeAccount();
				Wrapper.getAccounts().end();
				break;
				
			case 5:
				Wrapper.getAccounts().start();
				altList.refresh();
				break;
				
			case 6:
				mc.displayGuiScreen(parentGui);
				break;
		}
	}
	
	public void loadList() {
		new Thread(new Runnable() {
			public void run() {
				JFileChooser fileChooser = new JFileChooser();
				if (fileChooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
					File selectedFile = fileChooser.getSelectedFile();
					try {
						BufferedReader reader = new BufferedReader(
								new FileReader(selectedFile));
						String line;
						
						while((line = reader.readLine()) != null) {
							if (line.contains(":")) {
								Account account = new Account(
										line.split(":")[0], line.split(":")[1]);
								if (!Accounts.getAccounts().contains(account))
									Accounts.getAccounts().add(account);
							}
						}
						reader.close();
					} catch (FileNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}).start();
	}
}
