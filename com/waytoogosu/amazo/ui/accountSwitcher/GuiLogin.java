package com.waytoogosu.amazo.ui.accountSwitcher;

import org.lwjgl.input.Keyboard;

import com.waytoogosu.amazo.Wrapper;
import com.waytoogosu.amazo.util.account.Account;

import net.minecraft.src.GuiButton;
import net.minecraft.src.GuiScreen;
import net.minecraft.src.GuiTextField;
import net.minecraft.src.Session;

public class GuiLogin extends GuiScreen {
	private GuiTextField username, password;
	private GuiScreen parent;
	private boolean add;
	
	public GuiLogin(GuiScreen prevScreen, boolean addToList) {
        parent = prevScreen;
        add    = addToList;
	}
	
    public void updateScreen() {
        this.username.updateCursorCounter();
        this.password.updateCursorCounter();
    }
    
    public void initGui() {
    	Keyboard.enableRepeatEvents(true);
        buttonList.add(new GuiButton(0, this.width / 2 - 100, this.height / 4 + 96 + 12, add ? "Add" : "Login"));
        buttonList.add(new GuiButton(1, this.width / 2 - 100, this.height / 4 + 120 + 12, "Cancel"));
        
        username = new GuiTextField(mc.fontRenderer, this.width / 2 - 100, 66, 200, 20);
        username.setFocused(true);
        username.setMaxStringLength(128);
        
        password = new GuiTextField(mc.fontRenderer, this.width / 2 - 100, 106, 200, 20);
        password.setMaxStringLength(128);
    }
    
    protected void actionPerformed(GuiButton par1GuiButton) {
    	if(par1GuiButton.id == 0) {
    		if(add) {
    			Wrapper.getAccounts().getAccounts().add(new Account(username.getText(), password.getText()));
    			Wrapper.getAccounts().end();
    			mc.displayGuiScreen(parent);
    		} else {
    			if(username.getText().length() > 0 && password.getText().length() > 0) {
    				String[] login = GuiAccountList.login(Wrapper.getMc().getLogAgent(), username.getText(), password.getText());
    				if(login != null) {
						GuiAccountList.setSession(login[0], login[1]);
    					mc.displayGuiScreen(parent);
    				}
    			}
    		}
    	} else if(par1GuiButton.id == 1) {
    		mc.displayGuiScreen(parent);
    	}
    }
    
    public void keyTyped(char ch, int key) {
    	super.keyTyped(ch, key);
        this.username.textboxKeyTyped(ch, key);
        this.password.textboxKeyTyped(ch, key);

        if (key == Keyboard.KEY_TAB)
        {
            if (this.username.isFocused())
            {
                this.username.setFocused(false);
                this.password.setFocused(true);
            }
            else
            {
                this.username.setFocused(true);
                this.password.setFocused(false);
            }
        }

        if (key == Keyboard.KEY_RETURN)
        {
            this.actionPerformed((GuiButton)this.buttonList.get(0));
        }
        
        ((GuiButton)this.buttonList.get(0)).enabled = this.username.getText().length() > 3;
    }
    
    protected void mouseClicked(int par1, int par2, int par3) {
        super.mouseClicked(par1, par2, par3);
        this.username.mouseClicked(par1, par2, par3);
        this.password.mouseClicked(par1, par2, par3);
    }
    
    public void drawScreen(int par1, int par2, float par3) {
        this.drawDefaultBackground();
        this.drawCenteredString(this.fontRenderer, add ? "Add an account":"Login to an account", this.width / 2, 17, 0xff0000);
        this.drawString(this.fontRenderer, "Username", this.width / 2 - 100, 53, 0xff0000);
        this.drawString(this.fontRenderer, "Password", this.width / 2 - 100, 94, 0xff0000);
        this.username.drawTextBox();
        this.password.drawTextBox();
        super.drawScreen(par1, par2, par3);
        
        String username = null;
        
        Session s = mc.getSession();
        username = s.getUsername();
        
        mc.fontRenderer.drawStringWithShadow(username, 2, 2, 0xff0000);
        ((GuiButton)this.buttonList.get(0)).enabled = this.username.getText().length() > 3;

    }
}
