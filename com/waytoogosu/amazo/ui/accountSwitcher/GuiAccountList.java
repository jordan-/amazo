package com.waytoogosu.amazo.ui.accountSwitcher;

import io.github.jordandoyle.helpers.reflection.Mapping;

import java.io.IOException;
import java.lang.reflect.Field;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.imageio.ImageIO;

import org.apache.commons.lang3.StringUtils;
import org.lwjgl.opengl.GL11;

import com.waytoogosu.amazo.Wrapper;
import com.waytoogosu.amazo.util.account.Account;

import net.minecraft.src.DynamicTexture;
import net.minecraft.src.EntityPlayer;
import net.minecraft.src.EntityPlayerMP;
import net.minecraft.src.EntityPlayerSP;
import net.minecraft.src.GuiScreen;
import net.minecraft.src.GuiSlot;
import net.minecraft.src.HttpUtil;
import net.minecraft.src.IImageBuffer;
import net.minecraft.src.ILogAgent;
import net.minecraft.src.ImageBufferDownload;
import net.minecraft.src.Minecraft;
import net.minecraft.src.RenderManager;
import net.minecraft.src.ResourceLocation;
import net.minecraft.src.Session;
import net.minecraft.src.Tessellator;
import net.minecraft.src.TextureManager;
import net.minecraft.src.TextureObject;
import net.minecraft.src.ThreadDownloadImageData;

public class GuiAccountList extends GuiSlot {
	private Minecraft mc = Wrapper.getMc();
	private List<Account> accountList = new ArrayList<Account>();
	private int currentSlot = -1;
	private GuiScreen parent;

	public GuiAccountList(GuiScreen parent) {
		super(Minecraft.getMinecraft(), parent.width, parent.height, 32,
				(parent.height - 65) + 4, 24);

		this.parent = parent;
		refresh();
	}

	@Override
	protected int getSize() {
		return getList().size();
	}

	@Override
	protected void elementClicked(int var1, boolean var2) {
		currentSlot = var1;

		if (var1 < getSize() && var2) {
			Account account = getList().get(var1);
			String[] resp = login(mc.getLogAgent(), account.getUsername(),
					account.getPassword());

			if (resp != null) {
				setSession(resp[0], resp[1]);
			}
		}
	}

	@Override
	protected boolean isSelected(int var1) {
		return var1 == currentSlot;
	}

	@Override
	protected void drawBackground() {
		// TODO Auto-generated method stub

	}

	public int getCurrentSlot() {
		return currentSlot;
	}

	@Override
	protected void drawSlot(int var1, int var2, int var3, int var4,
			Tessellator var5) {
		Account a = (Account) getList().get(var1);

		mc.fontRenderer.drawString("\247f" + a.getUsername(), parent.width / 2
				- mc.fontRenderer.getStringWidth(a.getUsername()) / 2,
				var3 + 1, 0xffffff);

		String s = "";

		for (int l = 0; l < a.getPassword().length(); l++)
			s += "*";

		mc.fontRenderer.drawString("\247b" + s, parent.width / 2
				- mc.fontRenderer.getStringWidth(a.getUsername()) / 2,
				var3 + 13, 0xffffff);

		GL11.glPushMatrix();
		
		ResourceLocation locationSkin = getLocationSkin(a.getUsername());
		ThreadDownloadImageData downloadImageSkin = getDownloadImageSkin(locationSkin, a.getUsername());

		//mc.renderEngine.bindTexture(locationSkin);
		
		GL11.glBindTexture(GL11.GL_TEXTURE_2D, downloadImageSkin.getGlTextureId());
		GL11.glColor4f(1.0F, 1.0F, 1.0F, 1F);
		
		GL11.glScalef(0.5F, 0.5F, 0.5F);
        mc.ingameGUI.drawTexturedModalRect(parent.width + 160, var3 * 2 - 5, 29, 70,
			37, 50);
		GL11.glPopMatrix();
	}

	private ThreadDownloadImageData getDownloadImage(ResourceLocation par0ResourceLocation, String par1Str, ResourceLocation par2ResourceLocation, IImageBuffer par3IImageBuffer)
    {
        TextureManager var4 = Wrapper.getMc().getTextureManager();
        Object var5 = var4.getTexture(par0ResourceLocation);

        if (var5 == null)
        {
            var5 = new ThreadDownloadImageData(par1Str, par2ResourceLocation, par3IImageBuffer);
            var4.loadTexture(par0ResourceLocation, (TextureObject)var5);
        }

        return (ThreadDownloadImageData)var5;
    }
	
	private ResourceLocation getLocationSkin(String par0Str)
    {
        return new ResourceLocation("skins/" + net.minecraft.src.StringUtils.stripControlCodes(par0Str));
    }
	
	private ThreadDownloadImageData getDownloadImageSkin(ResourceLocation par0ResourceLocation, String par1Str)
    {
        return getDownloadImage(par0ResourceLocation, getSkinUrl(par1Str), new ResourceLocation("textures/entity/steve.png"), new ImageBufferDownload());
    }
	
	private String getSkinUrl(String par0Str)
    {
        return String.format("http://skins.minecraft.net/MinecraftSkins/%s.png", new Object[] {net.minecraft.src.StringUtils.stripControlCodes(par0Str)});
    }
	
	public void setList(ArrayList list) {
		this.accountList = list;
	}

	public List<Account> getList() {
		return accountList;
	}

	public static void setSession(String username, String sessionId) {
		Class c = Minecraft.class;
		Field f = null;

		try {
			f = c.getDeclaredField(Mapping.getMappedField(
					"net.minecraft.src.Minecraft", "session"));
		} catch (NoSuchFieldException e) {
			try {
				f = c.getDeclaredField("session");
			} catch (NoSuchFieldException e1) {
				e1.printStackTrace();
			} catch (SecurityException e2) {
				e2.printStackTrace();
			}
		} catch (SecurityException e) {
			e.printStackTrace();
		}

		f.setAccessible(true);
		try {
			f.set(Wrapper.getMc(), new Session(username, sessionId));
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
	}

	public void removeAccount() {
		if (getSize() != 0 && currentSlot != -1
				&& Wrapper.getAccounts().getAccounts().get(currentSlot) != null) {
			Wrapper.getAccounts().getAccounts().remove(currentSlot);
			setList(Wrapper.getAccounts().getAccounts());
		}
	}

	public void refresh() {
		setList(Wrapper.getAccounts().getAccounts());
	}

	public static String[] login(ILogAgent agent, String username,
			String password) {
		HashMap details = new HashMap();
		details.put("user", username);
		details.put("password", password);
		details.put("version", 13);

		String resp = null;

		try {
			resp = HttpUtil.sendPost(agent, new URL(
					"http://login.minecraft.net/"), details, false);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}

		if (!StringUtils.isBlank(resp)) {
			if (!resp.contains(":")) {
				if (agent == null) {
					System.out.println("Couldn't authenticate: " + resp);
				} else {
					agent.logWarning("Couldn't authenticate: " + resp);
				}

				return null;
			}

			String[] response = resp.split(":");

			return new String[] { response[2], response[3] };
		}

		if (agent == null) {
			System.out
					.println("Couldn't authenticate: recieved a blank response");
		} else {
			agent.logWarning("Couldn't authenticate: recieved a blank response");
		}

		return null;
	}
}
