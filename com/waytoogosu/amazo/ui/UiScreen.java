package com.waytoogosu.amazo.ui;

import static org.lwjgl.opengl.GL11.*;

import java.io.IOException;

import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.GL11;
import org.newdawn.slick.Color;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;
import org.newdawn.slick.util.ResourceLoader;

import com.waytoogosu.amazo.ui.panel.Panel;
import com.waytoogosu.amazo.ui.panel.PanelManager;

import net.minecraft.src.GuiScreen;
import net.minecraft.src.ResourceLocation;

public class UiScreen extends GuiScreen{

	private PanelManager panelManager;
	private boolean cancelCloseUiScreen;

	public UiScreen(){
		panelManager = new PanelManager();
	}

	public PanelManager getPanelManager(){
		return panelManager;
	}

	public void setCancelCloseUiScreen(boolean cancelCloseUiScreen){
		this.cancelCloseUiScreen = cancelCloseUiScreen;
	}

	@Override
	public void mouseClicked(int j, int k, int l){
		super.mouseClicked(j, k, l);
		panelManager.clickPanels(j, k, l);
	}

	@Override
	public void handleMouseInput(){
		super.handleMouseInput();
		panelManager.handleMouseInput();
	}

	@Override
	public void keyTyped(char par1, int par2){
		panelManager.keyTypePanels(par1, par2);
		if(par2 == Keyboard.KEY_ESCAPE && !cancelCloseUiScreen){
			super.keyTyped(par1, par2);
		}
		cancelCloseUiScreen = false;
	}

	@Override
	public void drawScreen(int j, int k, float f){
		super.drawDefaultBackground();
		for(Panel p : panelManager.getPanels()){
			p.draw();
		}
	}
}
