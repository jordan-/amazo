package com.waytoogosu.amazo.ui.console;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.lwjgl.input.Keyboard;

import com.waytoogosu.amazo.Wrapper;
import com.waytoogosu.amazo.command.Command;
import com.waytoogosu.amazo.command.CommandManager;
import com.waytoogosu.amazo.command.Parameter;
import com.waytoogosu.amazo.font.AmazoUnicodeFont;
import com.waytoogosu.amazo.mod.ChatEventListener;
import com.waytoogosu.amazo.ui.panel.Panel;
import com.waytoogosu.amazo.util.Bounds;

import net.minecraft.src.GuiScreen;
import net.minecraft.src.GuiTextField;

public class UiConsole extends GuiScreen{

	public static final String INPUT = "\247f[INPUT-]";
	public static final String OUTPUT = "\247f[OUTPUT]\247f";

	private GuiTextField textField;
	private List<String> log, lines;
	private List<PredictionBounds> predictedCommands;
	private boolean messageOutputed;


	public UiConsole(){
		int posX = 2;
		textField = new GuiTextField(Wrapper.getMc().fontRenderer, posX, 2, Wrapper.getWidth() - posX - posX, 15);
		textField.setFocused(true);
		textField.setMaxStringLength(45);
		log = new ArrayList();
		lines = new ArrayList();
		predictedCommands = new ArrayList();
		messageOutputed = false;
	}



	@Override
	public void drawScreen(int j, int k, float f){
		super.drawDefaultBackground();
		super.drawScreen(j, k, f);
		AmazoUnicodeFont font = Wrapper.getFontManager().getFont("Lucida Console", 15);
		String s = ">" + textField.getText();
		Wrapper.getUiMethods().drawBorderedRect(0, 0, width, 150, 1.5f , 0x99000000, 0x505c5c5c);
		font.drawString(s + (Wrapper.getMc().ingameGUI.getUpdateCounter()/6 % 2 == 0 ? "_" : ""), 3, 140, 0xffffffff);

		while(lines.size() > 17) { lines.remove(0); }
		int posY =  2;
		for(String s1 : lines){
			font.drawString(s1, 3, posY, 0xffffffff);
			posY += 8;
		}


		for(PredictionBounds pb : predictedCommands){
			pb.draw();
		}

		if(!messageOutputed){
			int length = width/font.getStringWidth("M") - 13;
			String str = "Welcome, any Console command is also usable using chat commands. The current prefix is \"" + ChatEventListener.prefix + "\". Change the prefix inside your Client folder in a text file called \"command.txt\". The GUI and CONSOLE keys can also be changed in the \"gui.txt\".";
			for(String s1 : wrapText(str, length)){
				lines.add(s1);
			}
			messageOutputed = true;
		}
	}

	public void respond(String prefix, String string) {
		DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
		Date date = new Date();
		String s1;
		log.add(s1 = dateFormat.format(date) + " " + prefix + " " + string);
		AmazoUnicodeFont font = Wrapper.getFontManager().getFont("Lucida Console", 15);

		int length = width/font.getStringWidth("M") - 13;
		for(String s : wrapText(s1, length)){
			lines.add(s);
		}
	}



	@Override
	public void mouseClicked(int j, int k, int l){
		super.mouseClicked(j, k, l);
		textField.setFocused(true);

		for(PredictionBounds pb : predictedCommands){
			if(pb.isMouseOverBounds()){
				pb.onClick(j, k);
			}
		}
	}

	@Override
	public void keyTyped(char par1, int par2){
		textField.setMaxStringLength(80);	
		Keyboard.enableRepeatEvents(true);
		textField.textboxKeyTyped(par1, par2);

		if(par2 == Keyboard.KEY_RETURN && textField.getText().length() > 0){
			handleInput(textField.getText().trim());
			textField.setText("");

		}
		updatePredictedCommand();
		super.keyTyped(par1, par2);
	}

	public void handleInput(String s){
		respond(INPUT, s);

		CommandManager cm = Wrapper.getCommandManager();
		
		int shortest = -1;
		String input = s.split(" ")[0];
		String closest = "";
		
		for(Command cmd : cm.getCommands()){
			int lev = StringUtils.getLevenshteinDistance(input.toLowerCase(), cmd.getName().toLowerCase());
			
			if(lev == 0) {
				closest = cmd.getName().toLowerCase();
				shortest = 0;
			}
			
			if(lev <= shortest || shortest < 0) {
				closest = cmd.getName().toLowerCase();
				shortest = lev;
			}
			
			try{
				String[] args = s.split(" ");
				if(!cmd.getName().equalsIgnoreCase(args[0])){
					continue;
				}
				if(args.length - 1!= cmd.getParams().length || !cm.argsMatchParam(args, cmd.getParams())){
					String params = "";
					for(Parameter parm : cmd.getParams()){
						params += " <\247a" + parm.getType() + " " + parm.getName() + "\247f>";
					}
					respond(OUTPUT, "\247cArguments: \247f\"" + cmd.getName() + params + "\"");		
					return;
				}
				if(cmd.onCommand(args, s, true)) return;
			} catch (Exception e) {
				respond(OUTPUT, "An error occured when running the command. Stacktrace printed to console");
				e.printStackTrace();
				return;
			}
		}
		respond(OUTPUT, "\247cUnknown command: \"\247f" + s + "\247c\""); 
		respond(OUTPUT, "\247cDid you mean: \"\247f" + closest + "\247c\"?");
	}
	
	private String stripColorCodes(String string){
		String array[] = string.split("\247");
		string = "";
		int index = 0;
		for(String s : array){
			if(s.length() > 1 && index != 0){
				s = s.substring(1);
			}
			string += s;
			index++;
		}
		return string;
	}


	private String[] wrapText(String s, int length){
		List<String> text = new ArrayList();
		int beginIndex = 0;
		int splitAt = 0;
		int count = 0;
		String lastColorCode = "";
		String colorCodePreSplit = "";
		for (int i = 0; i < s.length(); i++) {
			if(s.charAt(i) == '\247'){
				splitAt = i + 1;
				lastColorCode = "\247" + s.charAt(i + 1);
				i++;
			
				continue;
			}
			if(stripColorCodes(s).length() - beginIndex <= length){
				String s1 = lastColorCode + s.substring(beginIndex, s.length());
				text.add(s1);
				break;
			}
		
			count++;
			splitAt++;
			if(count >= length){
				String s1 = colorCodePreSplit + s.substring(beginIndex, splitAt);
				text.add(s1);
				colorCodePreSplit = lastColorCode;
				beginIndex = splitAt;
				count = 0;
			}
		}
		return text.toArray(new String[text.size()]);
	}
	
	private boolean predictedCommandsContainsCommand(Command cmd){
		for(PredictionBounds pb : predictedCommands){
			if(pb.getCommand().equals(cmd)){
				return true;
			}
		}
		return false;
	}

	private void updatePredictedCommand(){
		int y = 154;
		CommandManager cm = Wrapper.getCommandManager();
		String text = textField.getText();
		String args[] = text.split(" ");
		for(Command cmd : cm.getCommands()){			
			if(args.length > 0 && text.length() > 0){
				if((cmd.getName().equalsIgnoreCase(args[0]) || cmd.getName().toLowerCase().startsWith(args[0].toLowerCase())) && !predictedCommandsContainsCommand(cmd)){
					predictedCommands.add(new PredictionBounds(textField, cmd, predictedCommands, y));
				}
			} else {	
				predictedCommands.clear();
			}
		}

		boolean clear = false;
		List<PredictionBounds> remove = new ArrayList();
		for(PredictionBounds pb : predictedCommands){
			Command cmd = pb.getCommand();
			if(text.length() > 0){
				if(!(cmd.getName().equalsIgnoreCase(args[0]) || cmd.getName().toLowerCase().startsWith(args[0].toLowerCase()))){
					remove.add(pb);
				}
			} else {	
				clear = true;
			}
		}

		if(clear){
			predictedCommands.clear();
		}

		for(PredictionBounds pb : remove){
			predictedCommands.remove(pb);
		}

	}

}
