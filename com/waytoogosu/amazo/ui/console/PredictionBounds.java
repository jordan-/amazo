package com.waytoogosu.amazo.ui.console;

import java.util.List;

import org.lwjgl.input.Mouse;

import net.minecraft.src.GuiTextField;

import com.waytoogosu.amazo.Wrapper;
import com.waytoogosu.amazo.command.Command;
import com.waytoogosu.amazo.command.Parameter;
import com.waytoogosu.amazo.font.AmazoUnicodeFont;
import com.waytoogosu.amazo.util.Bounds;

public class PredictionBounds extends Bounds{

	private Command command;
	private AmazoUnicodeFont font;
	private GuiTextField textField;
	
	public PredictionBounds(GuiTextField tf, Command command, List<PredictionBounds> list, int posY) {
		super(7, list.size() * 9  + posY, 0, 11);
		font = Wrapper.getFontManager().getFont("Lucida Console", 15);
		this.command = command;
		this.textField = tf;
	}
	
	public void draw(){
		setWidth(Wrapper.getWidth() - getPosX() * 2);
		super.drawBorderedRect(1.5f, 0x99000000, isMouseOverBounds() && !Mouse.isButtonDown(0) ? 0x50171717 : 0x505c5c5c);
		String params = "";

		for(Parameter parm : command.getParams()){
			params += "\247f| <\247e" + parm.getType() + " " + parm.getName() + "\247f> ";
		}
		
		font.drawString(command.getName() + " \247e" + command.getDescription() + " " + params, getPosX() + 1, getPosY() + 2, 0xffffffff);
		font.drawString("\247a" + command.getName().substring(0, textField.getText().split(" ")[0].length()), getPosX() + 1, getPosY() + 2, 0x99ffffff);
	}

	public void onClick(int x, int y){
		if(super.isMouseOverBounds()){
			textField.setText(command.getName() + " ");
		}
	}
	
	public Command getCommand(){
		return command;
	}


	
}
