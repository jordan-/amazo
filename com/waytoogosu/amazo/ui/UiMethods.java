package com.waytoogosu.amazo.ui;
import java.lang.reflect.Field;

import org.lwjgl.opengl.GL11;
import org.lwjgl.util.glu.GLU;
import net.minecraft.src.ChatAllowedCharacters;
import net.minecraft.src.Gui;
import net.minecraft.src.Tessellator;

public class UiMethods extends Gui{
	
	public void drawGradientRectWithOutline(int xs, int ys, int xe, int ye, int c, int c1, int c2)
	{	
		this.drawGradientRect(xs + 1, ys + 1, xe- 1, ye - 1, c1,c2);
		GL11.glPushMatrix();
		GL11.glScalef(0.5f,0.5f,0.5f);	
		this.drawWHollowBorderedRect(xs*2+1, ys*2+1, xe*2-1, ye*2-1,1, c);
		GL11.glPopMatrix();
	}
	
	public void drawGradientRectWithOutline(int xs, int ys, int xe, int ye, int c, int c1, int c2, int c3)
	{	
		this.drawGradientRect(xs + 1, ys + 1, xe- 1, ye - 1, c1,c2);
		drawVerticalLine((xs+1), (ys), (ye) -1, c3);
		drawHorizontalLine(xs + 1, xe - 2, ys+1, c3);
		GL11.glPushMatrix();
		GL11.glScalef(0.5f,0.5f,0.5f);	
		this.drawWHollowBorderedRect(xs*2+1, ys*2+1, xe*2-1, ye*2-1,1, c);
		GL11.glPopMatrix();
	}

	public void drawSmallRect(int x, int y, int x1, int y1, int CO1) {
		GL11.glPushMatrix();
		GL11.glScalef(0.5F, 0.5F, 0.5F);
		drawRect(x * 2, y * 2, x1 * 2, y1 * 2, CO1);
		GL11.glPopMatrix();
	}

	public void drawSmallWHollowBorderedRect(int x, int y, int x1, int y1, int CO1) {
		GL11.glPushMatrix();
		GL11.glScalef(0.5F, 0.5F, 0.5F);
		drawWHollowBorderedRect(x * 2, y * 2, x1 * 2, y1 * 2, CO1);
		GL11.glPopMatrix();
	}

	public void drawWHollowBorderedRect(int x, int y, int x1, int y1, int CO1) {
		drawVerticalLine(x, y, y1 -1, CO1);
		drawVerticalLine(x1 - 1, y, y1 - 1, CO1);
		drawHorizontalLine(x + 1, x1 - 2, y, CO1);
		drawHorizontalLine(x + 1, x1 - 2, y1 -1, CO1);
	}

	public void drawBarMethod(int x, int y, int x1, int y1, int CO1, int CO2, int CO3) {
		drawSmallWBorderedBarRect(x, y, x1, y1, CO1, CO2, CO3);
	}
	

	public void drawSmallWBorderedBarRect(int x, int y, int x1, int y1, int CO1, int CO2, int CO3) {
		GL11.glPushMatrix();
		GL11.glScalef(0.5F, 0.5F, 0.5F);
		drawWBorderedBarRect(x * 2, y * 2, x1 * 2, y1 * 2, CO1, CO2, CO3);
		GL11.glPopMatrix();
	}

	public void drawWBorderedBarRect(int x, int y, int x1, int y1, int CO1, int CO2, int CO3) {
		drawRect(x + 1, y + 1, x1 - 1, y1 - 1, CO2);
		drawRect(x + 1, y + 1, x1 - 1, y1 - 6, CO3);
		drawVerticalLine(x, y, y1 -1, CO1);
		drawVerticalLine(x1 - 1, y, y1 - 1, CO1);
		drawHorizontalLine(x + 1, x1 - 2, y, CO1);
		drawHorizontalLine(x + 1, x1 - 2, y1 -1, CO1);
	}
	public static void drawRect(int x, int y, int x2, int y2, int col1) {
		float f = (float)(col1 >> 24 & 0xFF) / 255F;
		float f1 = (float)(col1 >> 16 & 0xFF) / 255F;
		float f2 = (float)(col1 >> 8 & 0xFF) / 255F;
		float f3 = (float)(col1 & 0xFF) / 255F;

		GL11.glEnable(GL11.GL_BLEND);
		GL11.glDisable(GL11.GL_TEXTURE_2D);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		GL11.glEnable(GL11.GL_LINE_SMOOTH);

		GL11.glPushMatrix();
		GL11.glColor4f(f1, f2, f3, f);
		GL11.glBegin(GL11.GL_QUADS);
		GL11.glVertex2d(x2, y);
		GL11.glVertex2d(x, y);
		GL11.glVertex2d(x, y2);
		GL11.glVertex2d(x2, y2);
		GL11.glEnd();
		GL11.glPopMatrix();

		GL11.glEnable(GL11.GL_TEXTURE_2D);
		GL11.glDisable(GL11.GL_BLEND);
		GL11.glDisable(GL11.GL_LINE_SMOOTH);
	}

	public void drawGradientRect(int x, int y, int x2, int y2, int col1, int col2) {
		float f = (float)(col1 >> 24 & 0xFF) / 255F;
		float f1 = (float)(col1 >> 16 & 0xFF) / 255F;
		float f2 = (float)(col1 >> 8 & 0xFF) / 255F;
		float f3 = (float)(col1 & 0xFF) / 255F;

		float f4 = (float)(col2 >> 24 & 0xFF) / 255F;
		float f5 = (float)(col2 >> 16 & 0xFF) / 255F;
		float f6 = (float)(col2 >> 8 & 0xFF) / 255F;
		float f7 = (float)(col2 & 0xFF) / 255F;

		GL11.glEnable(GL11.GL_BLEND);
		GL11.glDisable(GL11.GL_TEXTURE_2D);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		GL11.glEnable(GL11.GL_LINE_SMOOTH);
		GL11.glShadeModel(GL11.GL_SMOOTH);

		GL11.glPushMatrix();
		GL11.glBegin(GL11.GL_QUADS);
		GL11.glColor4f(f1, f2, f3, f);
		GL11.glVertex2d(x2, y);
		GL11.glVertex2d(x, y);

		GL11.glColor4f(f5, f6, f7, f4);
		GL11.glVertex2d(x, y2);
		GL11.glVertex2d(x2, y2);
		GL11.glEnd();
		GL11.glPopMatrix();

		GL11.glEnable(GL11.GL_TEXTURE_2D);
		GL11.glDisable(GL11.GL_BLEND);
		GL11.glDisable(GL11.GL_LINE_SMOOTH);
		GL11.glShadeModel(GL11.GL_FLAT);
	}
	
	public void drawRoundedGradientRect(int x, int y, int x1, int y1, int size,int col1, int col2, int borderC) {
		
		
		
		x *= 2;
		y *= 2;
		x1 *= 2;
		y1 *= 2;
		GL11.glScalef(0.5F, 0.5F, 0.5F);
		drawVerticalLine(x, y + 1, y1 - 2, borderC);
		drawVerticalLine(x1 - 1, y + 1, y1 - 2, borderC);
		drawHorizontalLine(x + 2, x1 - 3, y, borderC);
		drawHorizontalLine(x + 2, x1 - 3, y1 - 1, borderC);
		drawHorizontalLine(x + 1, x + 1, y + 1, borderC);
		drawHorizontalLine(x1 - 2, x1 - 2, y + 1, borderC);
		drawHorizontalLine(x1 - 2, x1 - 2, y1 - 2, borderC);
		drawHorizontalLine(x + 1, x + 1, y1 - 2, borderC);
		float f = (float)(col1 >> 24 & 0xFF) / 255F;
		float f1 = (float)(col1 >> 16 & 0xFF) / 255F;
		float f2 = (float)(col1 >> 8 & 0xFF) / 255F;
		float f3 = (float)(col1 & 0xFF) / 255F;

		float f4 = (float)(col2 >> 24 & 0xFF) / 255F;
		float f5 = (float)(col2 >> 16 & 0xFF) / 255F;
		float f6 = (float)(col2 >> 8 & 0xFF) / 255F;
		float f7 = (float)(col2 & 0xFF) / 255F;

		GL11.glEnable(GL11.GL_BLEND);
		GL11.glDisable(GL11.GL_TEXTURE_2D);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		GL11.glEnable(GL11.GL_LINE_SMOOTH);
		GL11.glShadeModel(GL11.GL_SMOOTH);

		GL11.glPushMatrix();
		GL11.glBegin(GL11.GL_QUADS);
		GL11.glColor4f(f1, f2, f3, f);
		GL11.glVertex2d(x1, y);
		GL11.glVertex2d(x, y);

		GL11.glColor4f(f5, f6, f7, f4);
		GL11.glVertex2d(x, y1);
		GL11.glVertex2d(x1, y1);
		GL11.glEnd();
		GL11.glPopMatrix();

		GL11.glEnable(GL11.GL_TEXTURE_2D);
		GL11.glDisable(GL11.GL_BLEND);
		GL11.glDisable(GL11.GL_LINE_SMOOTH);
		GL11.glShadeModel(GL11.GL_FLAT);
		GL11.glScalef(2F, 2F, 2F);
		
		
	}
	

	public static void drawBar(int x, int y, int x2, int y2, float l1, int col1, int col2, int col3, int var, boolean inverse) {
		int height = (y2 - y)/2; 
		if(inverse){
			drawRect(x, y, x2, y2 , col1);
			drawRect(x2 - var*4-2, y, x2, y + height , col2);
			drawRect(x2 - var*4-2,y+height,x2,y2,col3);
		}else {
			drawRect(x, y, x2, y2 , col1);
			drawRect(x, y, x+var*4+2, y + height , col2);
			drawRect(x,y+height,x+var*4+2,y2,col3);
		}

		float f = (float)(col1 >> 24 & 0xFF) / 255F;
		float f1 = (float)(col1 >> 16 & 0xFF) / 255F;
		float f2 = (float)(col1 >> 8 & 0xFF) / 255F;
		float f3 = (float)(col1 & 0xFF) / 255F;

		GL11.glEnable(GL11.GL_BLEND);
		GL11.glDisable(GL11.GL_TEXTURE_2D);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		GL11.glEnable(GL11.GL_LINE_SMOOTH);

		GL11.glPushMatrix();
		GL11.glColor4f(f1, f2, f3, f);
		GL11.glLineWidth(l1);
		GL11.glBegin(GL11.GL_LINES);
		GL11.glVertex2d(x, y);
		GL11.glVertex2d(x, y2);
		GL11.glVertex2d(x2, y2);
		GL11.glVertex2d(x2, y);
		GL11.glVertex2d(x, y);
		GL11.glVertex2d(x2, y);
		GL11.glVertex2d(x, y2);
		GL11.glVertex2d(x2, y2);
		GL11.glEnd();
		GL11.glPopMatrix();

		GL11.glEnable(GL11.GL_TEXTURE_2D);
		GL11.glDisable(GL11.GL_BLEND);
		GL11.glDisable(GL11.GL_LINE_SMOOTH);
	}

	public static void drawBorderedRect(int x, int y, int x2, int y2, float l1, int col1, int col2) {
		drawRect(x, y, x2, y2, col2);

		float f = (float)(col1 >> 24 & 0xFF) / 255F;
		float f1 = (float)(col1 >> 16 & 0xFF) / 255F;
		float f2 = (float)(col1 >> 8 & 0xFF) / 255F;
		float f3 = (float)(col1 & 0xFF) / 255F;

		GL11.glEnable(GL11.GL_BLEND);
		GL11.glDisable(GL11.GL_TEXTURE_2D);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		GL11.glEnable(GL11.GL_LINE_SMOOTH);

		GL11.glPushMatrix();
		GL11.glColor4f(f1, f2, f3, f);
		GL11.glLineWidth(l1);
		GL11.glBegin(GL11.GL_LINES);
		GL11.glVertex2d(x, y);
		GL11.glVertex2d(x, y2);
		GL11.glVertex2d(x2, y2);
		GL11.glVertex2d(x2, y);
		GL11.glVertex2d(x, y);
		GL11.glVertex2d(x2, y);
		GL11.glVertex2d(x, y2);
		GL11.glVertex2d(x2, y2);
		GL11.glEnd();
		GL11.glPopMatrix();

		GL11.glEnable(GL11.GL_TEXTURE_2D);
		GL11.glDisable(GL11.GL_BLEND);
		GL11.glDisable(GL11.GL_LINE_SMOOTH);
	}
	

	
	

	public static void drawHollowBorderedRect(int x, int y, int x2, int y2, float l1, int col1) {
		float f = (float)(col1 >> 24 & 0xFF) / 255F;
		float f1 = (float)(col1 >> 16 & 0xFF) / 255F;
		float f2 = (float)(col1 >> 8 & 0xFF) / 255F;
		float f3 = (float)(col1 & 0xFF) / 255F;

		GL11.glEnable(GL11.GL_BLEND);
		GL11.glDisable(GL11.GL_TEXTURE_2D);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		GL11.glEnable(GL11.GL_LINE_SMOOTH);

		GL11.glPushMatrix();
		GL11.glColor4f(f1, f2, f3, f);
		GL11.glLineWidth(l1);
		GL11.glBegin(GL11.GL_LINES);
		GL11.glVertex2d(x, y);
		GL11.glVertex2d(x, y2);
		GL11.glVertex2d(x2, y2);
		GL11.glVertex2d(x2, y);
		GL11.glVertex2d(x, y);
		GL11.glVertex2d(x2, y);
		GL11.glVertex2d(x, y2);
		GL11.glVertex2d(x2, y2);
		GL11.glEnd();
		GL11.glPopMatrix();

		GL11.glEnable(GL11.GL_TEXTURE_2D);
		GL11.glDisable(GL11.GL_BLEND);
		GL11.glDisable(GL11.GL_LINE_SMOOTH);
	}

	public void drawGradientBorderedRect(int x, int y, int x2, int y2, float l1, int col1, int col2, int col3) {
		this.drawGradientRect(x, y, x2, y2, col2, col3);

		float f = (float)(col1 >> 24 & 0xFF) / 255F;
		float f1 = (float)(col1 >> 16 & 0xFF) / 255F;
		float f2 = (float)(col1 >> 8 & 0xFF) / 255F;
		float f3 = (float)(col1 & 0xFF) / 255F;

		GL11.glEnable(GL11.GL_BLEND);
		GL11.glDisable(GL11.GL_TEXTURE_2D);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		GL11.glEnable(GL11.GL_LINE_SMOOTH);

		GL11.glPushMatrix();
		GL11.glColor4f(f1, f2, f3, f);
		GL11.glLineWidth(l1);
		GL11.glBegin(GL11.GL_LINES);
		GL11.glVertex2d(x, y);
		GL11.glVertex2d(x, y2);
		GL11.glVertex2d(x2, y2);
		GL11.glVertex2d(x2, y);
		GL11.glVertex2d(x, y);
		GL11.glVertex2d(x2, y);
		GL11.glVertex2d(x, y2);
		GL11.glVertex2d(x2, y2);
		GL11.glEnd();
		GL11.glPopMatrix();

		GL11.glEnable(GL11.GL_TEXTURE_2D);
		GL11.glDisable(GL11.GL_BLEND);
		GL11.glDisable(GL11.GL_LINE_SMOOTH);
	}

	public void drawWHollowBorderedRect(int x, int y, int x1, int y1, int size,
			int borderC) {
		super.drawVerticalLine(x, y + 1, y1 - 2, borderC);
		super.drawVerticalLine(x1 - 1, y + 1, y1 - 2, borderC);
		super.drawHorizontalLine(x + 2, x1 - 3, y, borderC);
		super.drawHorizontalLine(x + 2, x1 - 3, y1 - 1, borderC);
		super.drawHorizontalLine(x + 1, x + 1, y + 1, borderC);
		super.drawHorizontalLine(x1 - 2, x1 - 2, y + 1, borderC);
		super.drawHorizontalLine(x1 - 2, x1 - 2, y1 - 2, borderC);
		super.drawHorizontalLine(x + 1, x + 1, y1 - 2, borderC);
	}
	

	public static void drawVerticalLine(int x, int y, int y2, float l1, int col1) {
		float f = (float)(col1 >> 24 & 0xFF) / 255F;
		float f1 = (float)(col1 >> 16 & 0xFF) / 255F;
		float f2 = (float)(col1 >> 8 & 0xFF) / 255F;
		float f3 = (float)(col1 & 0xFF) / 255F;

		GL11.glEnable(GL11.GL_BLEND);
		GL11.glDisable(GL11.GL_TEXTURE_2D);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		GL11.glEnable(GL11.GL_LINE_SMOOTH);

		GL11.glPushMatrix();
		GL11.glColor4f(f1, f2, f3, f);
		GL11.glLineWidth(l1);
		GL11.glBegin(GL11.GL_LINES);
		GL11.glVertex2d(x, y);
		GL11.glVertex2d(x, y2);
		GL11.glEnd();
		GL11.glPopMatrix();

		GL11.glEnable(GL11.GL_TEXTURE_2D);
		GL11.glDisable(GL11.GL_BLEND);
		GL11.glDisable(GL11.GL_LINE_SMOOTH);
	}
	

	public static void drawHorizontalLine(int x, int x2, int y, float l1, int col1) {
		float f = (float)(col1 >> 24 & 0xFF) / 255F;
		float f1 = (float)(col1 >> 16 & 0xFF) / 255F;
		float f2 = (float)(col1 >> 8 & 0xFF) / 255F;
		float f3 = (float)(col1 & 0xFF) / 255F;

		GL11.glEnable(GL11.GL_BLEND);
		GL11.glDisable(GL11.GL_TEXTURE_2D);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		GL11.glEnable(GL11.GL_LINE_SMOOTH);

		GL11.glPushMatrix();
		GL11.glColor4f(f1, f2, f3, f);
		GL11.glLineWidth(l1);
		GL11.glBegin(GL11.GL_LINES);
		GL11.glVertex2d(x, y);
		GL11.glVertex2d(x2, y);
		GL11.glEnd();
		GL11.glPopMatrix();

		GL11.glEnable(GL11.GL_TEXTURE_2D);
		GL11.glDisable(GL11.GL_BLEND);
		GL11.glDisable(GL11.GL_LINE_SMOOTH);
	}

	public static void drawDiagonalLine(int x, int x2, int y, float l1, int col1) {
		float f = (float)(col1 >> 24 & 0xFF) / 255F;
		float f1 = (float)(col1 >> 16 & 0xFF) / 255F;
		float f2 = (float)(col1 >> 8 & 0xFF) / 255F;
		float f3 = (float)(col1 & 0xFF) / 255F;

		GL11.glEnable(GL11.GL_BLEND);
		GL11.glDisable(GL11.GL_TEXTURE_2D);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		GL11.glEnable(GL11.GL_LINE_SMOOTH);

		GL11.glPushMatrix();
		GL11.glColor4f(f1, f2, f3, f);
		GL11.glLineWidth(l1);
		GL11.glBegin(GL11.GL_LINES);
		GL11.glVertex2d(x, y);
		GL11.glVertex2d(y, x2);
		GL11.glEnd();
		GL11.glPopMatrix();

		GL11.glEnable(GL11.GL_TEXTURE_2D);
		GL11.glDisable(GL11.GL_BLEND);
		GL11.glDisable(GL11.GL_LINE_SMOOTH);
	}

	public static void drawCircle(int xx, int yy, int radius, int col) {
		float f = (float)(col >> 24 & 0xFF) / 255F;
		float f1 = (float)(col >> 16 & 0xFF) / 255F;
		float f2 = (float)(col >> 8 & 0xFF) / 255F;
		float f3 = (float)(col & 0xFF) / 255F;

		int sections = 70;
		double dAngle = 2 * Math.PI / sections;
		float x, y;

		GL11.glPushMatrix();
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glDisable(GL11.GL_TEXTURE_2D);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		GL11.glEnable(GL11.GL_LINE_SMOOTH);
		GL11.glShadeModel(GL11.GL_SMOOTH);
		GL11.glBegin(GL11.GL_LINE_LOOP);

		for(int i = 0; i < sections; i++) {
			x = (float)(radius * Math.cos(i * dAngle));
			y = (float)(radius * Math.sin(i * dAngle));

			GL11.glColor4f(f1, f2, f3, f);
			GL11.glVertex2f(xx + x, yy + y);
		}

		GL11.glEnd();
		GL11.glEnable(GL11.GL_TEXTURE_2D);
		GL11.glDisable(GL11.GL_BLEND);
		GL11.glDisable(GL11.GL_LINE_SMOOTH);
		GL11.glPopMatrix();
	}

	public static void drawFilledCircle(int xx, int yy, float radius, int col) {
		float f = (float)(col >> 24 & 0xFF) / 255F;
		float f1 = (float)(col >> 16 & 0xFF) / 255F;
		float f2 = (float)(col >> 8 & 0xFF) / 255F;
		float f3 = (float)(col & 0xFF) / 255F;

		int sections = 50;
		double dAngle = 2 * Math.PI / sections;
		float x, y;

		GL11.glPushMatrix();
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glDisable(GL11.GL_TEXTURE_2D);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		GL11.glEnable(GL11.GL_LINE_SMOOTH);
		GL11.glBegin(GL11.GL_TRIANGLE_FAN);

		for(int i = 0; i < sections; i++) {
			x = (float)(radius * Math.sin((i * dAngle)));
			y = (float)(radius * Math.cos((i * dAngle)));

			GL11.glColor4f(f1, f2, f3, f);
			GL11.glVertex2f(xx + x, yy + y);
		}

		GL11.glEnd();
		GL11.glEnable(GL11.GL_TEXTURE_2D);
		GL11.glDisable(GL11.GL_BLEND);
		GL11.glDisable(GL11.GL_LINE_SMOOTH);
		GL11.glPopMatrix();
	}

	public void drawMoreRoundedBorderedRect(int x, int y, int x1, int y1,int size, int borderC, int insideC) {
		x *= 2;
		y *= 2;
		x1 *= 2;
		y1 *= 2;
		GL11.glScalef(0.5F, 0.5F, 0.5F);
		drawRect(x + size, y + size, x1 - size, y1 - size, insideC);
		drawVerticalLine(x, y + 1, y1 - 2, borderC);
		drawVerticalLine(x1 - 1, y + 1, y1 - 2, borderC);
		drawHorizontalLine(x + 2, x1 - 3, y, borderC);
		drawHorizontalLine(x + 2, x1 - 3, y1 - 1, borderC);
		drawHorizontalLine(x + 1, x + 1, y + 1, borderC);
		drawHorizontalLine(x1 - 2, x1 - 2, y + 1, borderC);
		drawHorizontalLine(x1 - 2, x1 - 2, y1 - 2, borderC);
		drawHorizontalLine(x + 1, x + 1, y1 - 2, borderC);
		
		GL11.glScalef(2F, 2F, 2F);
	}

	
	


	public static void drawTri(int i, int j, int k) {
		//GL11.glScalef(0.5F, 0.5F, 0.5F);
		GL11.glRotatef(180F, 0.0F, 0.0F, 1.0F);
		float f = (float) (k >> 24 & 0xff) / 255F;
		float f1 = (float) (k >> 16 & 0xff) / 255F;
		float f2 = (float) (k >> 8 & 0xff) / 255F;
		float f3 = (float) (k & 0xff) / 255F;
		GL11.glColor4f(f1, f2, f3, f);
		GL11.glEnable(3042 /* GL_BLEND */);
		GL11.glDisable(3553 /* GL_TEXTURE_2D */);
		GL11.glEnable(2848 /* GL_LINE_SMOOTH */);
		GL11.glBlendFunc(770, 771);
		GL11.glBegin(4);
		GL11.glVertex2d(i, j + 2);
		GL11.glVertex2d(i + 2, j - 2);
		GL11.glVertex2d(i - 2, j - 2);
		GL11.glEnd();
		GL11.glDisable(2848 /* GL_LINE_SMOOTH */);
		GL11.glEnable(3553 /* GL_TEXTURE_2D */);
		GL11.glDisable(3042 /* GL_BLEND */);
		GL11.glRotatef(-180F, 0.0F, 0.0F, 1.0F);
		//GL11.glScalef(2F, 2F, 2F);
	}
	
	public static int ConvertRGB(int r, int g, int b){
		int value = ((255 & 0xFF) << 24) | 
				(((int)r & 0xFF) << 16) | 
				(((int)g & 0xFF) << 8)  | 
				(((int)b & 0xFF) << 0); 
		return value;
	}
	
	public static void drawFlagTri(int i, int j, int k) {
		//GL11.glScalef(0.5F, 0.5F, 0.5F);
		float f = (float) (k >> 24 & 0xff) / 255F;
		float f1 = (float) (k >> 16 & 0xff) / 255F;
		float f2 = (float) (k >> 8 & 0xff) / 255F;
		float f3 = (float) (k & 0xff) / 255F;
		GL11.glColor4f(f1, f2, f3, 255);
		GL11.glEnable(3042 /* GL_BLEND */);
		GL11.glDisable(3553 /* GL_TEXTURE_2D */);
		GL11.glEnable(2848 /* GL_LINE_SMOOTH */);
		GL11.glBlendFunc(770, 771);
		GL11.glBegin(4);
		GL11.glVertex2d(i, j + 2);
		GL11.glVertex2d(i , j - 2);
		GL11.glVertex2d(i-2, j );
		GL11.glEnd();
		GL11.glDisable(2848 /* GL_LINE_SMOOTH */);
		GL11.glEnable(3553 /* GL_TEXTURE_2D */);
		GL11.glDisable(3042 /* GL_BLEND */);
		//GL11.glScalef(2F, 2F, 2F);
	}
	public static void drawRightTri(int i, int j, int k) {
		GL11.glRotatef(180F, 0.0F, 0.0F, 1.0F);
		//GL11.glScalef(0.5F, 0.5F, 0.5F);
		float f = (float) (k >> 24 & 0xff) / 255F;
		float f1 = (float) (k >> 16 & 0xff) / 255F;
		float f2 = (float) (k >> 8 & 0xff) / 255F;
		float f3 = (float) (k & 0xff) / 255F;
		GL11.glColor4f(f1, f2, f3, 255);
		GL11.glEnable(3042 /* GL_BLEND */);
		GL11.glDisable(3553 /* GL_TEXTURE_2D */);
		GL11.glEnable(2848 /* GL_LINE_SMOOTH */);
		GL11.glBlendFunc(770, 771);
		GL11.glBegin(4);
		GL11.glVertex2d(i, j + 2);
		GL11.glVertex2d(i , j - 2);
		GL11.glVertex2d(i-2, j );
		GL11.glEnd();
		GL11.glDisable(2848 /* GL_LINE_SMOOTH */);
		GL11.glEnable(3553 /* GL_TEXTURE_2D */);
		GL11.glDisable(3042 /* GL_BLEND */);
		//GL11.glScalef(2F, 2F, 2F);
		GL11.glRotatef(-180F, 0.0F, 0.0F, 1.0F);
	}
	
	public void color(int c){
		float f = (float) (c >> 24 & 0xff) / 255F;
		float f1 = (float) (c >> 16 & 0xff) / 255F;
		float f2 = (float) (c >> 8 & 0xff) / 255F;
		float f3 = (float) (c & 0xff) / 255F;
		GL11.glColor4f(f1, f2, f3, f);
		//System.out.println(f1 + " " + f2 + " " + f3 + " " + f);
	}
	
	public static void drawDownTri(int i, int j, int k) {
		//GL11.glScalef(0.5F, 0.5F, 0.5F);
		
		float f = (float) (k >> 24 & 0xff) / 255F;
		float f1 = (float) (k >> 16 & 0xff) / 255F;
		float f2 = (float) (k >> 8 & 0xff) / 255F;
		float f3 = (float) (k & 0xff) / 255F;
		GL11.glColor4f(f1, f2, f3, 255);
		GL11.glEnable(3042 /* GL_BLEND */);
		GL11.glDisable(3553 /* GL_TEXTURE_2D */);
		GL11.glEnable(2848 /* GL_LINE_SMOOTH */);
		GL11.glBlendFunc(770, 771);
		GL11.glBegin(4);
		GL11.glVertex2d(i , j +2);
		GL11.glVertex2d(i + 2, j - 2);
		GL11.glVertex2d(i - 2, j - 2);
		GL11.glEnd();
		GL11.glDisable(2848 /* GL_LINE_SMOOTH */);
		GL11.glEnable(3553 /* GL_TEXTURE_2D */);
		GL11.glDisable(3042 /* GL_BLEND */);
		//GL11.glScalef(2F, 2F, 2F);
	}
	
	public static void drawDownLeft(int i, int j, int k) {
		//GL11.glScalef(0.5F, 0.5F, 0.5F);
		
		float f = (float) (k >> 24 & 0xff) / 255F;
		float f1 = (float) (k >> 16 & 0xff) / 255F;
		float f2 = (float) (k >> 8 & 0xff) / 255F;
		float f3 = (float) (k & 0xff) / 255F;
		GL11.glColor4f(f1, f2, f3, 255);
		GL11.glEnable(3042 /* GL_BLEND */);
		GL11.glDisable(3553 /* GL_TEXTURE_2D */);
		GL11.glEnable(2848 /* GL_LINE_SMOOTH */);
		GL11.glBlendFunc(770, 771);
		GL11.glBegin(4);
		GL11.glVertex2d(i -2, j +2);
		GL11.glVertex2d(i + 2, j - 1);
		GL11.glVertex2d(i - 2, j - 2);
		GL11.glEnd();
		GL11.glDisable(2848 /* GL_LINE_SMOOTH */);
		GL11.glEnable(3553 /* GL_TEXTURE_2D */);
		GL11.glDisable(3042 /* GL_BLEND */);
		//GL11.glScalef(2F, 2F, 2F);
	}
	
	public static void drawDownRight(int i, int j, int k) {
		//GL11.glScalef(0.5F, 0.5F, 0.5F);
		
		float f = (float) (k >> 24 & 0xff) / 255F;
		float f1 = (float) (k >> 16 & 0xff) / 255F;
		float f2 = (float) (k >> 8 & 0xff) / 255F;
		float f3 = (float) (k & 0xff) / 255F;
		GL11.glColor4f(f1, f2, f3, 255);
		GL11.glEnable(3042 /* GL_BLEND */);
		GL11.glDisable(3553 /* GL_TEXTURE_2D */);
		GL11.glEnable(2848 /* GL_LINE_SMOOTH */);
		GL11.glBlendFunc(770, 771);
		GL11.glBegin(4);
		GL11.glVertex2d(i +2, j +2);
		GL11.glVertex2d(i + 2, j - 2);
		GL11.glVertex2d(i - 2, j - 1);
		GL11.glEnd();
		GL11.glDisable(2848 /* GL_LINE_SMOOTH */);
		GL11.glEnable(3553 /* GL_TEXTURE_2D */);
		GL11.glDisable(3042 /* GL_BLEND */);
		//GL11.glScalef(2F, 2F, 2F);
	}
	
	public static void drawUpLeft(int i, int j, int k) {
		//GL11.glScalef(0.5F, 0.5F, 0.5F);
		
		float f = (float) (k >> 24 & 0xff) / 255F;
		float f1 = (float) (k >> 16 & 0xff) / 255F;
		float f2 = (float) (k >> 8 & 0xff) / 255F;
		float f3 = (float) (k & 0xff) / 255F;
		GL11.glColor4f(f1, f2, f3, 255);
		GL11.glEnable(3042 /* GL_BLEND */);
		GL11.glDisable(3553 /* GL_TEXTURE_2D */);
		GL11.glEnable(2848 /* GL_LINE_SMOOTH */);
		GL11.glBlendFunc(770, 771);
		GL11.glBegin(4);
		GL11.glVertex2d(i -2, j +2);
		GL11.glVertex2d(i + 2, j - 1);
		GL11.glVertex2d(i - 2, j - 2);
		GL11.glEnd();
		GL11.glDisable(2848 /* GL_LINE_SMOOTH */);
		GL11.glEnable(3553 /* GL_TEXTURE_2D */);
		GL11.glDisable(3042 /* GL_BLEND */);
		//GL11.glScalef(2F, 2F, 2F);
	}
	
	public static void drawUpRight(int i, int j, int k) {
		//GL11.glScalef(0.5F, 0.5F, 0.5F);
		
		float f = (float) (k >> 24 & 0xff) / 255F;
		float f1 = (float) (k >> 16 & 0xff) / 255F;
		float f2 = (float) (k >> 8 & 0xff) / 255F;
		float f3 = (float) (k & 0xff) / 255F;
		GL11.glColor4f(f1, f2, f3, 255);
		GL11.glEnable(3042 /* GL_BLEND */);
		GL11.glDisable(3553 /* GL_TEXTURE_2D */);
		GL11.glEnable(2848 /* GL_LINE_SMOOTH */);
		GL11.glBlendFunc(770, 771);
		GL11.glBegin(4);
		GL11.glVertex2d(i -1, j +2);
		GL11.glVertex2d(i +1, j - 3);
		GL11.glVertex2d(i - 3, j - 2);
		GL11.glEnd();
		GL11.glDisable(2848 /* GL_LINE_SMOOTH */);
		GL11.glEnable(3553 /* GL_TEXTURE_2D */);
		GL11.glDisable(3042 /* GL_BLEND */);
		//GL11.glScalef(2F, 2F, 2F);
	}
	
	public static void drawUpTri(int i, int j, int k) {
		GL11.glRotatef(180F, 0.0F, 0.0F, 1.0F);
		float f = (float) (k >> 24 & 0xff) / 255F;
		float f1 = (float) (k >> 16 & 0xff) / 255F;
		float f2 = (float) (k >> 8 & 0xff) / 255F;
		float f3 = (float) (k & 0xff) / 255F;
		
		GL11.glColor4f(f1, f2, f3, f);
		GL11.glEnable(3042 /* GL_BLEND */);
		GL11.glDisable(3553 /* GL_TEXTURE_2D */);
		GL11.glEnable(2848 /* GL_LINE_SMOOTH */);
		GL11.glBlendFunc(770, 771);
		GL11.glBegin(4);
		GL11.glVertex2d(i, j + 2);
		GL11.glVertex2d(i + 2, j - 2);
		GL11.glVertex2d(i - 2, j - 2);
		GL11.glEnd();
		GL11.glDisable(2848 /* GL_LINE_SMOOTH */);
		GL11.glEnable(3553 /* GL_TEXTURE_2D */);
		GL11.glDisable(3042 /* GL_BLEND */);
		GL11.glRotatef(-180F, 0.0F, 0.0F, 1.0F);
	}

	public static void drawTriSight(int i, int j, int k) {
		float f = (float) (k >> 24 & 0xff) / 255F;
		float f1 = (float) (k >> 16 & 0xff) / 255F;
		float f2 = (float) (k >> 8 & 0xff) / 255F;
		float f3 = (float) (k & 0xff) / 255F;
		GL11.glColor4f(f1, f2, f3, f);
		GL11.glEnable(3042 /* GL_BLEND */);
		GL11.glDisable(3553 /* GL_TEXTURE_2D */);
		GL11.glEnable(2848 /* GL_LINE_SMOOTH */);
		GL11.glBlendFunc(770, 771);
		GL11.glBegin(4);
		GL11.glVertex2d(i, j + 2);
		GL11.glVertex2d(i + 4, j - 4);
		GL11.glVertex2d(i - 4, j - 4);
		GL11.glEnd();
		GL11.glDisable(2848 /* GL_LINE_SMOOTH */);
		GL11.glEnable(3553 /* GL_TEXTURE_2D */);
		GL11.glDisable(3042 /* GL_BLEND */);
	}

	public static void drawFullCircle(int i, int j, double d, int k) {
		float f = (float)(k >> 24 & 0xff) / 255F;
		float f1 = (float)(k >> 16 & 0xff) / 255F;
		float f2 = (float)(k >> 8 & 0xff) / 255F;
		float f3 = (float)(k & 0xff) / 255F;
		GL11.glEnable(3042 /*GL_BLEND*/ );
		GL11.glDisable(3553 /*GL_TEXTURE_2D*/ );
		GL11.glEnable(2848 /*GL_LINE_SMOOTH*/ );
		GL11.glBlendFunc(770, 771);
		GL11.glColor4f(f1, f2, f3, f);
		GL11.glBegin(6);
		for (int l = 0; l <= 360; l++) {
			double d1 = Math.sin(((double) l * 3.1415259999999998D) / 180D) * d;
			double d2 = Math.cos(((double) l * 3.1415259999999998D) / 180D) * d;
			GL11.glVertex2d((double) i + d1, (double) j + d2);
		}
		GL11.glEnd();
		GL11.glDisable(2848 /*GL_LINE_SMOOTH*/ );
		GL11.glEnable(3553 /*GL_TEXTURE_2D*/ );
		GL11.glDisable(3042 /*GL_BLEND*/ );
	}

	public static void DrawCircle(float f, float f1, float f2, int i, int j) {
		float f3 = (float) (j >> 24 & 0xff) / 255F;
		float f4 = (float) (j >> 16 & 0xff) / 255F;
		float f5 = (float) (j >> 8 & 0xff) / 255F;
		float f6 = (float) (j & 0xff) / 255F;
		float f7 = (float) (6.2831852000000001D / (double) i);
		float f8 = (float) Math.cos(f7);
		float f9 = (float) Math.sin(f7);
		GL11.glColor4f(f4, f5, f6, f3);
		float f11 = f2;
		float f12 = 0.0F;
		GL11.glEnable(3042 /* GL_BLEND */);
		GL11.glDisable(3553 /* GL_TEXTURE_2D */);
		GL11.glEnable(2848 /* GL_LINE_SMOOTH */);
		GL11.glBlendFunc(770, 771);
		GL11.glBegin(2);
		for (int k = 0; k < i; k++) {
			GL11.glVertex2f(f11 + f, f12 + f1);
			float f10 = f11;
			f11 = f8 * f11 - f9 * f12;
			f12 = f9 * f10 + f8 * f12;
		}
		GL11.glEnd();
		GL11.glEnable(3553 /* GL_TEXTURE_2D */);
		GL11.glDisable(3042 /* GL_BLEND */);
		GL11.glDisable(2848 /* GL_LINE_SMOOTH */);
	}

	protected void drawSideGradientRect(int i, int j, int k, int l, int i1, int j1) {
		float f = (float) (i1 >> 24 & 0xff) / 255F;
		float f1 = (float) (i1 >> 16 & 0xff) / 255F;
		float f2 = (float) (i1 >> 8 & 0xff) / 255F;
		float f3 = (float) (i1 & 0xff) / 255F;
		float f4 = (float) (j1 >> 24 & 0xff) / 255F;
		float f5 = (float) (j1 >> 16 & 0xff) / 255F;
		float f6 = (float) (j1 >> 8 & 0xff) / 255F;
		float f7 = (float) (j1 & 0xff) / 255F;
		GL11.glDisable(3553 /* GL_TEXTURE_2D */);
		GL11.glEnable(3042 /* GL_BLEND */);
		GL11.glDisable(3008 /* GL_ALPHA_TEST */);
		GL11.glBlendFunc(770, 771);
		GL11.glShadeModel(7425 /* GL_SMOOTH */);
		Tessellator tessellator = Tessellator.instance;
		tessellator.startDrawingQuads();
		tessellator.setColorRGBA_F(f1, f2, f3, f);
		tessellator.addVertex(k, j, zLevel);
		tessellator.setColorRGBA_F(f5, f6, f7, f4);
		tessellator.addVertex(i, j, zLevel);
		tessellator.addVertex(i, l, zLevel);
		tessellator.setColorRGBA_F(f1, f2, f3, f);
		tessellator.addVertex(k, l, zLevel);
		tessellator.draw();
		GL11.glShadeModel(7424 /* GL_FLAT */);
		GL11.glDisable(3042 /* GL_BLEND */);
		GL11.glEnable(3008 /* GL_ALPHA_TEST */);
		GL11.glEnable(3553 /* GL_TEXTURE_2D */);
	}

	public int getOpenGLVar(String name) {
		try {
			try {
				Field f = GL11.class.getDeclaredField(name);
				return f.getInt(null);
			} catch (NoSuchFieldException e) {
				Field[] af = GL11.class.getDeclaredFields();
				if(!name.startsWith("GL_"))
					name = "GL_" + name;
				for(Field f : af)
					if(f.getName().toLowerCase().replace("_", "").equals(name))
						return f.getInt(null);
			}
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
		return -1;
	}
	public String getOpenGLVar(int id) {
		Field[] af = GL11.class.getDeclaredFields();
		for(Field f : af)
			try {
				if(f.getInt(null) == id)
					return f.getName();
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			}
		return null;
	}
}
