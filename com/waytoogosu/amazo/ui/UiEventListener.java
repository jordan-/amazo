package com.waytoogosu.amazo.ui;

import java.awt.Font;

import net.minecraft.src.FontRenderer;

import com.waytoogosu.amazo.Amazo;
import com.waytoogosu.amazo.Wrapper;
import com.waytoogosu.amazo.event.Event;
import com.waytoogosu.amazo.event.EventListener;
import com.waytoogosu.amazo.font.AmazoUnicodeFont;
import com.waytoogosu.amazo.font.FontManager;
import com.waytoogosu.amazo.font.TrueTypeFont;
import com.waytoogosu.amazo.mod.Mod;
import com.waytoogosu.amazo.mod.ModType;
import com.waytoogosu.amazo.ui.panel.Panel;

public class UiEventListener implements EventListener{

	private AmazoUnicodeFont verdanaB14;
	private TrueTypeFont font;
	
	public UiEventListener(){
		FontManager fm = Wrapper.getFontManager();
		verdanaB14 = fm.getFont("Myriad Pro", 14);
		font = new TrueTypeFont(new Font("Verdana", Font.BOLD, 17), 0, 255);
	}

	@Override
	public void onEvent(Event e) {
		FontManager fm = Wrapper.getFontManager();
		font.drawStringWithShadow("Amazo v\247e" + Amazo.VERSION, 2, 2, 0xffffffff);
	//	fm.getFont("Myriad Pro", 18).drawStringWithShadow("Amazo v\247e" + Amazo.VERSION + "", 2, 2, 0xffffffff);

		FontRenderer fr = Wrapper.getMc().fontRenderer;
		int y = 2;
		for(Mod m : Wrapper.getModManager().getEnabledMods()){
			if(!m.isShowInGui() || !m.getType().equals(ModType.TOGGLE)) continue;
			String s = m.getName();
			int x = Wrapper.getWidth() - 2 - fr.getStringWidth(m.getName());
			fr.drawStringWithShadow(s, x, y, m.getColor());
			y += 10;
		}
		
		if(Wrapper.getMc().currentScreen == Wrapper.getUiScreen()) return;
		for(Panel p : Wrapper.getUiScreen().getPanelManager().getPanels()){
			if(p.isPinned()){
				p.draw();
			}
		}
	}

}
