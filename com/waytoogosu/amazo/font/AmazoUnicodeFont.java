package com.waytoogosu.amazo.font;

import java.awt.Color;
import java.awt.Font;

import org.newdawn.slick.SlickException;
import org.newdawn.slick.UnicodeFont;
import org.newdawn.slick.font.effects.ColorEffect;
import static org.lwjgl.opengl.GL11.*;
import net.minecraft.src.*;

public class AmazoUnicodeFont extends FontRenderer{

	private final UnicodeFont font;
	private int size, style;
	private String name;

	@SuppressWarnings("unchecked")
	public AmazoUnicodeFont(Font awtFont) {
		super(Minecraft.getMinecraft().gameSettings, new ResourceLocation("textures/font/ascii.png"), Minecraft.getMinecraft().getTextureManager() /* renderEngine */, false);

		name = awtFont.getFontName();
		size = awtFont.getSize();
		style = awtFont.getStyle();

		font = new UnicodeFont(awtFont);
		font.addAsciiGlyphs();
		font.getEffects().add(new ColorEffect(Color.WHITE));
		try {
			font.loadGlyphs();
		} catch(SlickException exception) {
			throw new RuntimeException(exception);
		}
		String alphabet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		FONT_HEIGHT = font.getHeight(alphabet) / 2;
	}

	@Override
	public int drawString(String string, int x, int y, int color) {
		if(string == null)
			return 0;
		string = string.trim();
		glPushMatrix();
		glEnable(3042);
		glDisable(GL_DEPTH_TEST);
		glEnable(GL_POINT_SMOOTH);
		glHint(GL_POINT_SMOOTH_HINT, GL_NICEST);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glEnable(GL_BLEND);
		glDisable(GL_LIGHTING);
		glDisable(GL_TEXTURE_2D);
		glDepthMask(false);
		glScalef(0.5F, 0.5F, 0.5F);
		x *= 2;
		y *= 2;

		if(string.contains("\247")){
			if(string.charAt(string.length() - 2) == '\247') {
				string = string.substring(0, string.length() - 2);
			}
			String array[] = string.split("\247");	
			int newX = x;
			int index = 0;
			for(String s : array){
				String s1 = s;
				if(s.length() > 1 && index != 0){
					char code = s.charAt(0);
					String colorCodes = "0123456789abcdefk";
					if(colorCodes.contains(Character.toString(code))){
						color  = Minecraft.getMinecraft().fontRenderer.colorCode[colorCodes.indexOf(code)];
					}
					s = s.substring(1);
				}
				font.drawString(newX, y, s, new org.newdawn.slick.Color(color));
				newX += getStringWidth(s) * 2;
				index++;
			}

		} else {
			font.drawString(x, y, string, new org.newdawn.slick.Color(color));
		}

		glEnable(GL_DEPTH_TEST);
		glDisable(GL_POINT_SMOOTH);
		glDepthMask(true);
		glPopMatrix();
		return x;
	}

	@Override
	public int drawStringWithShadow(String string, int x, int y, int color) {
		if(string.contains("\247")){
			drawString(stripColorCodes(string), x + 1, y + 1, 0xff000000);
		} else {
			drawString(string, (x + 1), (y + 1), 0xff000000);
		}
		return drawString(string, x, y, color) ;
	}

	@Override
	public int getCharWidth(char c) {
		return getStringWidth(Character.toString(c));
	}

	@Override
	public int getStringWidth(String string) {
		if(string.contains("\247")){
			return font.getWidth(stripColorCodes(string))/2;
		} else {
			return font.getWidth(string)/2;
		}
	}

	private String stripColorCodes(String string){
		String array[] = string.split("\247");
		string = "";
		int index = 0;
		for(String s : array){
			if(s.length() > 1 && index != 0){
				s = s.substring(1);
			}
			string += s;
			index++;
		}
		return string;
	}

	public int getStringHeight(String string) {
		return font.getHeight(string)/2;
	}

	public int getSize() {
		return size;
	}

	public int getStyle() {
		return style;
	}

	public String getName() {
		return name;
	}


}
