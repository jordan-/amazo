package com.waytoogosu.amazo.font;

import java.awt.Font;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.waytoogosu.amazo.Wrapper;

public class FontManager {

	//private List<AmazoUnicodeFont> fonts;
	
	private HashMap<String, AmazoUnicodeFont> fonts;
	private HashMap<String, TrueTypeFont> ttfFonts;
	
	public FontManager(){
		fonts = new HashMap();
		ttfFonts = new HashMap();
		
		registerFonts();
	}
	
	public TrueTypeFont getTtfFont(String name, int style, int size){
		for(String key : ttfFonts.keySet()){
			if(key.equalsIgnoreCase(name + style + size)) return ttfFonts.get(key);
		}
		return registerTtfFont(name, style, size);
	}
	
	public TrueTypeFont getTtfFont(String name, int size){
		return getTtfFont(name, Font.TRUETYPE_FONT, size);
	}

	public TrueTypeFont registerTtfFont(String name, int style, int size){
		TrueTypeFont ttf;
		ttfFonts.put(name + style + size, ttf = new TrueTypeFont(new Font(name, style, size), 0, 255));
		return ttf;
	}
	
	public TrueTypeFont registerTtfFont(String name, int size){
		return registerTtfFont(name, Font.TRUETYPE_FONT, size);
	}

	public AmazoUnicodeFont getFont(String name, int style, int size){
		for(String key : fonts.keySet()){
			if(key.equalsIgnoreCase(name + style + size)) return fonts.get(key);
		}
		return registerFont(name, style, size);
	}
	
	public AmazoUnicodeFont getFont(String name, int size){
		return getFont(name, Font.TRUETYPE_FONT, size);
	}

	public AmazoUnicodeFont registerFont(String name, int style, int size){
		AmazoUnicodeFont auf;
		fonts.put(name + style + size, auf = new AmazoUnicodeFont(new Font(name, style, size)));
		return auf;
	}
	
	public AmazoUnicodeFont registerFont(String name, int size){
		return registerFont(name, Font.TRUETYPE_FONT, size);
	}
	
	private void registerFonts(){
		registerFont("Verdana", 15);
		registerFont("Verdana", Font.BOLD, 14);
		registerFont("Myriad", 15);
	}

}
