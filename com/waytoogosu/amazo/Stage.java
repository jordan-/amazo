package com.waytoogosu.amazo;

public enum Stage {
	ALPHA("a"),
	BETA("b"),
	STABLE("");

	private String name;
	
	private Stage(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
}
