package com.waytoogosu.amazo;

import com.waytoogosu.amazo.event.*;
import com.waytoogosu.amazo.event.events.EventClickBlock;
import com.waytoogosu.amazo.event.events.EventRenderGameOverlay;
import com.waytoogosu.amazo.font.AmazoUnicodeFont;
import com.waytoogosu.amazo.mod.mods.mining.ModInstantMiner;
import com.waytoogosu.amazo.ui.UiEventListener;

public class Amazo {
	
	public static final double VERSION = 0.60;
	
	public static void setupAmazo(){
		Wrapper.setupWrapper();
		new UpdateManager();
	}
	
	
}
