package com.waytoogosu.amazo.event;

public interface EventListener {

	public void onEvent(Event e);
	
}