package com.waytoogosu.amazo.event;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import org.lwjgl.input.Keyboard;

import com.waytoogosu.amazo.Wrapper;
import com.waytoogosu.amazo.event.events.EventClickBlock;
import com.waytoogosu.amazo.event.events.EventKeyPress;
import com.waytoogosu.amazo.event.events.EventRenderGameOverlay;
import com.waytoogosu.amazo.event.events.EventSendChatMessage;
import com.waytoogosu.amazo.keybind.Keybind;
import com.waytoogosu.amazo.mod.ChatEventListener;
import com.waytoogosu.amazo.mod.Mod;
import com.waytoogosu.amazo.mod.mods.mining.ModInstantMiner;
import com.waytoogosu.amazo.ui.UiEventListener;

public class EventManager {

	private ArrayList<EventListenerData> listeners;

	public EventManager(){
		listeners = new ArrayList();
		registerEvents();
	}

	public Event onEvent(Event event) {
		ArrayList<EventListenerData> listeners = new ArrayList<EventListenerData>();
		for (EventListenerData eld : getListeners()) {
			listeners.add(eld);
		}
		Collections.sort(listeners, new Comparator<EventListenerData>() {
			@Override
			public int compare(EventListenerData eld1, EventListenerData eld2) {
				return eld1.getEventPriority().getEventPriority() - eld2.getEventPriority().getEventPriority();
			}
		});
		for (EventListenerData eld : getListeners()) {
			if (eld.getEventClass() == event.getClass()) { 
				eld.getEventListener().onEvent(event);
			}
		}
		return event;
	}

	public void registerEvent(EventListener listener, Class<? extends Event> eventClass) {
		registerEvent(listener, eventClass, EventPriority.NORMAL);
	}

	public void registerEvent(EventListener listener, Class<? extends Event> eventClass, EventPriority priority) {
		registerEvent(new EventListenerData(listener, eventClass, priority));
	}
	
	public void registerEvent(EventListenerData eld){
		getListeners().add(eld);
	}

	public ArrayList<EventListenerData> getListeners(){
		return listeners;
	}
	
	private void registerEvents(){
		registerEvent(new UiEventListener(), EventRenderGameOverlay.class, EventPriority.HIGHEST);
		registerEvent(new ChatEventListener(), EventSendChatMessage.class, EventPriority.HIGHEST);
		registerEvent(new EventListenerData(new EventListener(){

			@Override
			public void onEvent(Event e) {
				EventKeyPress ekp = (EventKeyPress) e;
				for(Keybind kb : Wrapper.getKeybindManager().getKeybinds()){
					
					if(ekp.getKey() == kb.getKeybind() & kb.getKeybind() != Keyboard.KEY_NONE){						
						kb.onAction();
					}
				}
			}
			
		}, EventKeyPress.class, EventPriority.HIGHEST));
		
		for(Mod m : Wrapper.getModManager().getMods()){
			for(EventListenerData eld : m.getEventListenerData()){
				System.out.println("A " + m.getName() + " listener has been registered.");
				registerEvent(eld);
			}
		}
		
	}

}
