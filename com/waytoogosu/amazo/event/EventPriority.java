package com.waytoogosu.amazo.event;

public enum EventPriority {
	
	LOWEST(0),
	LOW(1),
	NORMAL(2),
	HIGH(3),
	HIGHEST(4);
	
	private int priority;
	
	private EventPriority(int i) {
		priority = i;
	}
	
	public int getEventPriority() {
		return priority;
	}
}
