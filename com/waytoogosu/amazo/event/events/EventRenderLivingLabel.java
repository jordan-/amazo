package com.waytoogosu.amazo.event.events;

import com.waytoogosu.amazo.event.Event;

import net.minecraft.src.EntityLiving;
import net.minecraft.src.EntityLivingBase;

public class EventRenderLivingLabel extends Event{

	private EntityLivingBase entityLiving;
	private String renderText;
	private int color, backColor, renderDistance;
	private boolean seeThroughWalls;
	private float scaleMultiplier;
	
	public EventRenderLivingLabel(EntityLivingBase par1EntityLivingBase, String renderText){
		this.entityLiving = par1EntityLivingBase;
		this.color = -1;
		this.seeThroughWalls = false;
		this.scaleMultiplier = 1.6f;
		this.renderDistance = 64;
		setRenderText(renderText);
	}
	
	public float getScaleMultplier(){
		return scaleMultiplier;
	}
	
	public void setScaleMultiplier(float scaleMultiplier){
		this.scaleMultiplier = scaleMultiplier;
	}
	
	public int getRenderDistance() {
		return renderDistance;
	}

	public void setRenderDistance(int renderDistance) {
		this.renderDistance = renderDistance;
	}

	public String getRenderText(){
		return renderText;
	}
	
	public void setRenderText(String renderText){
		this.renderText = renderText;
	}

	public EntityLivingBase getEntityLiving() {
		return entityLiving;
	}

	public int getColor() {
		return color;
	}
	
	public void setColor(int color){
		this.color = color;
	}

	public boolean isSeeThroughWalls() {
		return seeThroughWalls;
	}

	public void setSeeThroughWalls(boolean seeThroughWalls) {
		this.seeThroughWalls = seeThroughWalls;
	}
	
}
