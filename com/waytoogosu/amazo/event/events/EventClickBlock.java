package com.waytoogosu.amazo.event.events;

import com.waytoogosu.amazo.Wrapper;
import com.waytoogosu.amazo.event.Event;
import com.waytoogosu.amazo.util.Position;

import net.minecraft.src.Block;

public class EventClickBlock extends Event{

	private Block block;
	private Position position;
	private int sideHit;

	public EventClickBlock(Block block, Position position, int sideHit){
		this.block = block;
		this.position = position;
		this.sideHit = sideHit;
	}
	
	public EventClickBlock(Position position, int sideHit){
		this(Block.blocksList[Wrapper.getMc().theWorld.getBlockId((int)position.getPosX(), (int)position.getPosY(), (int)position.getPosZ())], position, sideHit);
	}

	public int getSideHit(){
		return sideHit;
	}

	public Block getBlock(){
		return block;
	}
	
	public Position getPosition(){
		return position;
	}

}
