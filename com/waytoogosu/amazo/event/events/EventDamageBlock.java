package com.waytoogosu.amazo.event.events;

import com.waytoogosu.amazo.Wrapper;
import com.waytoogosu.amazo.event.Event;
import com.waytoogosu.amazo.util.Position;

import net.minecraft.src.Block;
import net.minecraft.src.EntityPlayer;


public class EventDamageBlock extends Event {
	
	private Block block;
	private float curBlockDamage, curBlockDamageMultiplier, breakAtBlockDamage;
	private int blockHitDelay;
	private Position position;
	
	public EventDamageBlock(Block block, Position position, float curBlockDamage, int blockHitDelay) {
		this.block = block;
		this.curBlockDamage = curBlockDamage;
		this.blockHitDelay = blockHitDelay;
		this.position = position;
		setCurBlockDamageMultiplier(1f);
		breakAtBlockDamage = 1.0f;
	}
	
	
	public EventDamageBlock(Position position, float curBlockDamage, int blockHitDelay) {
		this(Block.blocksList[Wrapper.getMc().theWorld.getBlockId((int)position.getPosX(), (int)position.getPosY(), (int)position.getPosZ())], position, curBlockDamage, blockHitDelay);
	}
	
	public float getBreakAtBlockDamage() {
		return breakAtBlockDamage;
	}

	public void setBreakAtBlockDamage(float breakAtBlockDamage) {
		this.breakAtBlockDamage = breakAtBlockDamage;
	}

	public float getCurBlockDamage() {
		return curBlockDamage;
	}
	
	public void setCurBlockDamage(float curBlockDamge) {
		this.curBlockDamage = curBlockDamge;
	}
	
	public int getBlockHitDelay() {
		return blockHitDelay;
	}
	
	public void setBlockHitDelay(int blockHitDelay) {
		this.blockHitDelay = blockHitDelay;
	}
	
	public Block getBlock() {
		return block;
	}
	
	public Position getPosition(){
		return position;
	}
	
	public void setCurBlockDamageMultiplier(float curBlockDamageMultiplier) {
		this.curBlockDamageMultiplier = curBlockDamageMultiplier;
	}
	
	public float getCurBlockDamageMultiplier() {
		return curBlockDamageMultiplier;
	}
}
