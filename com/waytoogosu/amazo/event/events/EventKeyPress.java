package com.waytoogosu.amazo.event.events;

import com.waytoogosu.amazo.event.Event;

public class EventKeyPress extends Event{

	private int key;
	
	public EventKeyPress(int key){
		this.key = key;
	}
	
	public int getKey(){
		return key;
	}
	
}
