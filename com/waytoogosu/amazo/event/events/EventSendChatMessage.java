package com.waytoogosu.amazo.event.events;

import com.waytoogosu.amazo.event.Event;

public class EventSendChatMessage extends Event{

	private String message;
	
	public EventSendChatMessage(String message){
		this.message = message;
	}
	
	public String getMessage(){
		return message;
	}
	
	public void setMessage(String message){
		this.message = message;
	}
	
}
