package com.waytoogosu.amazo.event;

public class EventListenerData {
	
	private EventListener listener;
	private EventPriority priority;
	private Class<? extends Event> eventClass;
	
	public EventListenerData(EventListener listener, Class<? extends Event> eventClass, EventPriority priority) {
		this.listener = listener;
		this.priority = priority;
		this.eventClass = eventClass;
	}
	
	public EventListener getEventListener() {
		return listener;
	}
	
	public EventPriority getEventPriority() {
		return priority;
	}
	
	public Class<? extends Event> getEventClass() {
		return eventClass;
	}
}