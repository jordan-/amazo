package com.waytoogosu.amazo.event;

public class Event {

	public Event(){
		this.cancelled = false;
	}
	
	private boolean cancelled;

	public boolean isCancelled() {
		return cancelled;
	}

	public void setCancelled(boolean cancelled) {
		this.cancelled = cancelled;
	}
	
}
