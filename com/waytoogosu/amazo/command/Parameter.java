package com.waytoogosu.amazo.command;

public class Parameter {

	public final static String INTEGER = "int";
	public final static String DOUBLE = "decimal";
	public final static String BOOLEAN = "state";
	public final static String STRING = "text";
	
	private String name, type;
	
	public Parameter(String name, String type){
		this.name = name;
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public String getType() {
		return type;
	}
	
}
