package com.waytoogosu.amazo.command;

import java.util.ArrayList;
import java.util.List;

import com.waytoogosu.amazo.Wrapper;
import com.waytoogosu.amazo.command.commands.*;
import com.waytoogosu.amazo.mod.*;

public class CommandManager {

	private List<Command> commands;

	public CommandManager(){
		commands = new ArrayList();
		for(Mod m : Wrapper.getModManager().getMods()){
			for(Command cmd : m.getCommands()){
				getCommands().add(cmd);
			}
		}
		
		registerCommands();
	
	}
	
	private void registerCommands(){
		commands.add(new CommandToggle());
		commands.add(new CommandKeybind());
		commands.add(new CommandHelp());
		commands.add(new CommandSpawn());
	}

	public List<Command> getCommands() {
		return commands;
	}

	public boolean argsMatchParam(String args[], Parameter[] params){
		for(int i = 0 ; i < params.length; i++){
			try{
				String s = args[i+1];
				Parameter p = params[i];
				if(p.getType() == Parameter.BOOLEAN){
					Boolean.parseBoolean(s);
				}
				if(p.getType() == Parameter.DOUBLE){
					Double.parseDouble(s);
				}
				if(p.getType() == Parameter.INTEGER){
					Integer.parseInt(s);
				}
				
			}catch (Exception e) {return false;}

		}
		return true;
	}
	
}
