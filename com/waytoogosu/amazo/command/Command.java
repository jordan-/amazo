package com.waytoogosu.amazo.command;

import com.waytoogosu.amazo.Wrapper;
import com.waytoogosu.amazo.ui.console.UiConsole;

public abstract class Command {

	public abstract String getName();
	public abstract String getDescription();
	public abstract Parameter[] getParams();
	public abstract boolean onCommand(String[] args, String cmd, boolean console);

	public void respond(String s, boolean console){
		if(console){
			Wrapper.getUiConsole().respond(UiConsole.OUTPUT, s);
		} else{
			Wrapper.getPlayer().addChatMessage(s);
		}
	}

}
