package com.waytoogosu.amazo.command.commands;

import java.util.List;

import com.waytoogosu.amazo.Amazo;
import com.waytoogosu.amazo.Wrapper;
import com.waytoogosu.amazo.command.Command;
import com.waytoogosu.amazo.command.Parameter;
import com.waytoogosu.amazo.mod.Mod;

public class CommandHelp extends Command{

	@Override
	public String getName() {
		return "Help";
	}

	@Override
	public String getDescription() {
		return "Generates a list of commands and mods the client has.";
	}

	@Override
	public Parameter[] getParams() {
		return new Parameter[] {};
	}

	@Override
	public boolean onCommand(String[] args, String cmd, boolean console) {
		String s = "\247fThe mods Amazo v\247e" + Amazo.VERSION + " \247fincludes"; 
		List<Mod> mods = Wrapper.getModManager().getMods();
		int index = 0;
		for(Mod mod : mods){
			s += "\247a " + mod.getName() + (index != mods.size() - 1 ? "\247f," : "\247f.");
			index++;
		}
		respond(s, console);
		
		return true;
	}

}
