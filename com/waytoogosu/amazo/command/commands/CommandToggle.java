package com.waytoogosu.amazo.command.commands;

import com.waytoogosu.amazo.Wrapper;
import com.waytoogosu.amazo.command.Command;
import com.waytoogosu.amazo.command.Parameter;
import com.waytoogosu.amazo.mod.Mod;

public class CommandToggle extends Command{

	@Override
	public String getName() {
		return "Toggle";
	}

	@Override
	public String getDescription() {
		return "Toggles mods.";
	}

	@Override
	public Parameter[] getParams() {
		return new Parameter[] {new Parameter("modName", Parameter.STRING)};
	}

	@Override
	public boolean onCommand(String[] args, String cmd, boolean console) {
		Mod mod = getMod(args[1]);
		mod.toggle();
		respond("Toggled \247a" + mod.getName() + "\247f to \247a" + mod.isEnabled() + "\247f.", console);
		return true;
	}
	
	private Mod getMod(String name){
		for(Mod m : Wrapper.getModManager().getMods()){
			if(!m.isTogglableByCommand()) continue;
			if(m.getName().equalsIgnoreCase(name)){
				return m;
			}
		}
		return null;
	}

}
