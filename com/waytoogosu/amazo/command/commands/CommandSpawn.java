package com.waytoogosu.amazo.command.commands;

import net.minecraft.src.Minecraft;
import net.minecraft.src.Packet11PlayerPosition;

import com.waytoogosu.amazo.Wrapper;
import com.waytoogosu.amazo.command.Command;
import com.waytoogosu.amazo.command.Parameter;
import com.waytoogosu.amazo.mod.Mod;

public class CommandSpawn extends Command{

	@Override
	public String getName() {
		return "Spawn";
	}

	@Override
	public String getDescription() {
		return "Exploit to teleport you to spawn";
	}

	@Override
	public Parameter[] getParams() {
		return new Parameter[] {};
	}

	@Override
	public boolean onCommand(String[] args, String cmd, boolean console) {
		Minecraft.getMinecraft().getNetHandler().addToSendQueue(new Packet11PlayerPosition(Double.NaN, Double.NaN, Double.NaN, Double.NaN, true));
		return true;
	}

}