package com.waytoogosu.amazo.command.commands;

import org.lwjgl.input.Keyboard;

import com.waytoogosu.amazo.Wrapper;
import com.waytoogosu.amazo.command.Command;
import com.waytoogosu.amazo.command.Parameter;
import com.waytoogosu.amazo.mod.Mod;

public class CommandKeybind extends Command{

	public boolean onCommand(String[] args, String cmd, boolean console) {
		Mod mod = getMod(args[1]);
		if(args[2].equalsIgnoreCase("clear")){
			mod.setKeybind(Keyboard.KEY_NONE);
			respond("\247a" + mod.getName() + " \247fkeybind cleared.", console);
			return true;
		}
		respond("\247a" + mod.getName() + " \247fkeybind set to \247a" + args[2].toUpperCase() + "\247f.", console);
		mod.setKeybind(Keyboard.getKeyIndex(args[2].toUpperCase()));
		return true;
	}
	
	private Mod getMod(String name){
		for(Mod m : Wrapper.getModManager().getMods()){
			if(m.getName().equalsIgnoreCase(name)){
				return m;
			}
		}
		return null;
	}

	@Override
	public String getName() {
		return "Keybind";
	}

	@Override
	public String getDescription() {
		return "Changes keybinds";
	}

	@Override
	public Parameter[] getParams() {
		return new Parameter[] { new Parameter("modName", Parameter.STRING), new Parameter("key/clear", Parameter.STRING)};
	}
	
}
