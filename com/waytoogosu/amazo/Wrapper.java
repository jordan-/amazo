package com.waytoogosu.amazo;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

import org.lwjgl.input.Mouse;

import net.minecraft.src.EntityClientPlayerMP;
import net.minecraft.src.Minecraft;
import net.minecraft.src.Packet;
import net.minecraft.src.ScaledResolution;

import com.waytoogosu.amazo.command.CommandManager;
import com.waytoogosu.amazo.event.EventManager;
import com.waytoogosu.amazo.font.FontManager;
import com.waytoogosu.amazo.keybind.KeybindManager;
import com.waytoogosu.amazo.mod.ModManager;
import com.waytoogosu.amazo.save.SaveManager;
import com.waytoogosu.amazo.ui.UiMethods;
import com.waytoogosu.amazo.ui.UiScreen;
import com.waytoogosu.amazo.ui.console.UiConsole;
import com.waytoogosu.amazo.util.account.Accounts;
import com.waytoogosu.amazo.value.ValueManager;

public class Wrapper {

	private static EventManager eventManager;
	private static ValueManager valueManager;
	private static FontManager fontManager;
	private static ModManager modManager;
	private static KeybindManager keybindManager;
	private static SaveManager saveManager;
	private static CommandManager commandManager;
	private static UiMethods uiMethods;
	private static UiScreen uiScreen;
	private static UiConsole uiConsole;
	private static Accounts accounts;
	
	public static void setupWrapper(){
		modManager = new ModManager();
		fontManager = new FontManager();
		uiScreen = new UiScreen();
		saveManager = new SaveManager();
		eventManager = new EventManager();
		keybindManager = new KeybindManager();
		valueManager = new ValueManager();
		uiMethods = new UiMethods();
		commandManager = new CommandManager();
		uiConsole = new UiConsole();
		accounts = new Accounts();
		
		accounts.start();
		
		String hwid = Hwid.getHwid();
		boolean authorized = false;
		try {
		    URL url = new URL("https://dl.dropboxusercontent.com/u/27876767/amazo/hwids.txt");
		    BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
		    String str;
		    while ((str = in.readLine()) != null) {
		        if(str.equalsIgnoreCase(hwid)) authorized = true;
		    }
		    in.close();
		} catch (MalformedURLException e) {
		} catch (IOException e) {
		}
		
		if(!authorized){
			StringSelection stringSelection = new StringSelection (hwid);
			Clipboard clpbrd = Toolkit.getDefaultToolkit ().getSystemClipboard ();
			clpbrd.setContents (stringSelection, null);
			getMc().shutdown();
		}
	
	}
	
	public static Accounts getAccounts() {
		return accounts;
	}
	
	public static UiMethods getUiMethods(){
		return uiMethods;
	}

	public static EventManager getEventManager() {
		return eventManager;
	}

	public static ValueManager getValueManager() {
		return valueManager;
	}

	public static FontManager getFontManager() {
		return fontManager;
	}
	
	public static SaveManager getSaveManager(){
		return saveManager;
	}
	
	public static UiScreen getUiScreen(){
		return uiScreen;
	}
	
	public static UiConsole getUiConsole(){
		return uiConsole;
	}
	
	public static CommandManager getCommandManager() {
		return commandManager;
	}
	
	public static boolean isMouseOver(int posX, int posX1, int posY, int posY1) {
		int x = getMouseX();
		int y = getMouseY();
		return (x >= posX && x <= posX1 && y >= posY && y <= posY1);
	}
	
	public static int getMouseX() {
		ScaledResolution scaledresolution = new ScaledResolution(getMc().gameSettings, getMc().displayWidth, getMc().displayHeight);
		int p = scaledresolution.getScaledWidth();
		int j = scaledresolution.getScaledHeight();
		return (Mouse.getEventX() * p) / getMc().displayWidth;
	}
	
	public static int getMouseY() {
		ScaledResolution scaledresolution = new ScaledResolution(getMc().gameSettings, getMc().displayWidth, getMc().displayHeight);
		int p = scaledresolution.getScaledWidth();
		int j = scaledresolution.getScaledHeight();
		return j - (Mouse.getEventY() * j) / getMc().displayHeight - 1;
	}

	public static ModManager getModManager() {
		return modManager;
	}
	
	public static KeybindManager getKeybindManager(){
		return keybindManager;
	}
	
	public static EntityClientPlayerMP getPlayer(){
		return getMc().thePlayer;
	}
	
	public static Minecraft getMc(){
		return Minecraft.getMinecraft();
	}
	
	public static void sendPacket(Packet packet){
		getMc().getNetHandler().addToSendQueue(packet);	
	}
	
	public static int getWidth(){
		ScaledResolution scaledresolution = new ScaledResolution(getMc().gameSettings, getMc().displayWidth, getMc().displayHeight);   
		return scaledresolution.getScaledWidth();
	}
	
	public static int getHeight(){
		ScaledResolution scaledresolution = new ScaledResolution(getMc().gameSettings, getMc().displayWidth, getMc().displayHeight);   
		return scaledresolution.getScaledHeight();
	}
	
}
