package com.waytoogosu.amazo.util.account;

import java.util.ArrayList;

import com.waytoogosu.amazo.util.FileHelper;

public class Accounts extends FileHelper {
	private static ArrayList<Account> accounts = new ArrayList<Account>();

	public Accounts() {
		super("alts.txt");
	}

	@Override
	public void start() {
		accounts = new ArrayList<Account>();
		for (String s : this.read()) {
			try {
				String s1[] = s.split(":");
				accounts.add(new Account(s1[0], s1[1]));
			} catch (Exception e) {
			}
		}
	}

	@Override
	public void end() {
		ArrayList<String> l = new ArrayList<String>();
		for (Account a : accounts) {
			String userPass = a.getUsername() + ":" + a.getPassword();
			l.add(userPass);
		}
		this.save(l);
	}

	public boolean contains(Account account) {
		for (Account account2 : getAccounts()) {
			if (account.getUsername().equalsIgnoreCase(account2.getUsername()))
				return true;
		}
		return false;
	}

	public boolean remove(String username) {
		for (Account account : getAccounts()) {
			if (account.getUsername().equalsIgnoreCase(username)) {
				getAccounts().remove(account);
				return true;
			}
		}
		return false;
	}

	public static ArrayList<Account> getAccounts() {
		return accounts;
	}

	public static void setAccounts(ArrayList<Account> accounts) {
		Accounts.accounts = accounts;
	}
}