package com.waytoogosu.amazo.util.account;

public class Account {
	private String username, password;

	public Account(String s, String s1) {
		username = s;
		password = s1;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
