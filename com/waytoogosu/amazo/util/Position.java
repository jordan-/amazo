package com.waytoogosu.amazo.util;

import net.minecraft.src.Entity;

public class Position {

	public Position(double posX, double posY, double posZ){
		setPosition(posX, posY, posZ);
	}
	
	public Position(){
		this(0, 0, 0);
	}
	
	private double posX, posY, posZ;

	public double getPosX() {
		return posX;
	}

	public void setPosX(double posX) {
		this.posX = posX;
	}

	public double getPosY() {
		return posY;
	}

	public void setPosY(double posY) {
		this.posY = posY;
	}

	public double getPosZ() {
		return posZ;
	}

	public void setPosZ(double posZ) {
		this.posZ = posZ;
	}
	
	public void setPosition(double posX, double posY, double posZ){
		setPosX(posX);
		setPosY(posY);
		setPosZ(posZ);
	}
	
	public void setToEntityPosition(Entity e){
		setPosition(e.posX, e.posY, e.posZ);
	}
	
	public void setEntityToPosition(Entity e){
		e.setPosition(getPosX(), getPosY(), getPosZ());
	}
	
}
