package com.waytoogosu.amazo.util;

import com.waytoogosu.amazo.Wrapper;

public class Bounds {

	private int posX, posY, width, height;
	
	public Bounds(int posX, int posY, int width, int height){
		
		this.posX = posX;
		this.posY = posY;
		this.width = width;
		this.height = height;
	}
	
	public void setWidth(int i) {
		this.width = i;
	}
		
	public int getEndPosY(){
		return posY + height;
	}
	
	public int getEndPosX(){
		return posX + width;
	}
	
	public boolean isMouseOverBounds(){
		return Wrapper.isMouseOver(getPosX(), getEndPosX(), getPosY(), getEndPosY());
	}
	
	public void drawBorderedRect(float borderThickness, int borderColor, int color){
		Wrapper.getUiMethods().drawBorderedRect(posX, posY, posX + width, posY + height, borderThickness, borderColor, color);
	}
	
	public void drawRect(int color){
		Wrapper.getUiMethods().drawRect(posX, posY, posX + width, posY + height, color);
	}
	
	public void drawHollowBorderedRect(float borderThickness, int color){
		Wrapper.getUiMethods().drawHollowBorderedRect(posX, posY, posX + width, posY + height, borderThickness, color);
	}
	
	public void drawGradientRect(int color1, int color2){
		Wrapper.getUiMethods().drawGradientRect(posX, posY, posX + width, posY + height, color1, color2);
	}

	public int getPosX() {
		return posX;
	}

	public int getPosY() {
		return posY;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}
	
	
	
}
