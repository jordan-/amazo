package com.waytoogosu.amazo.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.waytoogosu.amazo.Wrapper;

import net.minecraft.src.Minecraft;

public abstract class FileHelper {
	private File saveFile;

	public FileHelper(String fileName) {
		saveFile = new File(this.getDirectory(), fileName);
		if (!saveFile.exists()) {
			try {
				saveFile.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public String getName() {
		return saveFile.getName();
	}

	public File getDirectory() {
		return Wrapper.getSaveManager().getClientDirectory();
	}

	public abstract void start();

	public abstract void end();

	public List<String> read() {
		List<String> l = new ArrayList<String>();
		try {
			BufferedReader reader = new BufferedReader(new FileReader(saveFile));
			
			String line;
			while ((line = reader.readLine()) != null) {
				l.add(StringUtils.trim(line));
			}
			reader.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return l;
	}

	public void save(List<String> list) {
		try {
			saveFile.delete();
			saveFile.createNewFile();

			PrintWriter writer = new PrintWriter(
					new FileWriter(saveFile, false));
			for (String s : list) {
				writer.println(s);
			}
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
