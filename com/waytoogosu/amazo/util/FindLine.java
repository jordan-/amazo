package com.waytoogosu.amazo.util;


public class FindLine {
	
	public Position getVertexOne() {
		return vertexOne;
	}

	public Position getVertexTwo() {
		return vertexTwo;
	}

	private Position vertexOne;
	private Position vertexTwo;
	
	public FindLine(Position b, Position b1){
		vertexOne = b;
		vertexTwo = b1;
	}
	
}
