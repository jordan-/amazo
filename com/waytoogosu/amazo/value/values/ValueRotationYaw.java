package com.waytoogosu.amazo.value.values;

import com.waytoogosu.amazo.Wrapper;
import com.waytoogosu.amazo.value.Value;



public class ValueRotationYaw extends Value{

	public ValueRotationYaw() {
		super("rotationYaw", (float)0);
	}
	
	public void updateValue(){
		this.value = (float)Wrapper.getPlayer().rotationYaw;
	}

}
