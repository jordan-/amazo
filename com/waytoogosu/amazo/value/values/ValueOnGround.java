package com.waytoogosu.amazo.value.values;

import com.waytoogosu.amazo.Wrapper;
import com.waytoogosu.amazo.value.Value;


public class ValueOnGround extends Value{

	public ValueOnGround() {
		super("onGround");
		value = false;
	}
	
	public void updateValue(){
		this.value = Wrapper.getPlayer().onGround;
	}

}
