package com.waytoogosu.amazo.value.values;

import com.waytoogosu.amazo.Wrapper;
import com.waytoogosu.amazo.value.Value;


public class ValueRotationPitch extends Value{

	public ValueRotationPitch() {
		super("rotationPitch", (float)0f);
	}
	
	public void updateValue(){
		this.value = (float)Wrapper.getPlayer().rotationPitch;
	}

}
