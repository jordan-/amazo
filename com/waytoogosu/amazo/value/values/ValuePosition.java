package com.waytoogosu.amazo.value.values;

import com.waytoogosu.amazo.Wrapper;
import com.waytoogosu.amazo.util.Position;
import com.waytoogosu.amazo.value.Value;

public class ValuePosition extends Value{

	public ValuePosition() {
		super("pos", new Position(0D, 0D, 0D));
	}
	
	public void updateValue(){
		((Position)value).setPosX(Wrapper.getPlayer().posX);
		((Position)value).setPosY(Wrapper.getPlayer().posY);
		((Position)value).setPosZ(Wrapper.getPlayer().posZ);
	}

}
