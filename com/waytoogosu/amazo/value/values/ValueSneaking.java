package com.waytoogosu.amazo.value.values;

import com.waytoogosu.amazo.Wrapper;
import com.waytoogosu.amazo.value.Value;


public class ValueSneaking extends Value{

	public ValueSneaking() {
		super("sneaking");
		value = false;
	}
	
	public void updateValue(){
		this.value = Wrapper.getPlayer().isSneaking();
	}

}
