package com.waytoogosu.amazo.value;

import java.util.ArrayList;
import java.util.List;

import com.waytoogosu.amazo.Wrapper;
import com.waytoogosu.amazo.value.values.*;

public class ValueManager {

	private List<Value> values;

	public List<Value> getValues() {
		return values;
	}
	
	public ValueManager(){
		values = new ArrayList();
		getValues().add(new ValueOnGround());
		getValues().add(new ValuePosition());
		getValues().add(new ValueRotationPitch());
		getValues().add(new ValueRotationYaw());
		getValues().add(new ValueSneaking());
		
		Wrapper.getEventManager().registerEvent(new ValueEventListener(), ValueEventListener.CLASS, ValueEventListener.PRIORITY);
	}
	
	public Value getValue(String name){
		for(Value sv : getValues()){
			if(sv.getName().equalsIgnoreCase(name)){
				return sv;
			}
		}
		return null;
	}
	
	public void updateValues(){
		if(Wrapper.getMc().theWorld == null) return;
		for(Value sv : getValues()){
			if(!sv.isValueChanged()){
				sv.updateValue();
			}
		}
	}
	
	
	
}
