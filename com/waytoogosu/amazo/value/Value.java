package com.waytoogosu.amazo.value;

import com.waytoogosu.amazo.Wrapper;
import com.waytoogosu.amazo.util.Position;


public class Value {

	private String name;

	/*private boolean booleanValue;
	private float floatValue;
	private double doubleValue;*/
	protected Object value;
	private Object fakeValue;
	private boolean valueChanged;

	private void resetValues(){
		value = null;
		fakeValue = null;
		/*booleanValue = false;
		floatValue = 0;
		doubleValue = 0;*/
	}
	
	public Value(String name){
		this(name, null);
	}

	public Value(String name, Object value){
		this.name = name;	
		resetValues();
		valueChanged = false;
		this.value = value;
		/*switch(valueType){
		case VALUE_BOOLEAN:
			booleanValue = (Boolean) value;
			break;

		case VALUE_DOUBLE:
			doubleValue = (Double) value;
			break;

		case VALUE_FLOAT:
			floatValue = (Float) value;
			break;
		}*/
	}
	
	
	public float getIntValue(){
		return (Integer) (isValueChanged() ? fakeValue : value);
	}
	
	public float getFloatValue(){
		try{
		return (Float) (isValueChanged() ? fakeValue : value);
		} catch(Exception e) {}
		return 0f;
	}
	
	public double getDoubleValue(){
		return (Double) (isValueChanged() ? fakeValue : value);
	}
	
	public Position getPositionValue(){
		return (Position) (isValueChanged() ? fakeValue : value);
	}
	
	public boolean isValue(){
		return (Boolean) (isValueChanged() ? fakeValue : value);
	}
	
	public boolean isValueChanged() {
		return valueChanged;
	}
	
	public void setValue(Object value){
		this.fakeValue = value;
		valueChanged = true;
	}

	public String getName() {
		return name;
	}
	
	public void resetValue(){
		if(Wrapper.getMc().theWorld == null) return;
		this.fakeValue = null;
		valueChanged = false;
		updateValue();
	}

	public void updateValue(){}

}
