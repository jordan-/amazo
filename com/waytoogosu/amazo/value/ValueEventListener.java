package com.waytoogosu.amazo.value;

import com.waytoogosu.amazo.Wrapper;
import com.waytoogosu.amazo.event.Event;
import com.waytoogosu.amazo.event.EventListener;
import com.waytoogosu.amazo.event.EventPriority;
import com.waytoogosu.amazo.event.events.EventPreMotionUpdate;


public class ValueEventListener implements EventListener{

	public static final Class<? extends Event> CLASS = EventPreMotionUpdate.class;
	public static final EventPriority PRIORITY = EventPriority.HIGHEST;
	
	@Override
	public void onEvent(Event e) {
		Wrapper.getValueManager().updateValues();
	}

}
